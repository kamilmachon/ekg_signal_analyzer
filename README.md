## ECG Signal Analyzer

This document describes the general structure of the ECG signal analyzer project.
It also contains instructions on extending its functionality with new modules.

### Overview

This section provides an overview of the core system and its classes and interfaces.

The ECG signal analyzer is a modular framework for processing ECG data. It specifies interfaces for I/O classes, as well as for processing nodes.
The output of this system is a single data structure containing all the signals, which can be used by any graphics library to display the results.
In this project, QT is used for that purpose, although by design the system is agnostic in this regard.


#### Structure

At the core of the system is the supervisor module, which manages the inner workings and exposes functionality to outside code. One of these funcionalities is loading input data and saving reports. To achieve this, the supervisor has an I/O module interface reference, which can be implemented and fed to the supervisor. Loaded data is then passed to the processing nodes. These form a tree structure, which is read from an XML config file and constructed at runtime - each module defines its parent (defining order of execution), input signals and output signals. The modules must implement the IEcgTask interface and be registered in the spawner (more on this process further in the document).
Once all processing nodes have finished, a callback function will be called (if provided), which can be used to display / save the results.

The following is a simplified diagram of the dataflow in the application.

![alt text](doc_resources/system_overview.png "System overview")

### Classes

This section briefly describes all classes and interfaces present in the core system.

#### WorkflowSupervisor

This class is the logical center of the system. It provides endpoints for loading data, triggering processing and fetching the results.
It also keeps references to all processing nodes, as well as data produced by them.
The supervisor can tell the I/O module to load / save data. It also provides a function for triggering data processing.
A callback function can be assigned, which will be called after a successfull data processing run.

TODO: Illustration

#### ThreadPool

This class is used to easily schedule tasks to be performed and distribute them among available CPUs. All data processing nodes run their operations
on the thread pool - once a parent node has finished, all its children can run simultaneously. Adding new processing nodes does not require any
knowledge or work to be done with this clas - it is only listed here for completeness.

#### ConcurrentSet

This is a simple, thread-safe wrapper for an std::set. It provides only the required subset of the functionalities of std::set, but can be safely used in multi-threaded applications. It is used by the IEcgTask interface and the supervisor to exchange execution dependencies. There should be no need to use it explicitly when adding new processing nodes.

#### ConcurrentHashMap

This is a thread-safe wrapper for an std::unordered_map. It provides most of the std::unordered_map's functionality. It is used to store all the signals loaded and produced by the processing nodes. The ExampleEcgTask explains how to use it when implementing a new node.

#### IInputModule

This is the interface, which must be implemented and fed to the supervisor to allow for loading and saving data. It provides basic Load / Save function
declarations. Processing nodes do not directly interact with it. The GUI layer will use it via the WorkflowSupervisor.

#### IEcgTask

This is the most important interface. Every processing node must derive from this base class. It handles multi-threading, communication with the supervisor, as well as fetching input and output references. All the control logic is done here. To create a new processing node, only one function must be overridden - the ProcessData(...) function.

#### IEcgTaskSpawner

This is the spawner class, which is used by the supervisor to construct the execution tree at runtime. Every processing node must be registered inside this class - this is done using a simple macro, as described further.

#### TimeSeries

This is a container for a single signal. It stores data as a vector of {value, timestamp} pairs.
It provides convenience functions for finding values based on timestamps.
This is useful both when reading data (finding the value at a specific point in time), and when writing (to find the correct spot to place the data).
IMPORTANT: this vector is not automatically sorted, so each processing node must take care to place data in it in the correct order.
In most cases only one node writes to any given signal, so it should be rather simple.

#### EcgData

This is a simple struct, which holds a ConcurrentHashMap<std::string, TimeSeries<double>>
containing all the signals loaded and produced by processing nodes.
The key std::string is the name of the signal. The GUI layer can use the names directly to find signals.
The processing nodes only get const references to specific signals, so they must iterate through their inputs (see ExampleEcgTask).

#### WFDB and Wfdbwrapper

This are classes allowing you to load data from files. If you want to load data from your disc, database files have to be in location /usr/local/database or in folder build. 
Some possible paths in this case:
	"physionet.org/files/mitdb/1.0.0/108" - when the files are placed in subfolders;
	"100" - when the files are directly in database or build folder.
If you prefer to use database from physionet server your path needs to be "ltstdb/s20021" -, where lstdb is a name of the database and s20021 name of the record you want to load.
Working databases: mitdb, edb, ltstdb. 
All of these databases are containig two signals, you can find them using keys: "signal-1" and "signal-2".

## How to create a new processing node

So you have the framework, it compiled, loaded the data and all that good stuff. Now how do you make it actually calculate something?
Simple - create a processing node, define its inputs and outputs, spawn it and it will do its thing. Here are the steps you must take
witness the magic.

### 1. Declare your class

Before you start, create a branch for your module. While on the master branch, use `git checkout -b <your-branch>` to create it.
If you don't know git, check out [this tutorial](https://product.hubspot.com/blog/git-and-github-tutorial-for-beginners).
Once you have your branch set up, you're ready to start working on your node.

First, you need the header file. Create a subdirectory for your module under `include/ecg_signal_analyzer` and put the header file there.
The file name should be the same as the name of your class (use `PascalCase` for consistency).

![alt text](doc_resources/header_file.png "Location of the header file.")

As mentioned, all processing nodes must derive from IEcgTask. Go ahead and do that.

```c++
// ExampleEcgTask.hpp
#ifndef ECG_SIGNAL_ANALYZER_EXAMPLE_ECG_TASK_HPP
#define ECG_SIGNAL_ANALYZER_EXAMPLE_ECG_TASK_HPP

#include <ecg_signal_analyzer/core/IEcgTask.hpp>

namespace ecg
{

class ExampleEcgTask : public IEcgTask
{
public:
    ExampleEcgTask();

    virtual ~ExampleEcgTask() {}

    int ProcessData(const InputsType &inputs, const OutputsType &outputs) override;

private:
};

} // namespace ecg

#endif
```

Let's go through this code and explain what exactly happened.

First, include guards - this is basic stuff, which makes sure the header file is only included once in a compilation unit.
```c++
#ifndef ECG_SIGNAL_ANALYZER_EXAMPLE_ECG_TASK_HPP
#define ECG_SIGNAL_ANALYZER_EXAMPLE_ECG_TASK_HPP
```

Then, a namespace. All code in the ECG Signal Analyzer should be in the `ecg` namespace.
```c++
namespace ecg
{
```

Now the important part - we declare our class and publicly derive from `IEcgTask`.
```c++
class ExampleEcgTask : public IEcgTask
{
public:
    ExampleEcgTask();

    virtual ~ExampleEcgTask() {}

    int ProcessData(const InputsType &inputs, const OutputsType &outputs) override;

private:
};
```

Crucial stuff is happening here. We declare a default constructor - it is necessary, since the system cannot pass arguments to constructors of processing nodes. Then, we declare (and in this case define) the virtual destructor. The last thing is declaring an override for the `ProcessData` function. This is where all your brilliant signal analysis code will be called.

Finally, we close the namespace and the include guard:

```c++
} // namespace ecg

#endif
```

### 2. Define the processing node

Now that the declarations are in place, it's time to implement the actual processing. The place for this is your class' `.cpp` file. Create a subdirectory under `src` and put the file there, again naming it after your class in `PascalCase`.

![alt text](doc_resources/source_file.png "Location of the source file.")

Now let's go through the implementation of the `ExampleEcgTask`. At the top, place all your `include`'s and open the `ecg` namespace.
The `ExampleEcgTask` does not actually do any math, so we only include the class' header file and `iostream` to print some basic info.

```c++
#include <ecg_signal_analyzer/example_task/ExampleEcgTask.hpp>

#include <iostream>
namespace ecg
{
```

First things first, let's define the constructor which we declared in the header file.
If your module has any configurable parameters, you can give them initial values here.
The parameters are stored in an `std::unordered_map<std::string, std::any>`. This means
you can store any type of data in a single container, but you will have to cast it
to the correct type to retrieve it.
These parameters can also be changed, added and removed by the GUI layer.

```c++
ExampleEcgTask::ExampleEcgTask()
{
    // The Constructor is needed only for parameter initialization (parameters could be changed in GUI).
    // task_parameters_ is a hash map where the key is string and the value is std::any.
    // Thanks to the std::any you can put any parameter type you want inside the map, just like in the python dictionary.
    // Parameters can be added by operator[] or AddParameter(std::string, std::any) method function, just like presented below.
    task_parameters_["example0"] = 2137;
    task_parameters_["czypapiesz"] = true;
    AddParameter("example2", std::vector<float>(69));
}
```

Now define the `ProcessData` function. Technically, you can put all your logic directly inside it (which we do in this case, since this node is very simple). Generally, you should create additional `private` methods for your class to keep the code clean and readable.
Let's look at the implementation of the `ExampleEcgTask::ProcessData` piece by piece - there's quite a bit to take in.

First, you can retrieve the parameter values, if your module has any. As mentioned, this requires casting to the desired type.
Make sure the type you are casting to is correct, and use `try {...} catch {...}` clauses if necessary.

```c++
int ExampleEcgTask::ProcessData(const InputsType &inputs, const OutputsType &outputs)
{

    // First you can acces the parameters of your node. Due to the fact that the value is std::any accessing parameters requires casting.
    // You can either do it manually or call templated method GetParameter<T>(std::string key).
    // Casting value on wrong type will result in segmentation fault, so don't do that ;)
    {
        int param1 = GetParameter<int>("example0");
        bool param2 = GetParameter<bool>("czypapiesz");
        std::vector<float> param3 = std::any_cast<std::vector<float>>(task_parameters_["example2"]);
    }
```

Now is a good time to look at your input data. The `WorkflowSupervisor` does a lot of the work for you - it fetches the signals you need,
finds the correct time period and serves it to you on a silver platter, with a cherry on top. Here's how to grab that data.

```c++
    // Input data is given to you using pairs of iterators.
    // Because 'inputs' is a const reference, you cannot access the signals by name:
    {
        // auto some_signal = inputs["MySignalName"]; // Compilation error.
    }
    // Instead, you must iterate through them like any other container.
    for (const auto &input : inputs)
    {
        // input here is of type std::pair<std::string, std::pair<it, it>>. Pair-ception!
        // So, input.first will be a string - the name of the signal.
        std::cout << "This is the signal name: " << input.first << std::endl;

        // input.second will give you the pair of iterators. You can use them to access the values of your input data.
        auto data_range = input.second;
        for (auto it = data_range.first; it != data_range.second; ++it)
        {
            std::cout << "Value of " << it->value << " at time " << it->time << "\n";
            // Do super smart signal processing stuff here.
            // You could also copy this data into a vector for convenience, although
            // this is not recommended because of performance.
        }
        std::cout << std::endl;
    }
```

This is where you can perform all the magic - you have all the inputs you need.
Your processing node should produce a `std::vector<TimeSeries<double>::Point>`, or just a `TimeSeries<double>` object.
Now you have to place it where it needs to be. Here's how to do it.

```c++
    // When you have your results ready, store them in your output data.
    // Again, since this is a const reference, you can not ask for the signals by name:
    {
        // auto & some_signal = outputs["MySignalName"] // Compilation errors gallore!
    }
    // So you have to iterate through it.
    for (auto &output : outputs)
    {
        // Here, output is of type std::pair<std::string, std::reference_wrapper<TimeSeries<double>>>.
        // So, output.first is again, the name of the signal.
        std::cout << "Output name: " << output.first << "\n";

        // output.second is the reference wrapper - use get() on it, and you have direct access to the TimeSeries object.
        auto &output_series = output.second.get();

        // By now you should have your output data. This example will mock it up right here (notice the {value, timestamp} pairs)
        TimeSeries<double> some_output = {{{1.0, 110},
                                           {1.1, 118},
                                           {0.9, 124}}};

        // Now, we have to put it in the right place of our output reference.
        // If you know you are the only one writing into it, you can assume
        // it is empty and insert your data at the beginning.
        // If not, it's up to you to guarantee the ascending order of timestamps,
        // so find the right spots to put your data into:
        auto start_from = begin(some_output.points); // Use this to avoid searching from the beginning each time.
        for (auto &point : some_output.points)
        {
            auto it = output_series.Find(point.time, start_from);
            start_from = it; // In the next iteration, search for the right spot will start from here.
            output_series.points.insert(it, point);
        }
    }
```

Nice! If all went well, `return 0` to indicate success.

```c++
    // All good! Since nothing went wrong, return 0 as a success code.
    return 0;
}
```

Now we just close the namespace, and the processing node is done.

```c++
} // namespace ecg
```
To compile your node, you need to tell CMake there is a new source file. Locate the `LIBRARY` section in `CMakeLists.txt` and add your `.cpp` file:
```cmake
###########
# LIBRARY #
###########

add_library(${PROJECT_NAME} SHARED
src/core/IEcgTask.cpp
src/core/WorkflowSupervisor.cpp
src/core/IEcgTaskSpawner.cpp

src/example_task/ExampleEcgTask.cpp # This makes sure your class is compiled into the library
)
target_link_libraries(
${PROJECT_NAME} pthread
)
```
Be careful not to modify or remove other sources - it will save everyone some pain.

Now your class should be compiled, but, there are still a few things you need to do before your node is accepted.

### 3. Registering your node

This is quick and simple. Since C++ does not have reflection, we must somehow let the code know that the class is available. This is what
`IEcgTaskSpawner` is for. It is used to create an instance of a processing node based on its name passed in as a string.
This effectively allows the whole execution tree to be constructed at runtime, using a config file. All you have to do is register your node.

To do this, you need to add one line of code to `IEcgTaskSpawner.cpp`:

```c++
IEcgTaskPtr IEcgTaskSpawner::Spawn(std::string task)
{
  REGISTER(ExampleEcgTask) // This line registers your class with the spawner. Obviously, replace ExampleEcgTask with the name of your node.

  // Default (empty) return value. Do not remove this.
  return std::shared_ptr<IEcgTask>();
}
```

### 4. Adding your node to the execution graph

Now that your node is registered, the system can spawn it. However, you must let it know when to launch your node, what data it needs and what data
it produces. This is done by adding your node to the `.xml` config file. Locate the `config.xml` in the `resources` folder.
Inside, you will something like the following, but with more 'task' nodes:

```xml
<execution_graph>
  <task>
    <name>ECG_Baseline</name>
    <type>ExampleEcgTask</type>  <!-- EcgBaseline is not implemented yet, so use ExampleEcgTask for now. -->
    <!--<parent> This node is the root, so no parent here. </parent>-->
    
    <inputs> <!-- All input signals required by this node. -->
      <signal>RawData</signal>
    </inputs>

    <outputs> <!-- All output signals this node produces. -->
      <signal>EcgBaseline</signal>
    </outputs>
  </task>

  <task>
    <name>Test_ECG_Task_0</name>
    <type>ExampleEcgTask</type>
    <parent>ECG_Baseline</parent>
    
    <inputs> <!-- All input signals required by this node. -->
      <signal>EcgBaseline</signal>
    </inputs>

    <outputs> <!-- All output signals this node produces. -->
      <signal>Test_ECG_Task_0_OUT</signal>
    </outputs>
  </task>
</execution_graph>
```

Each `<task> ... </task>` section defines the place of a processing node within the execution graph.
* 'name' uniquely identifies your node. It is how others will locate it.
* 'type' is the C++ type of your node. It is the name of the class you created. The `IEcgTaskSpawner` will use this to spawn your node.
* 'parent' is the name (NOT the type) of the node above yours in the execution graph. `WorkflowSupervisor` uses this to make sure
your node does not launch before all your input signals are calculated by the previous tasks.
* 'inputs' groups the names of the input signals of your node. This is what you iterate over in `ProcessData`.
* 'outputs' groups the name of the output signals of your node. You also iterate through them to place your results.
* 'signal' is simply the name of the signal you take as input or output.

You must add your own node here, filling in the fields as described in the example above. In most cases, the inputs of your node will
be the outputs of another - collaborate with the authors of those nodes and agree upon names of those signals. Make sure not to make any typos,
since it will prevent your node from getting its input data.

### 5. Testing your node

You might want to write tests for your node before you even implement the `ProcessData` function. This helps you avoid 'writing tests that pass',
and forces you to think of corner cases and problems in advance.

The system uses GoogleTest to handle unit and integration tests. It is VERY important that you make sure your node behaves correctly - other people's
work depends on that.
We will not cover GoogleTest in detail here - go to [GoogleTest's GitHub page](https://github.com/google/googletest) to learn more.

To test your node, create a subdirectory under `test`, and create a source file for your test cases.
Name it after your class in `PascalCase`, with extension `.t.cpp`.

![alt text](doc_resources/test_source_file.png "Location of the test source file.")

Let's look at the tests for the `ExampleEcgTask`.

First, we have the `include`s, and a `using` directive to save ourselves some pain, since this is a `.cpp` file.

NOTE: you should NEVER (and I mean NEVER) have a `using namespace ...` in a header file. For the sake of others, just don't do it.

```c++
#include <gtest/gtest.h>

#include <ecg_signal_analyzer/example_task/ExampleEcgTask.hpp>

using namespace ecg;
```

Now for the actual tests. For this example, we will test that the default parameter values we specified in the constructor of `ExampleEcgTask`
are set up correctly.
```c++
TEST(ExampleEcgTaskTests, ParameterInitialValues)
{
  // Constructor should assign default values.
  ExampleEcgTask task;

  try
  {
    // Now, check if they are there.
    EXPECT_TRUE(task.GetParameter<bool>("czypapiesz")) << "Parameter \'czypapiesz\' was not set up correctly.";
    EXPECT_EQ(task.GetParameter<int>("example0"), 2137) << "Parameter \'example0\' was not set up correctly.";
    EXPECT_TRUE(task.GetParameter<std::vector<float>>("example2") == std::vector<float>(69, 1)) << "Parameter \'example2\' was not set up correctly.";
  }
  catch (...)
  {
    FAIL() << "Unexpected exception.";
  }
}
```

You obviously don't need to test if your initial values are set - this logic is handled by IEcgTask, so you can assume it works. And if it doesn't,
let the maintainers of that code know.

Your tests should focus on verifying that your processing node produces the correct outputs given specific inputs.
If you have a model in Python or MATLAB, simulate input and output data to consider ground truth, and then compare them with output from your class.

As with the source file for your node, this one also needs to be added to `CMakeLists.txt`. Locate the `TESTING` section and add your source file:

```cmake
###########
# TESTING #
###########

find_package(GTest REQUIRED)

include(CTest)

add_executable(${PROJECT_NAME}_tests
test/core/main.t.cpp
test/core/TimeSeries.t.cpp
test/core/ConcurrentHashMap.t.cpp
test/core/WorkflowSupervisor.t.cpp

test/example_task/ExampleEcgTask.t.cpp  # This makes sure your tests are added to the executable.
)
target_link_libraries(${PROJECT_NAME}_tests ${PROJECT_NAME} ${GTEST_LIBRARIES} pthread)

gtest_discover_tests(${PROJECT_NAME}_tests)
```

### 6. Creating a merge request

That's it! You have everything you need to add your processing node to the system.
[Create a merge request on GitLab](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html), and assign someone to it
(most likely this will be the project coordinator, and preferably someone who develops a node that depends on yours.) They will review your changes
and ask you to explain anything they do not understand. Once the reviewers agree that everything is in order, they will merge your changes into
the `master` branch.
