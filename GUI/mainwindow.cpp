#include "mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <ecg_signal_analyzer/core/IEcgTask.hpp>
#include <ecg_signal_analyzer/core/EcgData.hpp>
#include <ecg_signal_analyzer/core/WorkflowSupervisor.hpp>
#include <ecg_signal_analyzer/core/IInputModule.hpp>
#include <ecg_signal_analyzer/input_system/WfdbWrapper.hpp>
#include <iostream>
#include "./ui_mainwindow.h"

#include "plotecg.h"
#include "plothist.h"
#include "plotfft.h"
#include "poincare.h"

static ecg::WorkflowSupervisor wsX("../resources/config.xml");
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    InitState();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::InitState()
{
    databaseLoaded = false;
    ui->dfa_label->setHidden(true);
    ui->dfa_alpha2->setHidden(true);
    wsX.SetLoader(std::make_shared<ecg::WfdbWrapper>());
    ReadParametes();    //read default parrameters from all modules and set GUI object

    //**** BLOCK UPDATE CHART BUTTON *****
    ui->baseline_plot_update->blockSignals(true);
    ui->rpeaks_plot_update->blockSignals(true);
    ui->hrv1_plot_update_1->blockSignals(true);
    ui->hrv1_plot_update_2->blockSignals(true);
    ui->hrv2_plot_update_1->blockSignals(true);
    ui->hrv2_plot_update_2->blockSignals(true);
    ui->waves_plot_update->blockSignals(true);
    ui->hrv_dfa_plot_update->blockSignals(true);
    ui->st_plot_update->blockSignals(true);
    ui->twave_plot_update->blockSignals(true);
}

void MainWindow::UnlockState()
{
    ui->baseline_plot_update->blockSignals(false);
    ui->rpeaks_plot_update->blockSignals(false);
    ui->hrv1_plot_update_1->blockSignals(false);
    ui->hrv1_plot_update_2->blockSignals(false);
    ui->hrv2_plot_update_1->blockSignals(false);
    ui->hrv2_plot_update_2->blockSignals(false);
    ui->waves_plot_update->blockSignals(false);
    ui->hrv_dfa_plot_update->blockSignals(false);
    ui->st_plot_update->blockSignals(false);
    ui->twave_plot_update->blockSignals(false);
    ui->hrv1_TimeStart->blockSignals(false);
    databaseLoaded = true;
    //Prevent load new detabese before restart program
    ui->BrowseButton->setVisible(false);
    ui->FileName->setEnabled(false);
}

void MainWindow::on_BrowseButton_clicked()
{
    QString WorkPath = QDir::currentPath();
    QString file1Name = QFileDialog::getOpenFileName(this,
             tr("Open File"), WorkPath, tr("ECG Database Files (*.dat)"));

    if (!file1Name.isEmpty()) {
        if (file1Name.contains(WorkPath))
        {
            ui->FileName->setText(file1Name);
            ui->RunAll->setEnabled(1);
            PathToFile = file1Name.mid(WorkPath.length(), (file1Name.length() - WorkPath.length() - 4));
        }
        else
        {
            ui->FileName->setText("Choose correct file");
            ui->RunAll->setEnabled(0);
            QMessageBox msgBox;
            msgBox.setText("Select a file with a path relative to the program's executable directory");
            msgBox.exec();
        }
    }
    else
    {
        ui->FileName->setText("Choose correct file");
        ui->RunAll->setEnabled(0);
    }
}

void MainWindow::on_RunAll_clicked()
{
    std::string path = PathToFile.toStdString();    
    if (databaseLoaded == false)
        wsX.Load(path);   // Load a database file

    if (ui->StopTime->value() != 0.0 or ui->StartTime->value() != 0.0)
        wsX.Run(((uint64_t)ui->StartTime->value()*1000000000*1000*60*60), ((uint64_t)ui->StopTime->value()*1000000000*1000*60*60));
    else
        wsX.Run();


    std::cout << "Workflow done." << std::endl;
    UnlockState();
    UpdateGUI();
}

void MainWindow::ReadParametes()
{
    //**** BASELINE PARAMETERS ****
    ui->baseline_main_order->blockSignals(true);
    ui->baseline_freq->blockSignals(true);
    ui->baseline_smooth->blockSignals(true);
    ui->baseline_secend_order->blockSignals(true);
    ui->baseline_windowsize->blockSignals(true);
    ui->baseline_main_order->setCurrentIndex(wsX.Tasks()["ECG_Baseline"]->GetParameter<int>("butter_order")-2);
    ui->baseline_freq->setValue(wsX.Tasks()["ECG_Baseline"]->GetParameter<double>("butter_cutoff_freq"));
    ui->baseline_smooth->setChecked(wsX.Tasks()["ECG_Baseline"]->GetParameter<bool>("smooth_enable"));
    ui->baseline_secend_order->setCurrentIndex(wsX.Tasks()["ECG_Baseline"]->GetParameter<int>("savgol_order")-2);
    ui->baseline_windowsize->setValue(wsX.Tasks()["ECG_Baseline"]->GetParameter<int>("savgol_window"));
    ui->baseline_main_order->blockSignals(false);
    ui->baseline_freq->blockSignals(false);
    ui->baseline_smooth->blockSignals(false);
    ui->baseline_secend_order->blockSignals(false);
    ui->baseline_windowsize->blockSignals(false);
    //**** HRV 1 PARAMETERS ****
    ui->hrv1_TimeStart->blockSignals(true);
    ui->hrv1_freq->blockSignals(true);
    ui->hrv1_windowsize->blockSignals(true);
    ui->hrv1_freq->setValue(wsX.Tasks()["HRV1_Module"]->GetParameter<double>("sampl_freq"));
    ui->hrv1_windowsize->setValue(wsX.Tasks()["HRV1_Module"]->GetParameter<int>("window_size"));
    ui->hrv1_freq->blockSignals(false);
    ui->hrv1_windowsize->blockSignals(false);
    //**** HRV DFA PARAMETERS ****
    ui->hrv_dfa_alpha->blockSignals(true);
    ui->hrv_dfa_alpha->setCurrentIndex(wsX.Tasks()["HrvDfa"]->GetParameter<int>("dfa_alpha")-1);
    ui->hrv_dfa_alpha->blockSignals(false);
}

void MainWindow::UpdateGUI()
{
    UpdateChart();
    UpdateHrv1Table(0);


    ui->hrv1_table_all->setItem(0,0,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_other_data"].points[0].value)));
    ui->hrv1_table_all->setItem(0,1,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_other_data"].points[1].value)));
    ui->hrv1_table_all->setItem(0,2,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_other_data"].points[2].value)));
    ui->hrv1_table_all->setItem(0,3,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_other_data"].points[3].value)));
    ui->hrv1_table_all->setItem(0,4,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_other_data"].points[4].value)));
    ui->hrv1_table_all->setItem(0,5,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_other_data"].points[5].value)));
    ui->hrv1_table_all->setItem(0,6,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_other_data"].points[6].value)));
    ui->hrv1_table_all->setItem(0,7,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_other_data"].points[7].value)));
    ui->hrv1_table_all->setItem(0,8,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_other_data"].points[8].value)));
    ui->dfa_alpha1->display(wsX.Data()->signals["HrvDfaAlpha"].points[1].value);
    ui->dfa_alpha2->display(wsX.Data()->signals["HrvDfaAlpha"].points[3].value);
    ui->hrv2_SD1->display(wsX.Data()->signals["SDVals"].points[0].value);
    ui->hrv2_SD2->display(wsX.Data()->signals["SDVals"].points[1].value);
    ui->hrv2_triangular->display(wsX.Data()->signals["TriangularIndex"].points[0].value);
    ui->hrv2_M->display(wsX.Data()->signals["TINN"].points[0].value);
    ui->hrv2_N->display(wsX.Data()->signals["TINN"].points[1].value);
    ui->hrv2_TINN->display(wsX.Data()->signals["TINN"].points[2].value);
}

void MainWindow::UpdateChart()
{
    PlotECGBaseline();
    PlotRPeaks();
    PlotHRV1_Tachogram();
    PlotHRV1_FFT(false, true);
    PlotHRV2_Histogram();
    PlotHRV2_Poincare();
    PlotHrvDfa();
    PlotWAVES();
    PlotSTSegment();
    PlotTWaveAlt();
}

void MainWindow::UpdateHrv1Table(int TimeStart)
{
    unsigned long start = TimeStart/5;
    ui->hrv1_table_5min->setItem(0,0,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_mean_5min"].points[start].value)));
    ui->hrv1_table_5min->setItem(0,1,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_std_5min"].points[start].value)));
    ui->hrv1_table_5min->setItem(0,2,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_rmssd_5min"].points[start].value)));
    ui->hrv1_table_5min->setItem(0,3,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_nn50_5min"].points[start].value)));
    ui->hrv1_table_5min->setItem(0,4,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_pnn50_5min"].points[start].value)));
    ui->hrv1_table_5min->setItem(0,5,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_hf_5min"].points[start].value)));
    ui->hrv1_table_5min->setItem(0,6,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_lf_5min"].points[start].value)));
    ui->hrv1_table_5min->setItem(0,7,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_vlf_5min"].points[start].value)));
    ui->hrv1_table_5min->setItem(0,8,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_tp_5min"].points[start].value)));
    ui->hrv1_table_5min->setItem(0,9,new QTableWidgetItem(QString::number(wsX.Data()->signals["HRV1_lfhf_5min"].points[start].value)));

}
// @@@@@@@@@@@@@@@@@@@@@@@@ SECTION TO UPDATE PARAMETERS IN ECG MODULES @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

void MainWindow::on_baseline_main_order_currentIndexChanged(int index)
{
    wsX.Tasks()["ECG_Baseline"]->AddParameter("butter_order", index +2);
}

void MainWindow::on_baseline_freq_valueChanged(double arg1)
{
    wsX.Tasks()["ECG_Baseline"]->AddParameter("butter_cutoff_freq", arg1);
}

void MainWindow::on_baseline_smooth_stateChanged(int arg1)
{
    wsX.Tasks()["ECG_Baseline"]->AddParameter("smooth_enable", (bool)arg1);
}

void MainWindow::on_baseline_secend_order_currentIndexChanged(int index)
{
    wsX.Tasks()["ECG_Baseline"]->AddParameter("savgol_order", index +2);
}

void MainWindow::on_baseline_windowsize_valueChanged(int arg1)
{
    wsX.Tasks()["ECG_Baseline"]->AddParameter("savgol_window", arg1);
}

void MainWindow::on_hrv1_freq_valueChanged(double arg1)
{
    wsX.Tasks()["HRV1_Module"]->AddParameter("sampl_freq", arg1);
}

void MainWindow::on_hrv1_windowsize_valueChanged(int arg1)
{
    wsX.Tasks()["HRV1_Module"]->AddParameter("window_size", arg1);
}

void MainWindow::on_hrv_dfa_alpha_currentIndexChanged(int index)
{
    if (index == 1)
    {
        ui->dfa_label->setHidden(false);
        ui->dfa_alpha2->setHidden(false);
    }
    else
    {
        ui->dfa_label->setHidden(true);
        ui->dfa_alpha2->setHidden(true);
    }
    wsX.Tasks()["HrvDfa"]->AddParameter("dfa_alpha", index + 1);
}

void MainWindow::on_StartTime_valueChanged(double arg1)
{
    if (arg1 == 0.0 and ui->StopTime->value() == 0.0)
        ui->time_info->setText("Full data length selected");
    else
        ui->time_info->setText("Part data length selected");
}

void MainWindow::on_StopTime_valueChanged(double arg1)
{
    if (arg1 == 0.0 and ui->StartTime->value() == 0.0)
        ui->time_info->setText("Full data length selected");
    else
        ui->time_info->setText("Part data length selected");
}

void MainWindow::on_hrv1_TimeStart_valueChanged(int arg1)
{
        UpdateHrv1Table(arg1);
}

void MainWindow::PlotECGBaseline(const bool x_log, const bool y_log)
{
    QCustomPlot* customPlot(ui->baseline_plot);
    if(customPlot->graphCount() > 0) {
        customPlot->clearGraphs();
    }

    PlotECG PlotECG(customPlot);
    PlotECG.setLogscale(x_log, y_log);

    const auto outputs = wsX.GetTaskOutputs("ECG_Baseline");
    const auto inputs = wsX.GetTaskInputs("ECG_Baseline", 0, std::numeric_limits<uint64_t>::max());

    for (const auto &input : inputs)
    {
        const auto input_series = input.second;
        QVector<QCPGraphData> timeData;
        for (auto it = input_series.first; it != input_series.second; ++it)
        {
            timeData.append(QCPGraphData(static_cast<double>(it->time) / 1000000000.0, (it->value-1024)/200.0));
        }
        PlotECG.addPlot(timeData, QString::fromStdString(input.first));
    }

    for (const auto &output : outputs)
    {
        const auto &output_series = output.second.get();
        QVector<QCPGraphData> timeData(output_series.points.size());

        int i = 0;
        for (const auto &point : output_series.points)
        {
            timeData[i].key = static_cast<double>(point.time) / 1000000000.0;
            timeData[i].value = point.value;
            i++;
        }
        PlotECG.addPlot(timeData, QString::fromStdString(output.first));
    }
}

void MainWindow::PlotRPeaks(const bool x_log, const bool y_log)
{
    QCustomPlot* customPlot(ui->rpeaks_plot);
    if(customPlot->graphCount() > 0) {
        customPlot->clearGraphs();
    }

    PlotECG PlotECG(customPlot);
    PlotECG.setLogscale(x_log, y_log);

    const auto inputs = wsX.GetTaskInputs("RpeaksModule", 0, std::numeric_limits<uint64_t>::max());
    const auto &output_series = wsX.GetTaskOutputs("RpeaksModule").at("RpeaksData").get();

    for (const auto &input : inputs)
    {
        const auto input_series = input.second;
        QVector<QCPGraphData> timeData;
        for (auto it = input_series.first; it != input_series.second; ++it)
        {
            timeData.append(QCPGraphData(static_cast<double>(it->time) / 1000000000.0, it->value));
        }
        PlotECG.addPlot(timeData, QString::fromStdString(input.first));
    }

    QVector<QCPGraphData> timeData(output_series.points.size());
    int i = 0;
    for (const auto &point : output_series.points)
    {
        timeData[i].key = static_cast<double>(point.time) / 1000000000.0;
        timeData[i].value = point.value;
        i++;
    }
    PlotECG.addPoints(timeData, "RpeaksData");
}

void MainWindow::PlotHRV1_Tachogram(const bool x_log, const bool y_log)
{
    QCustomPlot* customPlot(ui->hrv1_plot_1);
    if(customPlot->graphCount() > 0) {
        customPlot->clearGraphs();
    }

    PlotECG PlotECG(customPlot);
    PlotECG.setLogscale(x_log, y_log);

    const auto &output_series = wsX.GetTaskOutputs("HRV1_Module").at("HRV1_Tachogram").get();
    QVector<QCPGraphData> timeData(output_series.points.size());

    int i = 0;
    for (const auto &point : output_series.points)
    {
        timeData[i].key = static_cast<double>(point.time) / 1000000000.0;
        timeData[i].value = point.value;
        i++;
    }
    PlotECG.addPlot(timeData, QString::fromStdString("HRV1_Tachogram"));
    PlotECG.rescaleAxes();
}

void MainWindow::PlotHRV1_FFT(const bool x_log, const bool y_log)
{
    QCustomPlot* customPlot(ui->hrv1_plot_2);
    if(customPlot->graphCount() > 0) {
        customPlot->clearGraphs();
    }

    PlotFFT PlotFFT(customPlot);

    const auto outputs = wsX.GetTaskOutputs("HRV1_Module");
    const auto &output_series1 = outputs.at("HRV1_FFT_Magnitude").get();
    const auto &output_series2 = outputs.at("HRV1_FFT_Frequency").get();

    QVector<double> x, y;
    for (const auto &point : output_series1.points)
    {
        y.append(point.value);
    }
    for (const auto &point : output_series2.points)
    {
        x.append(point.value);
    }
    PlotFFT.addPlot(x, y, "FFT");

    QVector<double> aX = {0.4, 0.15, 0.04, 0.003};
    PlotFFT.addStraightLines(aX);
    PlotFFT.setLogscale(x_log, y_log);

}

void MainWindow::PlotHRV2_Histogram(const bool x_log, const bool y_log)
{
    QCustomPlot* customPlot(ui->hrv2_plot_1);
    if(customPlot->graphCount() > 0) {
        customPlot->clearGraphs();
    }

    PlotHist PlotHist(customPlot);

    const auto &output_series = wsX.GetTaskOutputs("HRV2Module").at("Histogram").get();

    QVector<double> x, y;
    for (const auto &point : output_series.points)
    {
        y.append(point.time);
        x.append(point.value);
    }
    PlotHist.addPlot(x, y, "Histogram");

    QVector<double> x1, y1;
    const auto &output_series2 = wsX.GetTaskOutputs("HRV2Module").at("TINN").get();
    const auto &output_series3 = wsX.GetTaskOutputs("HRV2Module").at("MaxHistogramPoint").get();

    x1.append(output_series2.points[0].value);
    y1.append(0);
    x1.append(output_series2.points[2].value);
    y1.append(0);
    x1.append(output_series3.points[0].value);
    y1.append(output_series3.points[1].value);
    PlotHist.simplePlot(x1, y1, "MaxHistogramPoint");

    PlotHist.setLogscale(x_log, y_log);
}

void MainWindow::PlotHRV2_Poincare(const bool x_log, const bool y_log)
{
    QCustomPlot* customPlot(ui->hrv2_plot_2);
    if(customPlot->graphCount() > 0) {
        customPlot->clearGraphs();
    }
    Poincare Poincare(customPlot);

    QVector<double> x, y;

    const auto &output_series = wsX.GetTaskOutputs("HRV2Module").at("PoincarePlot").get();
    for (const auto &point : output_series.points)
    {
        x.append(point.time);
        y.append(point.value);
    }
    Poincare.addPoints(x, y);
    Poincare.setLogscale(x_log, y_log);
}

void MainWindow::PlotHrvDfa()
{
    QCustomPlot* customPlot(ui->hrv_dfa_plot);
    if(customPlot->graphCount() > 0) {
        customPlot->clearGraphs();
    }

    PlotECG PlotECG(customPlot, "log10(F(n))", "log10^n", 1);

    const auto &output_seriesX = wsX.GetTaskOutputs("HrvDfa").at("HrvDfaDataX").get();
    const auto &output_seriesY = wsX.GetTaskOutputs("HrvDfa").at("HrvDfaDataY").get();

    QVector<double> x, y;
    for (const auto &point : output_seriesX.points)
    {
        x.append(point.value);
    }
    for (const auto &point : output_seriesY.points)
    {
        y.append(point.value);
    }
    PlotECG.addPlot(x, y, "Plot");
    PlotECG.addPoints(x, y, "Points");
}

void MainWindow::PlotWAVES(const bool x_log, const bool y_log)
{
    QCustomPlot* customPlot(ui->vaves_plot);
    if(customPlot->graphCount() > 0) {
        customPlot->clearGraphs();
    }

    PlotECG PlotECG(customPlot);

    const auto inputs = wsX.GetTaskInputs("WavesModule", 0, std::numeric_limits<uint64_t>::max());
    const auto outputs = wsX.GetTaskOutputs("WavesModule");

    const auto input_series = inputs.at("FilteredData");
    QVector<QCPGraphData> timeData;
    for (auto it = input_series.first; it != input_series.second; ++it)
    {
        timeData.append(QCPGraphData(static_cast<double>(it->time) / 1000000000.0, it->value));
    }
    PlotECG.addPlot(timeData, "FilteredData");

    for (const auto &output : outputs)
    {
        const auto &output_series = output.second.get();
        QVector<QCPGraphData> timeData(output_series.points.size());

        int i = 0;
        for (const auto &point : output_series.points)
        {
            timeData[i].key = static_cast<double>(point.time) / 1000000000.0;
            timeData[i].value = point.value;
            i++;
        }
        PlotECG.addPoints(timeData, QString::fromStdString(output.first));
    }

    PlotECG.setLogscale(x_log, y_log);
}

void MainWindow::PlotSTSegment(const bool x_log, const bool y_log)
{
    QCustomPlot* customPlot(ui->st_plot);
    if(customPlot->graphCount() > 0) {
        customPlot->clearGraphs();
    }

    PlotECG PlotECG(customPlot);

    const auto inputs = wsX.GetTaskInputs("STSegmentModule", 0, std::numeric_limits<uint64_t>::max());
    const auto input_series = inputs.at("FilteredData");
    QVector<QCPGraphData> timeData;
    for (auto it = input_series.first; it != input_series.second; ++it)
    {
        timeData.append(QCPGraphData(static_cast<double>(it->time) / 1000000000.0, it->value));
    }
    PlotECG.addPlot(timeData, "FilteredData");

    const auto outputsStart = wsX.GetTaskOutputs("STSegmentModule").at("STStartPoint").get();
    const auto outputsStop = wsX.GetTaskOutputs("STSegmentModule").at("STStopPoint").get();

    QVector<double> x;
    for (const auto &point : outputsStart.points)
    {
        x.append(static_cast<double>(point.time) / 1000000000.0);
    }
    for (const auto &point : outputsStop.points)
    {
        x.append(static_cast<double>(point.time) / 1000000000.0);
    }
    PlotECG.addStraightLines(x);

    PlotECG.setLogscale(x_log, y_log);
}

void MainWindow::PlotTWaveAlt(const bool x_log, const bool y_log)
{
    QCustomPlot* customPlot(ui->twave_plot);
    if(customPlot->graphCount() > 0) {
        customPlot->clearGraphs();
    }

    PlotECG PlotECG(customPlot);

    const auto inputs = wsX.GetTaskInputs("T_Wave_Alt_Task", 0, std::numeric_limits<uint64_t>::max());
    const auto input_series = inputs.at("FilteredData");
    QVector<QCPGraphData> timeData;
    for (auto it = input_series.first; it != input_series.second; ++it)
    {
        timeData.append(QCPGraphData(static_cast<double>(it->time) / 1000000000.0, it->value));
    }
    PlotECG.addPlot(timeData, "FilteredData");

    // const auto TwaValue = wsX.GetTaskOutputs("T_Wave_Alt_Task").at("TwaValue").get();
    // QVector<QCPGraphData> twaData;
    // for (const auto &point : TwaValue.points)
    // {
    //     twaData.append(QCPGraphData(static_cast<double>(point.time) / 1000000000.0, point.value*10));
    // }
    // PlotECG.addPlot(twaData, "TwaMagnitude");

    const auto TwaBeats = wsX.GetTaskOutputs("T_Wave_Alt_Task").at("TwaBeats").get();
    QVector<double> x, y;
    for (const auto &point : TwaBeats.points)
    {   
        double d = static_cast<double>(point.time) / 1000000000.0;
        x.append(d);
        y.append(point.value/ 1000000000.0 +  d);
    }
    PlotECG.addBackgroudHiglight(x, y);

    // // Plot T_on points
    // const auto Ton = wsX.GetTaskOutputs("T_Wave_Alt_Task").at("Ton").get();
    // QVector<QCPGraphData> TonData;
    // for (const auto &point : Ton.points)
    // {
    //     TonData.append(QCPGraphData(static_cast<double>(point.time) / 1000000000.0, point.value));
    // }
    // PlotECG.addPoints(TonData, QString::fromStdString("T_on"));

    // // Plot T_end points
    // const auto Te = wsX.GetTaskOutputs("WavesModule").at("Te").get();
    // QVector<QCPGraphData> TeData;
    // for (const auto &point : Te.points)
    // {
    //     TeData.append(QCPGraphData(static_cast<double>(point.time) / 1000000000.0, point.value));
    // }
    // PlotECG.addPoints(TeData, QString::fromStdString("T_e"));

    const auto TwaveAmp = wsX.GetTaskOutputs("T_Wave_Alt_Task").at("TwaveAmp").get();
    QVector<QCPGraphData> TwaveAmpData;
    for (const auto &point : TwaveAmp.points)
    {
        TwaveAmpData.append(QCPGraphData(static_cast<double>(point.time) / 1000000000.0, point.value));
    }
    PlotECG.addPoints(TwaveAmpData, QString::fromStdString("T wave amplitude"));
    PlotECG.addPlot(TwaveAmpData, "T wave trend line");
    


    PlotECG.setLogscale(x_log, y_log);
}

void MainWindow::on_baseline_plot_update_clicked()
{
    bool x_log = false;
    bool y_log = false;
    if(ui->baseline_logy->isChecked())
        y_log = true;
    PlotECGBaseline(x_log, y_log);
}

void MainWindow::on_rpeaks_plot_update_clicked()
{
    bool x_log = false;
    bool y_log = false;
    if(ui->rpeaks_logy->isChecked())
        y_log = true;
    PlotRPeaks(x_log, y_log);
}

void MainWindow::on_hrv1_plot_update_2_clicked()
{
    bool x_log = false;
    bool y_log = false;
    if(ui->hrv1_logx_2->isChecked())
        x_log = true;
    if(ui->hrv1_logy_2->isChecked())
        y_log = true;
    PlotHRV1_FFT(x_log, y_log);
}

void MainWindow::on_hrv1_plot_update_1_clicked()
{
    bool x_log = false;
    bool y_log = false;
    if(ui->hrv1_logy_1->isChecked())
        y_log = true;
    PlotHRV1_Tachogram(x_log, y_log);
}

void MainWindow::on_hrv2_plot_update_1_clicked()
{
    bool x_log = false;
    bool y_log = false;
    if(ui->hrv2_logx_1->isChecked())
        x_log = true;
    if(ui->hrv2_logy_1->isChecked())
        y_log = true;
    PlotHRV2_Histogram(x_log, y_log);
}

void MainWindow::on_hrv2_plot_update_2_clicked()
{
    bool x_log = false;
    bool y_log = false;
    if(ui->hrv2_logx_2->isChecked())
        x_log = true;
    if(ui->hrv2_logy_2->isChecked())
        y_log = true;
    PlotHRV2_Poincare(x_log, y_log);
}

void MainWindow::on_waves_plot_update_clicked()
{
    bool x_log = false;
    bool y_log = false;
    if(ui->waves_logy->isChecked())
        y_log = true;
    PlotWAVES(x_log, y_log);
}

void MainWindow::on_hrv_dfa_plot_update_clicked()
{
    PlotHrvDfa();
}

void MainWindow::on_st_plot_update_clicked()
{
    bool x_log = false;
    bool y_log = false;
    if(ui->st_logy->isChecked())
        y_log = true;
    PlotSTSegment(x_log, y_log);
}

void MainWindow::on_twave_plot_update_clicked()
{
    bool x_log = false;
    bool y_log = false;
    if(ui->twave_logy->isChecked())
        y_log = true;
    PlotTWaveAlt(x_log, y_log);
}
