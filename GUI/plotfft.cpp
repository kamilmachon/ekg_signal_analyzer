#include "plotfft.h"

PlotFFT::PlotFFT(QCustomPlot *customPlot)
{
    this->customPlot = customPlot;

    //Range Manipulation & scroll zoom
    this->customPlot->setNoAntialiasingOnDrag(true); // more performance/responsiveness during dragging
    this->customPlot->setInteraction(QCP::iRangeDrag, true);
    this->customPlot->setInteraction(QCP::iRangeZoom, true);

    // give the axes some labels:
    this->customPlot->xAxis->setLabel("Frequency [Hz]");
    this->customPlot->yAxis->setLabel("Amplitude [ms^2]");

    //legend
    this->customPlot->legend->setVisible(true);
    this->customPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignLeft|Qt::AlignTop); // make legend align in top left corner or axis rect

    this->customPlot->setInteraction(QCP::iSelectPlottables, true);

}

void PlotFFT:: addPlot(const QVector<double> x, const QVector<double> y, const QString name)
{
    // create graph and assign data to it:
    this->customPlot->addGraph();
    int i = this->customPlot->graphCount()-1;
    this->customPlot->graph(i)->setName(name);
    QColor color(0, 0, 0);
    switch (i) {
        case 0: {
            color.setRed(255);
            break;
        }
        case 1: {
            color.setBlue(255);
            break;
        }
        case 2: {
            color.setGreen(255);
            break;
        }
    }
    this->customPlot->graph(i)->setPen(QPen(color));
    this->customPlot->graph(i)->setData(x, y);

    this->customPlot->yAxis->setRange(0, 100000);
    this->customPlot->xAxis->setRange(0, 0.5);
    this->customPlot->replot();
}


void PlotFFT:: setLogscale(const bool x, const bool y)
{
    if(x){
        this->customPlot->xAxis->setScaleType(QCPAxis::stLogarithmic);
        this->customPlot->xAxis->setNumberFormat("eb"); // e = exponential, b = beautiful decimal powers
        this->customPlot->xAxis->setNumberPrecision(0); // makes sure "1*10^4" is displayed only as "10^4"
    }else{
        this->customPlot->xAxis->setScaleType(QCPAxis::stLinear);
        this->customPlot->xAxis->setNumberFormat("g");        this->customPlot->xAxis->setNumberPrecision(2);
    }
    if(y){
        this->customPlot->yAxis->setScaleType(QCPAxis::stLogarithmic);
        this->customPlot->yAxis->setNumberFormat("eb"); // e = exponential, b = beautiful decimal powers
        this->customPlot->yAxis->setNumberPrecision(0); // makes sure "1*10^4" is displayed only as "10^4"
    }else{
        this->customPlot->yAxis->setScaleType(QCPAxis::stLinear);
        this->customPlot->yAxis->setNumberFormat("g");
    }
    this->customPlot->replot();
}


void PlotFFT:: addStraightLines(const QVector<double> x)
{
    for(int i=0; i<x.size(); i++){
        QCPItemStraightLine *line = new QCPItemStraightLine(this->customPlot);
        //QCPItemLine *line = new QCPItemLine(customPlot);
        line->point1->setCoords(x[i], 0);  // location of point 1 in plot coordinate
        line->point2->setCoords(x[i], 1);  // location of point 1 in plot coordinate
    }
}
