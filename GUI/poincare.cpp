#include "poincare.h"

Poincare::Poincare(QCustomPlot *customPlot)
{
    this->customPlot = customPlot;

    //Range Manipulation & scroll zoom
    this->customPlot->setNoAntialiasingOnDrag(true); // more performance/responsiveness during dragging
    this->customPlot->setInteraction(QCP::iRangeDrag, true);
    this->customPlot->setInteraction(QCP::iRangeZoom, true);

    this->customPlot->xAxis->setLabel("RR(j) [ms]");
    this->customPlot->yAxis->setLabel("RR(j+1) [ms]");

    //legend
    this->customPlot->legend->setVisible(true);
    this->customPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignLeft|Qt::AlignTop); // make legend align in top left corner or axis rect

    this->customPlot->setInteraction(QCP::iSelectPlottables, true);
}

void Poincare:: addPoints(const QVector<double> x, const QVector<double> y){

    this->customPlot->addGraph();
    //int i = this->customPlot->graphCount()-1;

    this->customPlot->graph(0)->setPen(QColor("blue"));
    this->customPlot->graph(0)->setLineStyle(QCPGraph::lsNone);
    this->customPlot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 4));
    this->customPlot->graph(0)->setName("Poincare");
    this->customPlot->graph(0)->setData(x, y);

    this->customPlot->rescaleAxes();

    this->customPlot->replot();
}

void Poincare:: setLogscale(const bool x, const bool y)
{
    //Log scale:
    if(x){
        this->customPlot->xAxis->setScaleType(QCPAxis::stLogarithmic);
        this->customPlot->xAxis->setNumberFormat("eb"); // e = exponential, b = beautiful decimal powers
        this->customPlot->xAxis->setNumberPrecision(0); // makes sure "1*10^4" is displayed only as "10^4"
    }else{
        this->customPlot->xAxis->setScaleType(QCPAxis::stLinear);
        this->customPlot->xAxis->setNumberFormat("gb");
    }
    if(y){
        this->customPlot->yAxis->setScaleType(QCPAxis::stLogarithmic);
        this->customPlot->yAxis->setNumberFormat("eb"); // e = exponential, b = beautiful decimal powers
        this->customPlot->yAxis->setNumberPrecision(0); // makes sure "1*10^4" is displayed only as "10^4"
    }else{
        this->customPlot->yAxis->setScaleType(QCPAxis::stLinear);
        this->customPlot->yAxis->setNumberFormat("gb");
    }

    this->customPlot->replot();
}
