#ifndef HISTPLOT_H
#define HISTPLOT_H

#include <QMainWindow>
#include "qcustomplot.h"

class PlotHist
{
public:
    PlotHist(QCustomPlot *customPlot);

    void addPlot(const QVector<double> x, const QVector<double> y, const QString name);
    void simplePlot(const QVector<double> x, const QVector<double> y, const QString name);
    void setLogscale(const bool x=false, const bool y=false);

private:
    QCustomPlot *customPlot;

};

#endif // HISTPLOT_H
