#ifndef PLOTECG_H
#define PLOTECG_H

#include <QMainWindow>
#include "qcustomplot.h"

class PlotECG
{
public:
    PlotECG(QCustomPlot *customPlot);
    PlotECG(QCustomPlot *customPlot, const QString x_label, const QString y_label, const int i);

    void addPlot(const QVector<double> x, const QVector<double> y, const QString name);
    void addPlot(const QVector<QCPGraphData> x, const QString name);
    void addPoints(const QVector<double> x, const QVector<double> y, const QString name);
    void addPoints(const QVector<QCPGraphData> x, const QString name);
    void addStraightLines(const QVector<double> x, int colorRed=0);
    void setLogscale(const bool x=false, const bool y=false);
    void rescaleAxes();
    void addBackgroudHiglight(const QVector<double> x, const QVector<double> y);
private:
    QCustomPlot *customPlot;
    int basePlotSettings(const QString name);
    int basePointsSettings(const QString name);
    void setPlot(const QString x_label, const QString y_label, const int i);
};

#endif // PLOTECG_H
