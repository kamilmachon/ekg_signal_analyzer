#ifndef PLOTFFT_H
#define PLOTFFT_H

#include <QMainWindow>
#include "qcustomplot.h"

class PlotFFT
{
public:
    PlotFFT(QCustomPlot *customPlot);
    void addPlot(const QVector<double> x, const QVector<double> y, const QString name);
    void addStraightLines(const QVector<double> x);
    void setLogscale(const bool x=false, const bool y=false);
    

private:
    QCustomPlot *customPlot;

};

#endif // PLOTFFT_H
