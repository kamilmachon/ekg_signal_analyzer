#include "plothist.h"

PlotHist:: PlotHist(QCustomPlot *customPlot)
{
    this->customPlot = customPlot;
    this->customPlot->setNoAntialiasingOnDrag(true); // more performance/responsiveness during dragging
    this->customPlot->setInteraction(QCP::iRangeDrag, true);
    this->customPlot->setInteraction(QCP::iRangeZoom, true);

    this->customPlot->yAxis->setVisible(true);
    this->customPlot->xAxis->setVisible(true);

    this->customPlot->legend->setVisible(true);
    this->customPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignLeft|Qt::AlignTop); // make legend align in top left corner or axis rect

    this->customPlot->setInteraction(QCP::iSelectPlottables, true);
}

void PlotHist:: addPlot(const QVector<double> x, const QVector<double> y, const QString name)
{
    QCPBars *volume = new QCPBars(customPlot->xAxis, this->customPlot->yAxis);

    volume->setData(x, y);
    volume->setWidth(1);
    volume->setPen(Qt::NoPen);
    volume->setBrush(QColor(100, 180, 110));
    volume->setName(name);

    this->customPlot->rescaleAxes();
    this->customPlot->replot();
}

void PlotHist:: simplePlot(const QVector<double> x, const QVector<double> y, const QString name)
{
    this->customPlot->addGraph();
    int i = this->customPlot->graphCount()-1;
    this->customPlot->graph(i)->setName(name);
    this->customPlot->graph(i)->setPen(QColor("red"));
    this->customPlot->graph(i)->setData(x, y);
    this->customPlot->replot();
}


void PlotHist:: setLogscale(const bool x, const bool y)
{
    //Log scale:
    if(x){
        this->customPlot->xAxis->setScaleType(QCPAxis::stLogarithmic);
        this->customPlot->xAxis->setNumberFormat("eb"); // e = exponential, b = beautiful decimal powers
        this->customPlot->xAxis->setNumberPrecision(0); // makes sure "1*10^4" is displayed only as "10^4"
    }else{
        this->customPlot->xAxis->setScaleType(QCPAxis::stLinear);
        this->customPlot->xAxis->setNumberFormat("gb");
    }
    if(y){
        this->customPlot->yAxis->setScaleType(QCPAxis::stLogarithmic);
        this->customPlot->yAxis->setNumberFormat("eb"); // e = exponential, b = beautiful decimal powers
        this->customPlot->yAxis->setNumberPrecision(0); // makes sure "1*10^4" is displayed only as "10^4"
    }else{
        this->customPlot->yAxis->setScaleType(QCPAxis::stLinear);
        this->customPlot->yAxis->setNumberFormat("gb");
    }

    this->customPlot->replot();
}
