#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <ecg_signal_analyzer/core/WorkflowSupervisor.hpp>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private Q_SLOTS:


    void on_BrowseButton_clicked();

    void on_RunAll_clicked();


    void on_baseline_main_order_currentIndexChanged(int index);

    void on_baseline_freq_valueChanged(double arg1);

    void on_baseline_smooth_stateChanged(int arg1);

    void on_baseline_secend_order_currentIndexChanged(int index);

    void on_baseline_windowsize_valueChanged(int arg1);

    void on_hrv1_freq_valueChanged(double arg1);

    void on_hrv1_windowsize_valueChanged(int arg1);

    void on_hrv_dfa_alpha_currentIndexChanged(int index);

    void on_StartTime_valueChanged(double arg1);

    void on_StopTime_valueChanged(double arg1);

    void on_hrv1_TimeStart_valueChanged(int arg1);



    void on_baseline_plot_update_clicked();

    void on_rpeaks_plot_update_clicked();

    void on_hrv1_plot_update_2_clicked();

    void on_hrv1_plot_update_1_clicked();

    void on_hrv2_plot_update_1_clicked();

    void on_hrv2_plot_update_2_clicked();

    void on_waves_plot_update_clicked();

    void on_hrv_dfa_plot_update_clicked();

    void on_st_plot_update_clicked();

    void on_twave_plot_update_clicked();

private:
    Ui::MainWindow *ui;
    QString PathToFile;
    bool databaseLoaded;
    void ReadParametes();
    void UpdateGUI();
    void UpdateChart();
    void UpdateHrv1Table(int StartTime);
    void InitState();
    void UnlockState();

    void PlotECGBaseline(const bool x_log = false, const bool y_log = false);
    void PlotRPeaks(const bool x_log = false, const bool y_log = false);
    void PlotHRV1_Tachogram(const bool x_log = false, const bool y_log = false);
    void PlotHRV1_FFT(const bool x_log = false, const bool y_log = false);
    void PlotHRV2_Histogram(const bool x_log = false, const bool y_log = false);
    void PlotHRV2_Poincare(const bool x_log = false, const bool y_log = false);
    void PlotWAVES(const bool x_log = false, const bool y_log = false);
    void PlotHrvDfa();
    void PlotSTSegment(const bool x_log = false, const bool y_log = false);
    void PlotTWaveAlt(const bool x_log = false, const bool y_log = false);
};
#endif // MAINWINDOW_H
