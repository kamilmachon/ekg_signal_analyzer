#include "plotecg.h"

PlotECG::PlotECG(QCustomPlot *customPlot, const QString x_label, const QString y_label, const int i)
{
    this->customPlot = customPlot;
    setPlot(x_label, y_label, i);
}

PlotECG::PlotECG(QCustomPlot *customPlot)
{
    this->customPlot = customPlot;
    setPlot("Time (min:sec,ms))", "y", 0);
}

void PlotECG:: setPlot(const QString x_label, const QString y_label, const int i)
{
    //Range Manipulation & scroll zoom
    this->customPlot->setNoAntialiasingOnDrag(true); // more performance/responsiveness during dragging
    this->customPlot->setInteraction(QCP::iRangeDrag, true);
    this->customPlot->setInteraction(QCP::iRangeZoom, true);

    if(i==0){
        QSharedPointer<QCPAxisTickerDateTime> dateTicker(new QCPAxisTickerDateTime);
        dateTicker->setDateTimeSpec(Qt::UTC);
        dateTicker->setDateTimeFormat("mm:ss,zzz");
        this->customPlot->xAxis->setTicker(dateTicker);
    }

    // give the axes some labels:
    this->customPlot->xAxis->setLabel(x_label);
    this->customPlot->yAxis->setLabel(y_label);

    if(i==0){
        this->customPlot->xAxis->setRange(0, 3);
        this->customPlot->yAxis->setRange(-1, 1.5);
    }else{
        this->customPlot->rescaleAxes();
    }
    //legend
    this->customPlot->legend->setVisible(true);
    this->customPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignLeft|Qt::AlignTop);

    this->customPlot->setInteraction(QCP::iSelectPlottables, true);
}

void PlotECG:: addPlot(const QVector<double> x, const QVector<double> y, const QString name)
{
    int i = basePlotSettings(name);
    this->customPlot->graph(i)->setData(x, y);
    this->customPlot->rescaleAxes();
    this->customPlot->replot();
}

void PlotECG:: addPlot(const QVector<QCPGraphData> x, const QString name)
{
    int i = basePlotSettings(name);
    this->customPlot->graph(i)->data()->set(x);
    this->customPlot->rescaleAxes();
    this->customPlot->xAxis->setRange(x[0].key, x[0].key+3);
    this->customPlot->replot();

}

int PlotECG:: basePlotSettings(const QString name)
{
    // create graph and assign data to it:
    this->customPlot->addGraph();
    int i = this->customPlot->graphCount()-1;
    this->customPlot->graph(i)->setName(name);

    QVector<QColor> colors = {QColor("red"), QColor("black"), QColor("blue"), QColor("green"),
                              QColor("violet"), QColor("orange"), QColor("pink"), QColor("grey")};
    int num1 = 0;
    if(i>7){/*
        std::default_random_engine generator;
        std::uniform_int_distribution<int> distribution(0,7);
        num1 = distribution(generator);*/
        num1 = i-7;
    }else{
        num1 = i;
    }
    this->customPlot->graph(i)->setPen(colors[num1]);

    return i;
}

void PlotECG:: setLogscale(const bool x, const bool y)
{
    //Log scale:
    if(x){
        this->customPlot->xAxis->setScaleType(QCPAxis::stLogarithmic);
        this->customPlot->xAxis->setNumberFormat("eb"); // e = exponential, b = beautiful decimal powers
        this->customPlot->xAxis->setNumberPrecision(0); // makes sure "1*10^4" is displayed only as "10^4"
    }else{
        this->customPlot->xAxis->setScaleType(QCPAxis::stLinear);
        this->customPlot->xAxis->setNumberFormat("gb");
    }
    if(y){
        this->customPlot->yAxis->setScaleType(QCPAxis::stLogarithmic);
        this->customPlot->yAxis->setNumberFormat("eb"); // e = exponential, b = beautiful decimal powers
        this->customPlot->yAxis->setNumberPrecision(0); // makes sure "1*10^4" is displayed only as "10^4"
    }else{
        this->customPlot->yAxis->setScaleType(QCPAxis::stLinear);
        this->customPlot->yAxis->setNumberFormat("gb");
    }

    this->customPlot->replot();
}

void PlotECG:: rescaleAxes(){
    this->customPlot->rescaleAxes();
    this->customPlot->replot();
}

void PlotECG:: addPoints(const QVector<double> x, const QVector<double> y, const QString name)
{
    int i = basePointsSettings(name);

    this->customPlot->graph(i)->setData(x, y);
    this->customPlot->rescaleAxes();
    this->customPlot->replot();
}

void PlotECG:: addPoints(const QVector<QCPGraphData> x, const QString name)
{
    int i = basePointsSettings(name);

    this->customPlot->graph(i)->data()->set(x);
    this->customPlot->rescaleAxes();
    this->customPlot->xAxis->setRange(x[0].key, x[0].key+3);
    this->customPlot->replot();
}

int PlotECG:: basePointsSettings(const QString name)
{
    this->customPlot->addGraph();
    int i = this->customPlot->graphCount()-1;

    this->customPlot->graph(i)->setName(name);
    this->customPlot->graph(i)->setLineStyle(QCPGraph::lsNone);

    QVector<QColor> colors = {QColor("red"), QColor("black"), QColor("blue"), QColor("green"),
                              QColor("violet"), QColor("orange"), QColor("pink"), QColor("grey")};
    QVector<QCPScatterStyle> scatterStyle = {QCPScatterStyle(QCPScatterStyle::ssDisc, 12)};//, QCPScatterStyle(QCPScatterStyle::ssDisc, 14)};
    //QCPScatterStyle(QCPScatterStyle::ssPeace, 14), QCPScatterStyle(QCPScatterStyle::ssStar, 14),
    int num1 = 0;
    /*if(numP<0){
        std::default_random_engine generator2;
        std::uniform_int_distribution<int> distribution2(0,1);
        num2 = distribution2(generator2);
    }else{
        num2 = numP;
    }*/
    if(i>7){
        /*std::default_random_engine generator;
        std::uniform_int_distribution<int> distribution(0,7);
        num1 = distribution(generator);*/
        num1 = i-7;
    }else{
        num1 = i;
    }
    this->customPlot->graph(i)->setPen(colors[num1]);
    this->customPlot->graph(i)->setScatterStyle(scatterStyle[0]);

    return i;
}

void PlotECG:: addStraightLines(const QVector<double> x, int colorRed){
    for(int i=0; i<x.size(); i++){
        QCPItemStraightLine *line = new QCPItemStraightLine(this->customPlot);
        //QCPItemLine *line = new QCPItemLine(customPlot);
        line->point1->setCoords(x[i], 0);  // location of point 1 in plot coordinate
        line->point2->setCoords(x[i], 1);  // location of point 1 in plot coordinate
        if(colorRed == 1){
            line->setPen(QColor("red"));
        }
    }
}

void PlotECG:: addBackgroudHiglight(const QVector<double> x, const QVector<double> y){
    for(int i=0; i<x.size(); i++){
        QCPItemRect *rec = new QCPItemRect(this->customPlot);
        //QCPItemLine *line = new QCPItemLine(customPlot);
        // rec->topLeft->setCoords(x[i], 0);  // location of point 1 in plot coordinate
        rec->topLeft->setCoords(x[i], 1e9);  // location of point 1 in plot coordinate
        // rec->bottomRight->setCoords(y[i], 0);  // location of point 1 in plot coordinate
        rec->bottomRight->setCoords(y[i], -1e9);  // location of point 1 in plot coordinate
        rec->setPen(QPen(QColor(255, 0, 0, 25)));
        rec->setBrush(QBrush(QColor(255, 0, 0, 25)));
    }
}


