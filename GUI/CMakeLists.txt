cmake_minimum_required(VERSION 3.5)

project(GUI LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -pthread")
add_compile_options(-DQT_NO_KEYWORDS)
find_package(Qt5 COMPONENTS Widgets PrintSupport REQUIRED)
find_package(Eigen3 REQUIRED)
find_package(Boost REQUIRED COMPONENTS)
include_directories(
../include
${Boost_INCLUDE_DIRS}
)
add_library(APP SHARED
    ../src/core/IEcgTask.cpp
    ../src/core/WorkflowSupervisor.cpp
    ../src/core/IEcgTaskSpawner.cpp
    ../src/ecg_baseline/EcgBaselineTask.cpp
    ../src/ecg_baseline/Butter.cpp
    ../src/ecg_baseline/SavGol.cpp
    ../src/example_task/ExampleEcgTask.cpp
    ../src/input_system/WFDB.cpp
    ../src/input_system/WfdbWrapper.cpp
    ../src/rpeaks/Rpeaks.cpp
    ../src/hrv1/hrv1.cpp
    ../src/hrv_dfa/HrvDfaTask.cpp
    ../src/waves/Waves.cpp
    ../src/hrv2/HRV2.cpp
    ../src/t_wave_alt/TWaveAltTask.cpp
    ../src/st_segment/STSegmentTask.cpp
    )
add_executable(GUI
  main.cpp
  mainwindow.cpp
  mainwindow.h
  mainwindow.ui
  qcustomplot.cpp
  plotecg.cpp
  plotfft.cpp
  plothist.cpp
  poincare.cpp
)

target_link_libraries(GUI PRIVATE APP Qt5::Widgets Qt5::PrintSupport m fftw3 wfdb gsl gslcblas Eigen3::Eigen stdc++fs)
add_custom_command(TARGET GUI
POST_BUILD
WORKING_DIRECTORY
COMMAND cp -R ${CMAKE_CURRENT_SOURCE_DIR}/../resources ${CMAKE_CURRENT_SOURCE_DIR})
