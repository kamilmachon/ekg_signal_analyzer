#ifndef POINCARE_H
#define POINCARE_H

#include <QMainWindow>
#include "qcustomplot.h"

class Poincare
{
public:
    Poincare(QCustomPlot *customPlot);
    void addPoints(const QVector<double> x, const QVector<double> y);
    void setLogscale(const bool x=false, const bool y=false);

private:
    QCustomPlot *customPlot;

};

#endif // POINCARE_H
