#ifndef ECG_SIGNAL_ANALYZER_HRV2_HPP
#define ECG_SIGNAL_ANALYZER_HRV2_HPP

#include <ecg_signal_analyzer/core/IEcgTask.hpp>
#include <vector>

namespace ecg
{

class HRV2 : public IEcgTask
{
public:
    HRV2() {}
    virtual ~HRV2() {}
    int ProcessData(const InputsType &inputs, const OutputsType &outputs) override;

private:
    std::vector<std::pair<double, int32_t>> GetHistogram(std::vector<double> & timestamp);
    double GetTriangularIndex(std::vector<int32_t> & histogram);
    std::pair<double, double> MaxHistogramPoint(std::vector<int32_t> & histogram);
    std::pair<double, double> GetTINN(std::vector<double> & timestamp, std::vector<int32_t> & histogram, double X);
    std::pair<double, double> GetSDValues(std::vector<double> & timestamp);
    std::vector<std::pair<double, double>> GetPoincare(std::vector<double> & timestamp);
};

} // namespace ecg

#endif