// ExampleEcgTask.hpp
#ifndef ECG_SIGNAL_ANALYZER_ECG_BASELINE_HPP
#define ECG_SIGNAL_ANALYZER_ECG_BASELINE_HPP

#include <ecg_signal_analyzer/core/IEcgTask.hpp>
#include <ecg_signal_analyzer/ecg_baseline/Butter.hpp>
#include <ecg_signal_analyzer/ecg_baseline/SavGol.hpp>

namespace ecg
{

class EcgBaselineTask : public IEcgTask
{
    public:
        EcgBaselineTask();

        virtual ~EcgBaselineTask() {}

        int ProcessData(const InputsType &inputs, const OutputsType &outputs) override;
        int32_t SetBaselineFilterParameters(double cutoff_frequency = 0.5, uint32_t filter_order = 2);
        int32_t FilterBaseline(std::vector<double> & timestamp, std::vector<double> & raw_signal, std::vector<double> & filtered_signal, std::vector<double> & noise);

        int32_t SetSmoothFilterParameters(uint32_t window_size = 21, uint32_t filter_order = 3);
        int32_t SmoothSignal(std::vector<double> & timestamp, std::vector<double> & raw_signal, std::vector<double> & filtered_signal, std::vector<double> & noise);

    private:
        Butter::Butter ButterFilter_;
        SavGol::SavGol SavGolFilter_;
        
};

} // namespace ecg

#endif
