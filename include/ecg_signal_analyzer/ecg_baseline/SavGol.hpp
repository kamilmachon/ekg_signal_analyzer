#ifndef _SAVGOL_H_
#define _SAVGOL_H_

#include <iostream>
#include <vector>
#include <iomanip>
#include <math.h>

namespace SavGol
{
    class SavGol
    {
        public:
            SavGol(uint32_t window_size = 21, uint32_t filter_order = 3);
            ~SavGol() {};
            int32_t SetWindowSize(uint32_t window_size);
            int32_t SetFilterOrder(uint32_t filter_order);
            int32_t FilterSignal(std::vector<double> & timestamp, std::vector<double> & raw_signal, std::vector<double> & filtered_signal, std::vector<double> & noise);
            uint32_t GetWindowSize() {return window_size_;};
            uint32_t GetFilterOrder() {return filter_order_;};
            void GetSavGolCoefficients(std::vector<double> & coeffs);

        private:
            uint32_t window_size_ = 0;
            uint32_t filter_order_ = 0;
            std::vector<double> coeffs_;

            void MatrixCofactor(std::vector<std::vector<double>> & A, std::vector<std::vector<double>> & out, uint32_t p, uint32_t q, uint32_t n);
            double MatrixDeterminant(std::vector<std::vector<double>> & A, uint32_t n);
            void MatrixAdjoint(std::vector<std::vector<double>> & A, std::vector<std::vector<double>> & adj);
            bool MatrixInverse(std::vector<std::vector<double>> & A, std::vector<std::vector<double>> & inverse);
            bool MatrixTranspose(std::vector<std::vector<double>> & A, std::vector<std::vector<double>> & trans);
            bool MatrixMul(std::vector<std::vector<double>> & A, std::vector<std::vector<double>> & B, std::vector<std::vector<double>> & out);
            void CalculateSavGolCoefficients(std::vector<double> & coeffs);
            template<class T> 
            void MatrixDisplay(std::vector<std::vector<T>> & A);
    };

} // namespace SavGol

#endif