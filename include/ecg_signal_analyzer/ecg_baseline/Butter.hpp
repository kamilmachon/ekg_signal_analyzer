#ifndef _Butter_H_
#define _Butter_H_

#include <complex>
#include <iostream>
#include <vector>
#include <fstream>
#include <math.h>
#include <cmath>
#include <algorithm>

namespace Butter
{
    class Butter
    {
        public:
            Butter(double cutoff_frequency = 0.5, uint32_t filter_order = 2);
            ~Butter() {};
            int32_t SetCutoffFrequency(double cutoff_frequency);
            int32_t SetFilterOrder(uint32_t filter_order);
            int32_t FilterSignal(std::vector<double> & timestamp, std::vector<double> & raw_signal, std::vector<double> & filtered_signal, std::vector<double> & noise);
            double GetCutoffFrequency() {return cutoff_frequency_;};
            double GetFilterOrder() {return filter_order_;};

        private:
            double cutoff_frequency_ = 0;
            uint32_t filter_order_ = 0;

            std::vector<double> ComputeDenCoeffs(uint32_t filter_order, double low_cutoff, double high_cutoff);
            std::vector<double> TrinomialMultiply(uint32_t filter_order, std::vector<double> & b, std::vector<double> & c);
            std::vector<double> ComputeNumCoeffs(uint32_t filter_order, double low_cutoff, double high_cutoff, std::vector<double> DenC);
            std::vector<double> ComputeLP(uint32_t filter_order);
            std::vector<double> ComputeHP(uint32_t filter_order);
            void Filter(std::vector<double> & x, std::vector<double> & coeff_b, std::vector<double> & coeff_a, std::vector<double> & filtered_signal);
    };

} // namespace Butter

#endif