#ifndef ECG_SIGNAL_ANALYZER_EXAMPLE_ECG_TASK_HPP
#define ECG_SIGNAL_ANALYZER_EXAMPLE_ECG_TASK_HPP

#include <ecg_signal_analyzer/core/IEcgTask.hpp>

namespace ecg
{

class ExampleEcgTask : public IEcgTask
{
public:
    ExampleEcgTask();

    virtual ~ExampleEcgTask() {}

    int ProcessData(const InputsType &inputs, const OutputsType &outputs) override;

private:
};

} // namespace ecg

#endif