#ifndef ECG_SIGNAL_ANALYZER_RPEAKS_HPP
#define ECG_SIGNAL_ANALYZER_RPEAKS_HPP

#include <ecg_signal_analyzer/core/IEcgTask.hpp>

namespace ecg
{

class Rpeaks : public IEcgTask
{
public:
    Rpeaks() {}

    virtual ~Rpeaks() {}

    int ProcessData(const InputsType &inputs, const OutputsType &outputs) override;

private:
    int32_t DifferentiateSignal(std::vector<double> & signal, std::vector<double> & differentiated_signal);
    int32_t HilbertTransform(std::vector<double> & differentiated_signal, std::vector<double> & hilbert_transformed_signal);
    int32_t AdaptiveThresholding(std::vector<double> & signal, std::vector<double> & hilbert_signal, std::vector<std::pair<double, uint64_t>> & noisy_rpeaks);
    double CalculateThreshold(double rms, double max_amplitude, double previous_max_amplitude);
    int32_t RemoveFalseRpeaks(std::vector<std::pair<double, uint64_t>> & noisy_rpeaks, double time_period);
    double RootMeanSquare(std::vector<double> & signal);
};

} // namespace ecg

#endif
