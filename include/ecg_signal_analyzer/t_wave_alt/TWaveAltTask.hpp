#ifndef ECG_SIGNAL_ANALYZER_T_WAVE_ALT_TASK_HPP
#define ECG_SIGNAL_ANALYZER_T_WAVE_ALT_TASK_HPP

#include <ecg_signal_analyzer/core/IEcgTask.hpp>
#include <gsl/gsl_statistics_double.h>

namespace ecg
{

class TWaveAltTask : public IEcgTask
{
public:
    TWaveAltTask();

    ~TWaveAltTask();

    int ProcessData(const InputsType &inputs, const OutputsType &outputs) override;
    double getAlt(void);

private:
    int LoadInputs(const InputsType &inputs, std::vector<struct TwaHeartBeat> & Beats);
    int ProcessBeat(struct TwaHeartBeat Beat);
    int FindTon(struct TwaHeartBeat *Beat);
    int FindTwaveAmp(struct TwaHeartBeat *Beat);

    int ProcessWave(std::vector<ecg::TimeSeries<double>::Point>::iterator waveStart,
                    std::vector<ecg::TimeSeries<double>::Point>::iterator waveEnd);
    int UpdateMma(int isOdd, std::vector<double> & signal, int TendOffset);
    double ComputeTwa(int TendOffset, int isEaven);
    std::vector<double> mma[2];
    double maxMmaAmp[2];
    int beatsSkipped[2];
    size_t waveNum;
    double TwaValue;
};


/**
 * Struktura przechwująca dane o jednym uderzeniu serca
 * Na jej podstawie dokonywana będzie analiza TWA 
 */
struct TwaHeartBeat
{ 
    std::vector<ecg::TimeSeries<double>::Point>::iterator 
            BeatStartIt;  /**< Iterator wskazujący na początek uderzenia */
    std::vector<ecg::TimeSeries<double>::Point>::iterator 
            BeatEndIt;    /**< Iterator wskazujący na koniec uderzenia */
    std::vector<ecg::TimeSeries<double>::Point>::iterator 
            TonIt;        /**< Iterator wskazujący na punkt T_on */
    std::vector<ecg::TimeSeries<double>::Point>::iterator 
            TendIt;       /**< Iterator wskazujący na punkt T_end */
    std::vector<ecg::TimeSeries<double>::Point>::iterator 
            TwaveAmp;       /**< Iterator wskazujący na punkt T_end */
    double  RrInterval;   /**< RR interwal [s] */
    uint8_t    isEaven;      /**< Identyfikator tego czy uderzenie jest parzyste */
};


} // namespace ecg


#endif //ECG_SIGNAL_ANALYZER_T_WAVE_ALT_TASK_HPP