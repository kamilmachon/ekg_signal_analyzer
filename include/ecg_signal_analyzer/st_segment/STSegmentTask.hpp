// ExampleEcgTask.hpp
#ifndef ECG_SIGNAL_ANALYZER_ST_SEGMENT_HPP
#define ECG_SIGNAL_ANALYZER_ST_SEGMENT_HPP

#include <ecg_signal_analyzer/core/IEcgTask.hpp>
#include <math.h>

namespace ecg
{

class STSegmentTask : public IEcgTask
{
    public:
        STSegmentTask();
        virtual ~STSegmentTask() {}

        int ProcessData(const InputsType &inputs, const OutputsType &outputs) override;

    private:
        uint64_t GetTPointIdx(std::vector<double> & timestamp, std::vector<double> & filtered_signal, uint64_t s_point_idx, uint64_t t_end_idx);
        double GetSTSegmentAngle(std::vector<double> & timestamp, std::vector<double> & filtered_signal, uint64_t s_point_idx, uint64_t t_point_idx);
        
};

} // namespace ecg

#endif
