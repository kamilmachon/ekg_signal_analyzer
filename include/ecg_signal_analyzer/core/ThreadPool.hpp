#ifndef ECG_SIGNAL_ANALYZER_THREAD_POOL_H_
#define ECG_SIGNAL_ANALYZER_THREAD_POOL_H_

#include <iostream>
#include <stdexcept>

#include <vector>
#include <queue>

#include <thread>
#include <atomic>
#include <future>
#include <memory>
#include <functional>
#include <utility>
#include <iterator>


namespace ecg
{

/**
 * Contains implementation for a thread-safe queue and other utility classes not needed to interface with converters.
 */
namespace detail
{

/**
 * Thread-safe queue where jobs are placed for processing.
 */
template <typename T>
class Queue
{
public:
  /** Constructor. */
  Queue() {}

  /** Destructor. */
  virtual ~Queue(){};

  /**
   * Pushes an item to the end of the queue.
   * @param[in] item - item to push
   */
  void Push(T const &item)
  {
    std::unique_lock<std::mutex> lock(mutex_);
    queue_.push(item);
  }

  /**
   * Constructs an item in-place at the end of the queue.
   * @param[in] item - item to emplace
   */
  void Emplace(T const &&item)
  {
    std::unique_lock<std::mutex> lock(mutex_);
    queue_.emplace(item);
  }

  /** Returns a reference to the first object in the queue.*/
  T &Front()
  {
    std::unique_lock<std::mutex> lock(mutex_);
    return queue_.front();
  }

  /** Returns a const reference to the first object in the queue.*/
  const T &Front() const
  {
    std::unique_lock<std::mutex> lock(mutex_);
    return queue_.front();
  }

  /** Removes the item at the front of the queue.*/
  void Pop()
  {
    std::unique_lock<std::mutex> lock(mutex_);
    queue_.pop();
  }

  /**
   * Checks if the queue is empty.
   * @return empty - true if the queue is empty, false if not.
   */
  bool Empty()
  {
    std::unique_lock<std::mutex> lock(mutex_);
    return queue_.empty();
  }

  /**
   * Attempts to retrieve the first item in the queue, then pop it.
   * If there were no elements in the queue, item is not assigned to and false is returned.
   * Otherwise, item is assigned and true is returned.
   * @param[in] item - front will be copied here before it is popped.
   * @return empty - wether the operation was successful.
   */
  bool FrontAndPop(T &item)
  {
    std::unique_lock<std::mutex> lock(mutex_);
    if (queue_.empty())
    {
      return false;
    }
    else
    {
      item = queue_.front();
      queue_.pop();
      return true;
    }
  }

  /** Clears the contents of theq queue.*/
  void Clear()
  {
    while (!queue_.empty())
    {
      queue_.pop();
    }
  }

private:
  /** Container for the items.*/
  std::queue<T> queue_;
  /** Mutex to ensure thread-safety.*/
  std::mutex mutex_;

}; // class Queue

} // namespace detail

/** Implements a thread pool, which can store functions and member functions, along with their arguments, to be executed.*/
class ThreadPool
{

public:
  /** Constructor. Automatic thread count, based on std::thread::hardware_concurrency()*/
  ThreadPool()
  {
    Init();
    Resize(std::thread::hardware_concurrency());
  }

  /**
   * Constructor with thread count provided by user.
   * @param[in] num_threads - how many threads to spawn.
   */
  explicit ThreadPool(int num_threads)
  {
    Init();
    Resize(num_threads);
  }

  /** Destructor.*/
  virtual ~ThreadPool()
  {
    Stop(true);
  }

  /**
   * Returns the number of threads in the pool.
   * @return number of threads.
   */
  int NumThreads() { return static_cast<int>(threads_.size()); }

  /**
   * Returns the number of threads currently waiting for a job.
   * @return number of threads waiting for a job.
   */
  int NumWaiting() { return num_threads_waiting_; }

  /**
   * Returns a reference to the i-th thread.
   * param[in] i - index of the thread.
   */
  std::thread &GetThread(size_t i)
  {
    if (i >= 0 && i < threads_.size())
    {
      return *threads_[i];
    }
    else
    {
      throw std::invalid_argument("\'i\' must be between 0 and Size().");
    }
  }

  /**
   * Adds a member function with arguments to the queue.
   * @param[in] f_ptr - member function pointer.
   * @param[in] obj - pointer to the object instance.
   * @param[in] args - arguments for the member function.
   * @return future - std::future object that will hold the return value of the member function.
   */
  template <typename T, typename F, typename... Args> // T - Object instance. F - Member function pointer. Args - Member function arguments.
  auto AddJob(F &&f_ptr, T *obj, Args &&... args)
  {
    auto task = std::make_shared<std::packaged_task<decltype((obj->*f_ptr)(args...))()>>(std::bind(std::forward<F>(f_ptr), std::forward<T *>(obj), std::forward<Args>(args)...));
    queue_.Emplace(std::make_shared<std::function<void()>>([task] { (*task)(); }));
    std::unique_lock<std::mutex> lock(mutex_);
    new_jobs_.notify_one();
    return task->get_future();
  }


  /**
   * Adds a function with arguments to the queue.
   * @param[in] f_ptr - function pointer.
   * @param[in] args - arguments for the function.
   * @return future - std::future object that will hold the return value of the function.
   */
  template <typename F, typename... Args>
  auto AddJob(F &&f_ptr, Args &&... args)
  {
    auto task = std::make_shared<std::packaged_task<decltype((*f_ptr)(args...))()>>(std::bind(std::forward<F>(f_ptr), std::forward<Args>(args)...));
    queue_.Emplace(std::make_shared<std::function<void()>>([task] { (*task)(); }));
    std::unique_lock<std::mutex> lock(mutex_);
    new_jobs_.notify_one();
    return task->get_future();
  }

  /**
   * Changes the number of threads in the pool.
   * @param[in] num_threads - new number of threads.
   */
  void Resize(int num_threads)
  {
    if (stop_ || done_ || num_threads < 0)
      return;

    int prev_num_threads = static_cast<int>(threads_.size());
    std::cout << "Resizing from " << prev_num_threads << " to " << num_threads << " threads.\n";

    if (num_threads > prev_num_threads) // increase number of threads.
    {
      threads_.resize(num_threads);
      flags_.resize(num_threads);

      for (int i = prev_num_threads; i < num_threads; ++i)
      {
        flags_[i] = std::make_shared<std::atomic<bool>>(false);
        StartThread(i);
      }
    }
    else // decrease number of threads.
    {
      for (int i = num_threads; i < prev_num_threads; ++i)
      {
        *flags_[i] = true;     // ask thread to stop.
        threads_[i]->detach(); // detach thread so that the vector can be safely resized.
      }
      {
        std::unique_lock<std::mutex> lock(mutex_);
        new_jobs_.notify_all(); // notify threads that were waiting (make sure they respond to their flag)
      }

      threads_.resize(num_threads); // safe to delete pointers because threads were detached.
      flags_.resize(num_threads);   // threads have copies of pointers to flags, so safe to delete.
    }
  }

  /** Clears the job queue.*/
  void ClearQueue()
  {
    queue_.Clear();
  }

  /**
   * Stops the thread pool.
   * If <tt>process_queue</tt> is set, all jobs in the queue will be called first.
   * Otherwise, only jobs in progress will finish. Any waiting jobs will be discarded.
   * @param[in] process_queue - wether to process remaining jobs.
   */
  void Stop(bool process_queue)
  {
    if (!process_queue)
    {
      if (stop_)
        return;

      stop_ = true;
      for (size_t i = 0; i < flags_.size(); ++i)
      {
        *flags_[i] = true; // ask all threads to stop.
      }
      ClearQueue();
    }
    else
    {
      if (done_ || stop_)
        return;
      done_ = true; // set the command for all waiting threads to stop.
    }
    {
      std::unique_lock<std::mutex> lock(mutex_);
      new_jobs_.notify_all(); // stop all waiting threads.
    }
    for (int i = 0; i < static_cast<int>(threads_.size()); ++i) // wait for all computing threads to finish.
    {
      if (threads_[i]->joinable())
      {
        threads_[i]->join();
      }
    }

    ClearQueue();
    threads_.clear();
    flags_.clear();
  }

private:
  /** Initializes the thread pool.*/
  void Init()
  {
    stop_ = false;
    done_ = false;
    num_threads_waiting_ = 0;
  }

  /**
   * Starts the i-th thread in the pool.
   * param[in] i - index of the thread to start.
   */
  void StartThread(int i) // Start i-th thread.
  {
    std::shared_ptr<std::atomic<bool>> this_flag(flags_[i]);

    auto f = [this, i, this_flag]() {
      std::atomic<bool> &flag = *this_flag;
      std::shared_ptr<std::function<void()>> job;
      bool popped = queue_.FrontAndPop(job);
      while (true)
      {
        while (popped) // Keep processing while there are items in the queue.
        {
          (*job)();
          if (flag)
          {
            return; // Stop the thread if commanded.
          }
          else
          {
            popped = queue_.FrontAndPop(job);
          }
        }

        std::unique_lock<std::mutex> lock(mutex_);
        ++num_threads_waiting_;
        new_jobs_.wait(lock, [this, &job, &popped, &flag]() { popped = queue_.FrontAndPop(job); return popped || done_ || flag; });
        --num_threads_waiting_;
        if (!popped)
        {
          return; // if queue is empty, the wake-up was caused by done_ or by flag, so the thread is expected to stop.
        }
      }
    };

    threads_[i].reset(new std::thread(f));
  }

  /** Container for the threads.*/
  std::vector<std::unique_ptr<std::thread>> threads_;

  /** Container for flags used to stop the threads.*/
  std::vector<std::shared_ptr<std::atomic<bool>>> flags_; // tell threads to stop

  /** Thread-safe queue used to store waiting jobs.*/
  detail::Queue<std::shared_ptr<std::function<void()>>> queue_; // store incoming jobs

  /** Used to tell all threads to exit.*/
  std::atomic<bool> done_; //
  /** True if Stop() was called. Prevents Stop() from working multiple times.*/
  std::atomic<bool> stop_;

  /** mutex corresponding to the new_jobs_ condition variable */
  std::mutex mutex_;
  /** Used to notify threads of new jobs or to wake them up for exit.*/
  std::condition_variable new_jobs_;

  /** Number of threads that are currently not processing a job.*/
  std::atomic<unsigned int> num_threads_waiting_;
}; // class ThreadPool

} // ecg

#endif 
