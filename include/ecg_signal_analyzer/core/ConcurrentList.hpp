#ifndef ECG_SIGNAL_ANALYZER_CONCURRENT_LIST_HPP
#define ECG_SIGNAL_ANALYZER_CONCURRENT_LIST_HPP

#include <list>

namespace ecg
{
namespace detail
{

template <typename T>
class ConcurrentList
{
public:
  ConcurrentList() = default;
  ConcurrentList(const ConcurrentList &) = default;
  ConcurrentList(ConcurrentList &&) = default;
  ConcurrentList &operator=(const ConcurrentList &) = default;
  ConcurrentList &operator=(ConcurrentList &&) = default;
  ~ConcurrentList() {}

  void PushBack(const T &t)
  {
    std::lock_guard<std::mutex> lg(mx_);
    list_.push_back(t);
  }

  void PushFront(const T &t)
  {
    std::lock_guard<std::mutex> lg(mx_);
    list_.push_front(t);
  }

  void Clear()
  {
    std::lock_guard<std::mutex> lg(mx_);
    list_.clear();
  }

  auto Front()
  {
    std::lock_guard<std::mutex> lg(mx_);
    return list_.front();
  }

  auto Back()
  {
    std::lock_guard<std::mutex> lg(mx_);
    return list_.back();
  }

  void PopFront()
  {
    std::lock_guard<std::mutex> lg(mx_);
    list_.pop_front();
  }

  void PopBack()
  {
    std::lock_guard<std::mutex> lg(mx_);
    list_.pop_back();
  }

private:
  std::mutex mx_;
  std::list<T> list_;
};

} // namespace detail
} // namespace ecg

#endif