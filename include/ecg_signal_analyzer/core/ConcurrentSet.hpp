#ifndef ECG_SIGNAL_ANALYZER_CONCURRENT_SET_HPP
#define ECG_SIGNAL_ANALYZER_CONCURRENT_SET_HPP

#include <set>
#include <mutex>
#include <utility>
#include <iterator>

namespace ecg
{
namespace detail
{
template <typename T>
class ConcurrentSet
{
public:
    auto Insert(const T &item)
    {
        std::unique_lock<std::mutex> lock(mutex_);

        return set_.insert(item);
    }

    bool Empty()
    {
        std::unique_lock<std::mutex> lock(mutex_);
        return set_.empty();
    }

    auto Erase(const T &item)
    {
        std::unique_lock<std::mutex> lock(mutex_);
        return set_.erase(item);
    }

    auto begin()
    {
        std::unique_lock<std::mutex>(mutex_);
        return set_.begin();
    }

    auto end()
    {
        std::unique_lock<std::mutex>(mutex_);

        return set_.end();
    }

    auto cbegin()
    {
        std::unique_lock<std::mutex>(mutex_);

        return set_.cbegin();
    }

    auto cend()
    {
        std::unique_lock<std::mutex>(mutex_);

        return set_.cend();
    }

private:
    std::mutex mutex_;
    std::set<T> set_;
};

} // namespace detail
} // namespace ecg

#endif