#ifndef ECG_SIGNAL_ANALYZER_WORKFLOW_SUPERVISOR_HPP
#define ECG_SIGNAL_ANALYZER_WORKFLOW_SUPERVISOR_HPP
#include <ecg_signal_analyzer/core/IEcgTask.hpp>
#include <ecg_signal_analyzer/core/EcgData.hpp>
#include <ecg_signal_analyzer/example_task/ExampleEcgTask.hpp>
#include <ecg_signal_analyzer/core/IInputModule.hpp>

#include <chrono>
#include <map>

namespace ecg
{

class IInputModule;

class WorkflowSupervisor
{
public:
    /** Constructor. */
    WorkflowSupervisor();

    /** Destructor. */
    explicit WorkflowSupervisor(std::string config_path);

    /**
     * @brief Set the Loader object.
     * 
     * Sets the reference of IInputLoader to use while loading database files.
     * @param loader Loader to set.
     */
    void SetLoader(const std::shared_ptr<IInputModule> &loader);

    /**
     * @brief Get the Loader object
     * 
     * @return const std::shared_ptr<IInputModule>& The loader object. 
     */
    const std::shared_ptr<IInputModule> &GetLoader() const;

    void Load(const std::string &path);

    /**
     * @brief Triggers the execution graph.
     * 
     * This function will fetch the portions of data indicated by 'first' and 'last' and feed them into appropriate tesks.
     * The tasks are run on all available threads. The function blocks and returns once all tasks are complete.
     * @param first The lower bound of the time range.
     * @param last The upper bound of the time range.
     */
    void Run(uint64_t first = 0, uint64_t last = std::numeric_limits<uint64_t>::max());

    /**
     * @brief Load the exection graph from the XML config.
     * 
     * This will spawn the tasks specified and construct the execution graph.
     * @param config_path Path to the configuraion file.
     * @throw std::runtime_error if the file fails to load or if it contains errors.
     */
    void LoadExecutionGraph(std::string config_path);

    /**
     * @brief Get the inputs for a specific task, clipped to the specified time range.
     * 
     * @param task_name The name of the task, as provided in the XML config.
     * @param first The lower bound of the time range.
     * @param last The upper bound of the time range.
     */
    auto GetTaskInputs(std::string task_name, uint64_t first, uint64_t last)
        -> std::unordered_map<std::string, decltype(((TimeSeries<double> *)nullptr)->Range(0, 0))>;

    /**
     * @overload ecg::WorkflowSupervisor::GetTaskInputs(std::string task_name, uint64_t first, uint64_t last)
     */
    auto GetTaskInputs(IEcgTaskPtr task, uint64_t first, uint64_t last)
        -> std::unordered_map<std::string, decltype(((TimeSeries<double> *)nullptr)->Range(0, 0))>;

    /**
     * @brief Get the outputs for a specific task, clipped to the specified time range.
     * 
     * @param task_name The name of the task, as provided in the XML config.
     * @param first The lower bound of the time range.
     * @param last The upper bound of the time range.
     */
    auto GetTaskOutputs(std::string task_name)
        -> std::unordered_map<std::string, std::reference_wrapper<TimeSeries<double>>>;

    /**
     * @overload  GetTaskOutputs(std::string task_name)
     */
    auto GetTaskOutputs(IEcgTaskPtr task)
        -> std::unordered_map<std::string, std::reference_wrapper<TimeSeries<double>>>;

    /**
     * @brief Get the pointer to the loaded/calculated data.
     * 
     * @return const EcgDataPtr& Pointer to the data object.
     */
    const EcgDataPtr &Data();

    /**
     * @brief Clear processing data froma time range.
     * 
     * @param first The lower bound of the time range.
     * @param last The upper bound of the time range.
     */
    void ResetRange(uint64_t first, uint64_t last);

    auto Tasks()
    {
        return tasks_;
    }

private:
    EcgDataPtr data_ = std::make_shared<EcgData>();

    IEcgTaskPtr root_task_;

    std::unordered_map<std::string, IEcgTaskPtr> tasks_;

    std::shared_ptr<IInputModule> loader_;
};

} // namespace ecg

#endif