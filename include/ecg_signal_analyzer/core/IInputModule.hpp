#ifndef ECG_SIGNAL_ANALYZER_I_INPUT_MODULE_HPP
#define ECG_SIGNAL_ANALYZER_I_INPUT_MODULE_HPP

#include <memory>
#include <vector>
#include <string>

namespace ecg
{
struct EcgData;

/**
 * @brief Defines the interface for an input module to be used by ecg.
 */
class IInputModule
{
public:
    /**
     * @brief Destructor.
     */
    virtual ~IInputModule() = default;

    /**
     * @brief Load the data from file and return it in a shared_ptr.
     * 
     * @return std::shared_ptr<EcgData> Pointer to the loaded data.
     */
    virtual std::shared_ptr<EcgData> Load(const std::string &) = 0;
};

} // namespace ecg

#endif