#ifndef ECG_SIGNAL_ANALYZER_I_ECG_TASK_HPP
#define ECG_SIGNAL_ANALYZER_I_ECG_TASK_HPP

#include <vector>
#include <set>
#include <memory>
#include <unordered_map>
#include <condition_variable>
#include <any>

#include <ecg_signal_analyzer/core/ThreadPool.hpp>
#include <ecg_signal_analyzer/core/ConcurrentSet.hpp>
#include <ecg_signal_analyzer/core/TimeSeries.hpp>
namespace ecg
{

// Typedefs

typedef std::unordered_map<std::string, decltype(((TimeSeries<double> *)nullptr)->Range(0, 0))> InputsType;
typedef std::unordered_map<std::string, std::reference_wrapper<TimeSeries<double>>> OutputsType;

// Forward declaration
struct EcgData;
class IEcgTaskVisitor;
typedef std::shared_ptr<EcgData> EcgDataPtr;
typedef std::shared_ptr<const EcgData> EcgDataConstPtr;

// Forward declaration
class IEcgTask;
typedef std::shared_ptr<IEcgTask> IEcgTaskPtr;
typedef std::shared_ptr<const IEcgTask> IEcgTaskConstPtr;

/**
 * @brief Interface class for ECG processing modules.
 * 
 * All modules should derive from this class and implement the ProcessData function.
 */
class IEcgTask : public std::enable_shared_from_this<IEcgTask>
{
public:
    /**
     * @brief Perform asynchronous processing of data on a thread pool.
     * 
     * @param inputs Names of signals mapped to iterator pairs containing the specified range of a signal.
     * @param outputs Names of output signals mapped to reference wrappers for thos signals.
     * @param tasks_left Queue containing the tasks that still need to be processed. The children of this instance will be added to it.
     * @param tasks_done A set of tasks that have already been completed. This instance will add itself to it once it is done.
     * @param wake_up A condition variable used to wake the WorkflowSupervisor.
     * @param wake_up_mutex a mutex securing access to the condition variable.
     * @return std::future<int> The return code of ProcessData.
     */
    std::future<int> RunAsync(const InputsType &inputs,
                              const OutputsType &outputs,
                            //   detail::Queue<IEcgTaskPtr> &tasks_left,
                              detail::ConcurrentSet<IEcgTaskPtr> &tasks_left,
                              detail::ConcurrentSet<IEcgTaskPtr> &tasks_done,
                              std::condition_variable &wake_up,
                              std::mutex &wake_up_mutex);

    int Run(const InputsType &inputs, const OutputsType &outputs);

    /**
     * @brief The function that deriving tasks must implement.
     * 
     * This is where the task acutally processes its data.
     * 
     * @param inputs Names of signals mapped to iterator pairs containing the specified range of a signal.
     * @param outputs Names of output signals mapped to reference wrappers for thos signals.
     * @return int Return code. Should be 0 if successful.
     */
    virtual int ProcessData(const InputsType &inputs, const OutputsType &outputs) = 0;

    /**
     * @brief Get the children of this task,
     * 
     * @return const std::set<IEcgTaskPtr>& The set containing pointers to the task's children.
     */
    const std::set<IEcgTaskPtr> &GetChildren() const;

    /**
     * @brief Get the parameter by its name, casting it to the proper type.
     * 
     * @tparam T The type to cast the parameter to.
     * @param key The name of the parameter.
     * @return T The parameter.
     */
    template <typename T>
    T GetParameter(const std::string &key)
    {
        return std::any_cast<T>(task_parameters_[key]);
    }

    /**
     * @brief Add a parameter to the task.
     * 
     * @param key The name of the parameter.
     * @param value The value of the parameter.
     */
    void AddParameter(const std::string &key, const std::any &value);

    /**
     * @brief Get the name of the task.
     * 
     * @return const std::string& The name of the task.
     */
    const std::string &Name()
    {
        return name_;
    }

protected:
    std::unordered_map<std::string, std::any> task_parameters_;

private:
    void AddChild(IEcgTaskPtr);

    std::string name_;

    std::set<IEcgTaskPtr> children_;

    std::weak_ptr<IEcgTask> parent_;

    static std::unique_ptr<ThreadPool> thread_pool_;

    std::vector<std::string> inputs_;

    std::vector<std::string> outputs_;

    friend class WorkflowSupervisor;
};

} // namespace ecg

#endif
