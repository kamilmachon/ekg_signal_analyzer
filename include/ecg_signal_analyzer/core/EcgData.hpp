#ifndef ECG_SIGNAL_ANALYZER_ECG_DATA_HPP
#define ECG_SIGNAL_ANALYZER_ECG_DATA_HPP

// Custom
#include <ecg_signal_analyzer/core/TimeSeries.hpp>
#include <ecg_signal_analyzer/core/ConcurrentHashMap.hpp>

// STL
#include <vector>
#include <unordered_map>

// Other
#include <string>


namespace ecg
{

struct EcgData final
{
    ConcurrentHashMap<std::string, TimeSeries<double>> signals;

    void Clear(uint64_t first, uint64_t last)
    {
        for (auto & s : signals)
        {
            s.second.Clear(first, last);
        }
    }
};

} // namespace ecg

#endif