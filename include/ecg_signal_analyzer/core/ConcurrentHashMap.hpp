#ifndef ECG_SIGNAL_ANALIZER_CONCURRENT_HASH_MAP_HPP
#define ECG_SIGNAL_ANALIZER_CONCURRENT_HASH_MAP_HPP

#include <mutex>
#include <unordered_map>

namespace ecg
{

template <typename Key, typename T>
class ConcurrentHashMap
{

public:
    T &operator[](const Key &key)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        return map_.try_emplace(key).first->second;
    }

    void Insert(const Key &key, const T &value)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        map_[key] = value;
    }

    auto Get(const Key &key)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        auto find_value = map_.find(key);
        if (find_value == map_.end())
        {
            throw std::invalid_argument("Signal not found.");
        }
        return find_value->second;
    }

    void Erase(const Key &key)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        auto find_value = map_.find(key);
        if (find_value == map_.end())
        {
            throw std::invalid_argument("signal not found");
        }
        map_.erase(key);
    }

    size_t Size()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        return map_.size();
    }

    auto begin()
    {
        return map_.begin();
    }

    auto rbegin()
    {
        return map_.rbegin();
    }

    auto cbegin()
    {
        return map_.cbegin();
    }

    auto crbegin()
    {
        return map_.crbegin();
    }

    auto end()
    {
        return map_.end();
    }

    auto rend()
    {
        return map_.rend();
    }

    auto cend()
    {
        return map_.cend();
    }

    auto crend()
    {
        return map_.crend();
    }

private:
    std::mutex mutex_;
    std::unordered_map<Key, T> map_;
};

} // namespace ecg

#endif