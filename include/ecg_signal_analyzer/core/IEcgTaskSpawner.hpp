#ifndef ECG_SIGNAL_ANALYZER_IECG_TASK_SPAWNER_HPP
#define ECG_SIGNAL_ANALYZER_IECG_TASK_SPAWNER_HPP

#include <memory> // std::shared_ptr

namespace ecg
{

// Forward Declarations

class IEcgTask;

/**
 * @brief Class used to spawn tasks based on their names passed as a string.
 * 
 * The REGISTER macro in the .cpp file is used to register created tasks.
 */
class IEcgTaskSpawner
{
public:
  /**
   * @brief Spawn a task based on its name.
   * 
   * @param task The name of the task.
   * @return std::shared_ptr<IEcgTask> Pointer to the spawned task.
   */
  static std::shared_ptr<IEcgTask> Spawn(std::string task);
};

} // namespace ecg

#endif
