#ifndef ECG_SIGNAL_ANALYZER_TIME_SERIES_HPP
#define ECG_SIGNAL_ANALYZER_TIME_SERIES_HPP

// STL
#include <vector>
#include <utility>   // std::pair
#include <algorithm> // std::find_if
#include <iterator>

// Other
#include <cstdint> // integer types

namespace ecg
{

/**
 * @brief A structure containing a collectrion of timestamped values.
 * The timestamp should be encoded as milliseconds since the UNIX epoch (01.01.1970).
 * @tparam T Type of the values.
 */
template <typename T>
struct TimeSeries
{
  static_assert(std::is_arithmetic<T>::value);

  /**
   * @brief Represents a single timestamped value.
   */
  struct Point
  {
    /** The value. */
    T value;
    /** The timestamp. */
    uint64_t time;
  };

  /** Stores the stamped values. */
  std::vector<Point> points;

  /**
   * @brief Returns the range of values with timestamps specified by 'first' and 'last'
   * 
   * @param first Earliest timestamp to find.
   * @param last Latest timestamp to find.
   * @return A pair of iterators. The first iterator points to the first value whose timestamp is NOT SMALLER than 'first'.
   * The second iterator points to the first value whose timestamp is LARGER than 'last'.
   */
  auto Range(uint64_t first, uint64_t last)
  {
    auto first_it = std::find_if(begin(points), end(points), [first](const Point &point) {
      return point.time >= first;
    });

    auto last_it = std::find_if(first_it, end(points), [last](const Point &point) {
      return point.time > last;
    });

    return std::make_pair(first_it, last_it);
  }

  /**
   * @brief Find a data point using a timestsamp.
   * 
   * @param time The timestamp to find.
   * @return  An iterator pointing to the first sample whose timestamp is NOT SMALLER than 'time'.
   */
  auto Find(uint64_t time)
  {
    return std::find_if(begin(points), end(points), [time](const Point &point) {
      return point.time >= time;
    });
  }

  /**
   * @overload ecg::TimeSeries::Find(uint64_t)
   */
  auto Find(uint64_t time, typename std::vector<Point>::iterator &first)
  {
    return std::find_if(first, end(points), [time](const Point &point) {
      return point.time >= time;
    });
  }

  /**
   * @overload ecg::TimeSeries::Find(uint64_t)
   */
  auto Find(uint64_t time, size_t offset)
  {
    return std::find_if(begin(points) + offset, end(points), [time](const Point &point) {
      return point.time >= time;
    });
  }

  /**
   * @brief Clear a range of values from the signal.
   * 
   * @param first Earliest timestamp to clear.
   * @param last Latest timestamp to clear.
   */
  void Clear(uint64_t first, uint64_t last)
  {
    auto range = Range(first, last);
    points.erase(range.first, range.second);
  }
};

} // namespace ecg

#endif