#ifndef ECG_SIGNAL_ANALYZER_HRV1_HPP
#define ECG_SIGNAL_ANALYZER_HRV1_HPP

#include <ecg_signal_analyzer/core/IEcgTask.hpp>

namespace ecg
{

class HRV1 : public IEcgTask
{
public:
    HRV1();

    virtual ~HRV1() {}
	
	//Function to process data from Rpeaks module
    int ProcessData(const InputsType &inputs, const OutputsType &outputs) override;

private:
	//Compute time analysis parameters of input data
	void HRV1_time_analysis(std::vector<double> &data_in, std::vector<double> &mean_out, std::vector<double> &std_out,  std::vector<double> &rmssd_out, std::vector<double> &nn50_out,  std::vector<double> &pnn50_out);

	//Compute mean of input data
	double compute_mean(std::vector<double> &data_in);

	//Compute std of input data
	double compute_std(std::vector<double> &data_in);

	//Resample input data
	void resample(std::vector<double> &data_in, std::vector<double> &time_in, std::vector<double> &data_out, std::vector<double> &time_out);

	//Compute FFT of input data
	void doFFT(std::vector<double> &data_in, std::vector<double> &magnitude, std::vector<double> &frequency);

	//Smooth data with average filter
	void avgFilter(std::vector<double> &data_in, std::vector<double> &data_out, int window_size);

	//Compute frequency analysis parameters of input data
	void HRV1_freq_analysis(std::vector<double> &RR_in, std::vector<double> &time_in, std::vector<double> &hf_out, std::vector<double> &lf_out, std::vector<double> &vlf_out, std::vector<double> &ulf_out, std::vector<double> &tp_out, std::vector<double> &lfhf_out);
};

} // namespace ecg

#endif
