#ifndef ECG_SIGNAL_ANALYZER_WAVES_HPP
#define ECG_SIGNAL_ANALYZER_WAVES_HPP

#include <ecg_signal_analyzer/core/IEcgTask.hpp>
#include <vector>
namespace ecg
{

class Waves : public IEcgTask
{
public:
    Waves();
    virtual ~ Waves() {}
    int ProcessData(const InputsType &inputs, const OutputsType &outputs) override;

private:
    std::vector<std::pair<double, double>> Gradient(std::vector<std::pair<double, double>> signal);
    std::vector<std::pair<double, double>> AverageFilter(std::vector<std::pair<double, double>> &signal);
};

} // namespace ecg

#endif