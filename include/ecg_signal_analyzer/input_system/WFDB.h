#ifndef ECG_SIGNAL_ANALYZER_WFDB_H
#define ECG_SIGNAL_ANALYZER_WFDB_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <wfdb/wfdb.h>
#include <ecg_signal_analyzer/core/EcgData.hpp>
#include <ecg_signal_analyzer/core/IEcgTask.hpp>
#include <ecg_signal_analyzer/core/TimeSeries.hpp>
#include <unordered_map>
#include <condition_variable>
#include <any>

#include <time.h>
#include <regex>

namespace input_system
{

class WFDB
{
public:
	struct InputData
	{
		std::vector<int> val1;
		std::vector<int> val2;
		std::vector<std::string> time;
	};

private:
	WFDB_Siginfo *signals;
	FINT len;
	std::shared_ptr<ecg::EcgData> ecg_data_;
	uint64_t db_size;
	uint64_t freq;

public:
	WFDB();
	// WFDB(std::shared_ptr<ecg::EcgData> data); // ecg_data_ removed, read_all() returns a shared_ptr instead.
	WFDB(std::string path);
	~WFDB();

	bool open(std::string path);
	// std::string WFDB::normalizeTime(std::string_view time) const;
	// time_t WFDB::parseTime(std::string_view time) const;
	std::shared_ptr<ecg::EcgData> read_all() const;

	
};
} // namespace input_system


#endif //ECG_SIGNAL_ANALYZER_INPUT_SYSTEM_H