#ifndef ECG_SIGNAL_ANALYZER_WFDB_WRAPPER_H
#define ECG_SIGNAL_ANALYZER_WFDB_WRAPPER_H

#include <ecg_signal_analyzer/core/IInputModule.hpp>

namespace ecg
{
struct EcgData;

class WfdbWrapper : public IInputModule
{
public:
  WfdbWrapper() = default;
  WfdbWrapper(const WfdbWrapper &) = default;
  WfdbWrapper(WfdbWrapper &&) = default;

  WfdbWrapper &operator=(const WfdbWrapper &) = default;
  WfdbWrapper &operator=(WfdbWrapper &&) = default;

  virtual ~WfdbWrapper();
  std::shared_ptr<EcgData> Load(const std::string &);
};

} // namespace ecg

#endif