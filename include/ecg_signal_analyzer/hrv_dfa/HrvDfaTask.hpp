// HrvDfaTask.hpp
#ifndef ECG_SIGNAL_ANALYZER_HRV_DFA_TASK_HPP
#define ECG_SIGNAL_ANALYZER_HRV_DFA_TASK_HPP

#include <ecg_signal_analyzer/core/IEcgTask.hpp>

namespace ecg
{

class HrvDfaTask : public IEcgTask
{
public:
    HrvDfaTask();

    virtual ~HrvDfaTask() {}

    int ProcessData(const InputsType &inputs, const OutputsType &outputs) override;

private:
    void polyfit(const std::vector<double> &xv, const std::vector<double> &yv, std::vector<double> &coeff);
    void polyval(const std::vector<double> &xv, std::vector<double> &yv, std::vector<double> &coeff);
};

} // namespace ecg

#endif
