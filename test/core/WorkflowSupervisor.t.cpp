#include <gtest/gtest.h>

#include <ecg_signal_analyzer/core/WorkflowSupervisor.hpp>
#include <ecg_signal_analyzer/core/TimeSeries.hpp>

#include <iostream>

TEST(WorkflowSupervisorTests, GetTaskInputs)
{
  ecg::WorkflowSupervisor wf("resources/tests_config.xml"); // Load default config file.

  auto data = wf.Data();

  data->signals["RawData"] = ecg::TimeSeries<double>{{{0.0, 0},
                                                      {1.0, 1},
                                                      {2.0, 2},
                                                      {3.0, 3},
                                                      {4.0, 4},
                                                      {5.0, 5}}};

  auto inputs = wf.GetTaskInputs("ECG_Baseline", 1, 3);
  auto raw_data = inputs.find("RawData");

  ASSERT_FALSE(raw_data == end(inputs)) << "GetTaskInputs did not place 'RawData' in the hash map.";

  EXPECT_EQ(raw_data->second.first->time, 1);
  EXPECT_EQ(raw_data->second.first->value, 1.0);

  EXPECT_EQ(raw_data->second.second->time, 4);
  EXPECT_EQ(raw_data->second.second->value, 4.0);
}

TEST(WorkflowSupervisorTests, GetTaskOutputs)
{
  ecg::WorkflowSupervisor wf("resources/tests_config.xml"); // Load default config file.

  auto data = wf.Data();

  data->signals["RawData"] = ecg::TimeSeries<double>{{{0.0, 0},
                                                      {1.0, 1},
                                                      {2.0, 2},
                                                      {3.0, 3},
                                                      {4.0, 4},
                                                      {5.0, 5}}};

  auto outputs = wf.GetTaskOutputs("ECG_Baseline");

  auto ecg_baseline = outputs.find("EcgBaseline");

  ASSERT_FALSE(ecg_baseline == end(outputs)) << "GetTaskOutputs did not place 'EcgBaseline' in the hash map.";

  auto &signal = ecg_baseline->second.get();
  signal.points.push_back(ecg::TimeSeries<double>::Point{2137.0, 2137});

  ASSERT_EQ(data->signals["EcgBaseline"].points.size(), 1) << "Point not added properly to 'EcgBaseline'";
  EXPECT_EQ(data->signals["EcgBaseline"].points[0].time, 2137);
  EXPECT_EQ(data->signals["EcgBaseline"].points[0].value, 2137.0);
}

TEST(WorkflowSupervisorTests, ResetRange)
{
  ecg::WorkflowSupervisor wf("resources/tests_config.xml"); // Load default config file.

  auto data = wf.Data();

  // This should not be reset.
  data->signals["RawData"] = ecg::TimeSeries<double>{{{0.0, 0},
                                                      {1.0, 1},
                                                      {2.0, 2},
                                                      {3.0, 3},
                                                      {4.0, 4},
                                                      {5.0, 5}}};

  // This should
  data->signals["OtherData"] = ecg::TimeSeries<double>{{{0.0, 0},
                                                        {1.0, 1},
                                                        {2.0, 2},
                                                        {3.0, 3},
                                                        {4.0, 4},
                                                        {5.0, 5}}};

  wf.ResetRange(1, 2);
  EXPECT_EQ(data->signals["RawData"].points.size(), 6);
  EXPECT_EQ(data->signals["RawData"].points[1].time, 1);
  EXPECT_EQ(data->signals["RawData"].points[1].value, 1.0);

  EXPECT_EQ(data->signals["OtherData"].points.size(), 4);
  EXPECT_EQ(data->signals["OtherData"].points[1].time, 3);
  EXPECT_EQ(data->signals["OtherData"].points[1].value, 3.0);
}

TEST(WorkflowSupervisorTests, LoadNonExistentXml)
{
  using namespace ecg;
  WorkflowSupervisor w;
  try
  {
    w.LoadExecutionGraph("non-existent-file");
  }
  catch (std::runtime_error &e)
  {
    SUCCEED() << "Properly caught runtime error.";
    return;
  }
  catch (...)
  {
    FAIL() << "Unexpected exception.";
    return;
  }

  FAIL() << "Failed to catch runtime error.";
}

TEST(WorkflowSupervisorTests, LoadIncorrectXml)
{
  using namespace ecg;
  WorkflowSupervisor w;
  try
  {
    w.LoadExecutionGraph("resources/incorrect_config.xml");
  }
  catch (std::runtime_error &e)
  {
    SUCCEED() << "Properly caught runtime error.";
    return;
  }
  catch (...)
  {
    FAIL() << "Unexpected exception.";
    return;
  }

  FAIL() << "Failed to catch runtime error.";
}