#include <gtest/gtest.h>

#include <ecg_signal_analyzer/core/TimeSeries.hpp>

#include <iostream>

using namespace ecg;

TEST(TimeSeriesTests, RangeTest)
{
  TimeSeries<double> ts;
  ts.points = {
      {0.0, 0},
      {1.0, 1},
      {2.0, 2},
      {3.0, 3},
      {4.0, 4},
      {5.0, 5},
  };

  auto result = ts.Range(1, 4);

  EXPECT_TRUE(result.first == begin(ts.points) + 1) << "Wrong first element returned.";
  EXPECT_TRUE(result.second == begin(ts.points) + 5) << "Wrong pas-last element returned.";
}

TEST(TimeSeriesTests, FindTest)
{
  TimeSeries<double> ts;
  ts.points = {
      {0.0, 0},
      {1.0, 1},
      {2.0, 2},
      {3.0, 3},
      {4.0, 4},
      {5.0, 5},
  };

  auto result = ts.Find(2);
  EXPECT_EQ(result->value, 2.0);

  result = ts.Find(-1);
  EXPECT_EQ(result->value, 0.0);

  result = ts.Find(5);
  EXPECT_EQ(result->value, 5.0);

  result = ts.Find(10);
  EXPECT_EQ(result, end(ts.points));
}

TEST(TimeSeriesTests, ClearTest)
{
  TimeSeries<double> ts;
  ts.points = {
      {0.0, 0},
      {1.0, 1},
      {2.0, 2},
      {3.0, 3},
      {4.0, 4},
      {5.0, 5},
  };

  ts.Clear(1, 2);

  EXPECT_EQ(ts.points[0].value, 0.0);
  EXPECT_EQ(ts.points[1].value, 3.0);
  EXPECT_EQ(ts.points.size(), 4);
}