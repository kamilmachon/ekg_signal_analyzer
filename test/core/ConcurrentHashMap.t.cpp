#include <gtest/gtest.h>

#include <ecg_signal_analyzer/core/ConcurrentHashMap.hpp>

#include <vector>
#include <thread>
#include <iostream>
#include <chrono>

using namespace ecg;

TEST(ConcurrentHashMapTest, InsertRemoveTest)
{
    ConcurrentHashMap<int, int> map;

    std::vector<std::thread> threads_;

    for (int i = 0; i < std::thread::hardware_concurrency(); ++i)
    {
        if (i % 2)
        {
            threads_.push_back(std::thread(&ConcurrentHashMap<int, int>::Insert, &map, i, i));
        }
        else
        {
            threads_.push_back(std::thread([](int i, ConcurrentHashMap<int, int> &map) { map[i] = i; }, i, std::ref(map)));
        }
    }

    std::this_thread::sleep_for(std::chrono::seconds(1));

    for (auto &thread : threads_)
    {
        if (thread.joinable())
            thread.join();
    }

    EXPECT_EQ(map.Size(), std::thread::hardware_concurrency()) << "Elements were not inserted.";

    for (int i = 0; i < std::thread::hardware_concurrency(); ++i)
    {
        threads_.push_back(std::thread(&ConcurrentHashMap<int, int>::Erase, &map, i));
    }

    for (auto &thread : threads_)
    {
        if (thread.joinable())
            thread.join();
    }

    EXPECT_EQ(map.Size(), 0) << "Not all elements removed.";
}