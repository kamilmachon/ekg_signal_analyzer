#include <gtest/gtest.h>
#include <ecg_signal_analyzer/core/EcgData.hpp>
#include <ecg_signal_analyzer/core/TimeSeries.hpp>

using namespace ecg;

TEST(EcgDataTests, ClearTest)
{
  EcgData data;
  data.signals["A"] = TimeSeries<double>{
      {
          {0.0, 0},
          {1.0, 1},
          {2.0, 2},
          {3.0, 3},
          {4.0, 4},
          {5.0, 5},
      }};

  data.signals["B"] = TimeSeries<double>{
      {
          {6.0, 0},
          {7.0, 1},
          {8.0, 2},
          {9.0, 3},
          {10.0, 4},
          {11.0, 5},
      }};

  data.Clear(1, 2);

  EXPECT_EQ(data.signals["A"].points[0].value, 0.0);
  EXPECT_EQ(data.signals["A"].points[1].value, 3.0);
  EXPECT_EQ(data.signals["A"].points.size(), 4);

  EXPECT_EQ(data.signals["B"].points[0].value, 6.0);
  EXPECT_EQ(data.signals["B"].points[1].value, 9.0);
  EXPECT_EQ(data.signals["B"].points.size(), 4);
}