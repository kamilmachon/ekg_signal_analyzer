#include <gtest/gtest.h>

#include <ecg_signal_analyzer/input_system/WfdbWrapper.hpp>
#include <ecg_signal_analyzer/core/EcgData.hpp>

using namespace ecg;

TEST(WfdbWrapperTests, LoadExistingFile)
{
  WfdbWrapper wfdb;
  std::string path = "resources/database/108";

  auto data = wfdb.Load(path); // Load a test file here.

  auto test_signal = data->signals["RawData"]; // Isolate a signal by name here, e.g. 'signal-1'
  //Kasia:

  size_t expected_signal_length = 650000;           // A file with a known number of samples should be used.
  size_t signal_length = size(test_signal.points);  //Kasia: test_signal
  ASSERT_EQ(signal_length, expected_signal_length); // Make sure the signal was loaded.

  double expected_value = 992;                                      // Choose a value you know to test against, for example the 7th value of the test signal.
  ASSERT_FLOAT_EQ(test_signal.points[25000].value, expected_value); // Check that value to verify they were loaded.
}

TEST(WfdbWrapperTests, LoadNonExistingFile)
{
  WfdbWrapper wfdb;
  auto data = wfdb.Load("non-existent-file"); // Try to load a file that is not there.

  ASSERT_NE(data, nullptr); // Make sure the EcgData instance was created.

  ASSERT_EQ(data->signals.Size(), 0); // Make sure it is empty.
}