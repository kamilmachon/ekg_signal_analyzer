#include <gtest/gtest.h>

#include <ecg_signal_analyzer/hrv_dfa/HrvDfaTask.hpp>

using namespace ecg;

TEST(HrvDfaTaskTests, ParameterInitialValues)
{
  // Constructor should assign default values.
  HrvDfaTask task;

  try
  {
    // Now, check if they are there.
    EXPECT_TRUE(task.GetParameter<int>("dfa_alpha") == 1 || task.GetParameter<int>("dfa_alpha") == 2) << "Parameter \'alpha\' was not set up correctly.";
    //EXPECT_EQ(task.GetParameter<int>("alpha"), 1) << "Parameter \'alpha\' was not set up correctly.";
  }
  catch (...)
  {
    FAIL() << "Unexpected exception.";
  }
}
