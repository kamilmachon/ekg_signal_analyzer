#include <gtest/gtest.h>

#include <ecg_signal_analyzer/ecg_baseline/EcgBaselineTask.hpp>

using namespace ecg;

TEST(EcgBaselineTests, ParameterInitialValues)
{
  // Constructor should assign default values.
  EcgBaselineTask task;

  try
  {
    // Now, check if they are there.
    EXPECT_EQ(task.GetParameter<std::string>("main_filter_type"), std::string("butter")) << "Parameter \'main_filter_type\' was not set up correctly.";
    EXPECT_TRUE(task.GetParameter<bool>("smooth_enable")) << "Parameter \'smooth_enable\' was not set up correctly.";
    EXPECT_FALSE(task.GetParameter<int>("savgol_window") == 24) << "Parameter \'savgol_window\' was not set up correctly.";
    EXPECT_EQ(task.SetBaselineFilterParameters(-1, -1), -1) << "Baseline filter allow to set wrong parameters.";

    std::vector<double> raw_data{1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0};
    std::vector<double> timestamp{1.0/360.0, 2.0/360.0, 3.0/360.0, 4.0/360.0, 5.0/360.0, 6.0/360.0, 7.0/360.0, 8.0/360.0, 9.0/360.0, 10.0/360.0};
    std::vector<double> filtered_signal_ref{0.540968, 1.20085, 2.54208, 3.18369, 4.5433, 5.16641, 6.54461, 7.14906, 8.54598, 9.13166};
    std::vector<double> noise_signal_ref{0.459032, 0.799146, 0.457925, 0.816311, 0.456701, 0.833586, 0.455388, 0.850941, 0.454016, 0.868342};
    std::vector<double> smoothed_signal_ref{0.540968, 1.20085, 2.3022, 3.42987, 4.29078, 5.42531, 6.27932, 7.42077, 8.54598, 9.13166};
    std::vector<double> noise_smooth_ref{0, 0, 0.239875, -0.246179, 0.25252, -0.258892, 0.265291, -0.27171, 0, 0};
    std::vector<double> noise_ref{0.459032, 0.799146, 0.6978, 0.570131, 0.709221, 0.574694, 0.720679, 0.579231, 0.454016, 0.868342};

    std::vector<double> filtered_signal;
    std::vector<double> noise_signal;
    std::vector<double> smoothed_signal;
    std::vector<double> noise_smooth;
    std::vector<double> noise;

    task.FilterBaseline(timestamp, raw_data, filtered_signal, noise_signal);
    task.SetSmoothFilterParameters(5, 2);
    task.SmoothSignal(timestamp, filtered_signal, smoothed_signal, noise_smooth);

    noise.resize(filtered_signal.size());
    for (int i=0; i<filtered_signal.size(); i++)
    {
        noise[i] = noise_signal[i] + noise_smooth[i];
    }

    bool noise_equal = true;
    bool smoothed_equal = true;
    double epsilon = 0.00001;
    for (int i=0; i<filtered_signal.size(); i++)
    {
        if(fabs(noise_ref[i] - noise[i]) > epsilon) noise_equal = false;
        if(fabs(smoothed_signal_ref[i] - smoothed_signal[i]) > epsilon) smoothed_equal = false;
        
    }

    EXPECT_TRUE(noise_equal) << "Signal noise error.";
    EXPECT_TRUE(smoothed_equal) << "Signal filtered error.";

  }
  catch (...)
  {
    FAIL() << "Unexpected exception.";
  }
}