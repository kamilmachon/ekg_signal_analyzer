#include <gtest/gtest.h>

#include <ecg_signal_analyzer/example_task/ExampleEcgTask.hpp>

using namespace ecg;

TEST(ExampleEcgTaskTests, ParameterInitialValues)
{
  // Constructor should assign default values.
  ExampleEcgTask task;

  try
  {
    // Now, check if they are there.
    EXPECT_TRUE(task.GetParameter<bool>("czypapiesz")) << "Parameter \'czypapiesz\' was not set up correctly.";
    EXPECT_EQ(task.GetParameter<int>("example0"), 2137) << "Parameter \'example0\' was not set up correctly.";
    EXPECT_TRUE(task.GetParameter<std::vector<float>>("example2") == std::vector<float>(69, 1)) << "Parameter \'example2\' was not set up correctly.";
  }
  catch (...)
  {
    FAIL() << "Unexpected exception.";
  }
}