#include <ecg_signal_analyzer/rpeaks/Rpeaks.hpp>
#include <iostream>
#include <fstream>
#include <math.h>
#include <fftw3.h>

namespace ecg
{

int Rpeaks::ProcessData(const InputsType &inputs, const OutputsType &outputs)
{
    std::cout << "[Rpeaks] Processing data!" << std::endl;

    std::vector<std::pair<double, uint64_t>> rpeaks;
    std::vector<uint64_t> timestamp;
    const double LOCAL_SAMPLES_NUMBER = 5000.0;

    for (const auto &input : inputs) {
        std::cout << "[Rpeaks] This is the input signal name: " << input.first << std::endl;

        auto data_range = input.second;
        uint64_t num_of_samples = std::distance(data_range.first, data_range.second);
        double time_period = ((data_range.first+1)->time - data_range.first->time)/1000000.0;
        std::vector<double> signal;

        //std::ofstream output_file;
        //output_file.open("/home/esdemit/example_filtered_signal.csv");
        //output_file << "timestamp, value" << std::endl;
        uint32_t i = 0;
        for (auto it = data_range.first; it != data_range.second; ++it) {
        	signal.push_back(it->value);
        	timestamp.push_back(it->time);
        	//output_file << it->time << ", " << it->value << std::endl;
        	//std::cout << "Value of " << it->value << " at time " << it->time << "\n";
            i++;
        }
        //output_file.close();

        std::vector<double> differentiated_signal(signal.size(), 0);
        std::vector<double> hilbert_signal;

        DifferentiateSignal(signal, differentiated_signal);

        uint32_t n = std::floor(num_of_samples/LOCAL_SAMPLES_NUMBER);
		std::vector<double> hilbert_signal_local(LOCAL_SAMPLES_NUMBER, 0);
        for(uint32_t i=0; i < (n - 1); i++) {
			std::vector<double> differentiated_signal_local(differentiated_signal.begin()+i*LOCAL_SAMPLES_NUMBER,
															differentiated_signal.begin()+(i+1)*LOCAL_SAMPLES_NUMBER);
            HilbertTransform(differentiated_signal_local, hilbert_signal_local);
            hilbert_signal.insert(hilbert_signal.end(), hilbert_signal_local.begin(), hilbert_signal_local.end());
        }
        std::vector<double> hilbert_signal_local_end((num_of_samples-LOCAL_SAMPLES_NUMBER*(n-1)), 0);
		std::vector<double> differentiated_signal_local_end(differentiated_signal.begin()+(n-1)*LOCAL_SAMPLES_NUMBER, differentiated_signal.end());
		HilbertTransform(differentiated_signal_local_end, hilbert_signal_local_end);
        hilbert_signal.insert(hilbert_signal.end(), hilbert_signal_local_end.begin(), hilbert_signal_local_end.end());

        AdaptiveThresholding(signal, hilbert_signal, rpeaks);
        RemoveFalseRpeaks(rpeaks, time_period);
    }

    for (auto &output : outputs)
    {
        // Here, output is of type std::pair<std::string, std::reference_wrapper<TimeSeries<double>>>.
        std::cout << "[Rpeaks] This is the output signal name: " << output.first << "\n";

        // output.second is the reference wrapper - use get() on it, and you have direct access to the TimeSeries object.
        auto & output_series = output.second.get();
        output_series.points.resize(rpeaks.size());

        if (output.first == std::string("RpeaksData")) {
            uint32_t i = 0;
            //std::ofstream output_file;
            //output_file.open("/home/esdemit/example_rpeaks.csv");
            //output_file << "timestamp, value" << std::endl;
            for (auto &rpeak : rpeaks)
            {
            	output_series.points[i].value = rpeak.first;
            	output_series.points[i].time = timestamp[rpeak.second];
            	//output_file << output_series.points[i].time << ", " << output_series.points[i].value << std::endl;
            	//std::cout << output_series.points[i].time << ": " << output_series.points[i].value << std::endl;
                i++;
            }
            //output_file.close();
        } else {
            uint32_t i = 0;
//            std::ofstream output_file;
//            output_file.open("/home/esdemit/example_rpeaks2.csv");
//            output_file << "timestamp_difference, value" << std::endl;
            for (auto rpeak = rpeaks.begin(); rpeak != rpeaks.end() - 1; rpeak++)
            {
            	output_series.points[i].value = rpeak->first;
            	output_series.points[i].time = timestamp[(rpeak+1)->second] - timestamp[rpeak->second];
//            	output_file << output_series.points[i].time << ", " << output_series.points[i].value << std::endl;
                i++;
            }
        	output_series.points[i].value = (rpeaks.end()-1)->first;
        	output_series.points[i].time = 0;
//        	output_file << output_series.points[i].time << ", " << output_series.points[i].value << std::endl;
//          output_file.close();
        }
    }
    
    std::cout << "[Rpeaks] Data processing succes!\n\n" << std::endl;
    return 0;
}

int32_t Rpeaks::DifferentiateSignal(std::vector<double> & signal, std::vector<double> & differentiated_signal)
{
	uint32_t n = 2;
    for(auto point = signal.begin()+2; point != signal.end()-2; point++) {
        differentiated_signal[n++] = 1.0/8.0*(-signal[n-2]-2*signal[n-1]+2*signal[n+1]+signal[n+2]);
    }

    return 0;
}

int32_t Rpeaks::HilbertTransform(std::vector<double> & differentiated_signal, std::vector<double> & hilbert_signal)
{
	uint32_t N = differentiated_signal.size();
    fftw_complex in1[N], out1[N], in2[N], out2[N];
    for (uint32_t i = 0; i < N; i++) {
    	in1[i][0] = differentiated_signal[i];
    	in1[i][1] = 0;
    }

    // Fourier transform
    fftw_plan p;
    p = fftw_plan_dft_1d(N, in1, out1, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(p);

    // Multiplying positive and negative frequencies by -j and +j
    for (uint32_t i = 0; i < N/2; i++) {
    	in2[i][0] = out1[i][1];
    	in2[i][1] = -out1[i][0];
    }
    for (uint32_t i = N/2; i < N; i++) {
    	in2[i][0] = -out1[i][1];
    	in2[i][1] = out1[i][0];
    }
    fftw_destroy_plan(p);

    // Inverse Fourier transform
    p = fftw_plan_dft_1d(N, in2, out2, FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(p);
    for (uint32_t i = 0; i < N; i++)
    	hilbert_signal[i] = std::pow(out2[i][0]/N, 2.0);
    fftw_destroy_plan(p);
    fftw_cleanup();

    return 0;
}

int32_t Rpeaks::AdaptiveThresholding(std::vector<double> & signal, std::vector<double> & hilbert_signal, std::vector<std::pair<double, uint64_t>> & noisy_rpeaks)
{
    double rms = RootMeanSquare(hilbert_signal);
    double max_amplitude = *std::max_element(hilbert_signal.begin(), hilbert_signal.end());
    double previous_max_amplitude = max_amplitude;
    double threshold = CalculateThreshold(rms, max_amplitude, previous_max_amplitude);

    uint32_t n = 2;
    std::vector<double> signal_thresholded(signal.size(), 0);
    for(auto point = hilbert_signal.begin()+2; point != hilbert_signal.end(); point++) {
    	if(*point >= threshold) {
    		signal_thresholded[n] = *point;
    	}

    	std::pair<double, uint64_t> rpeak;
    	if(signal_thresholded[n-2] < signal_thresholded[n-1] && signal_thresholded[n-1] > signal_thresholded[n]) {
    		rpeak.first = signal[n-1];
    		rpeak.second = n-1;
    		noisy_rpeaks.push_back(rpeak);
			//std::cout << rpeak.second << ": " << rpeak.first << std::endl;
    	}
    	n++;
    }

    return 0;
}

double Rpeaks::CalculateThreshold(double rms, double max_amplitude, double previous_max_amplitude)
{
    double threshold;
    if(rms >= 0.18*max_amplitude)
        threshold = 0.39*max_amplitude;
    else if(max_amplitude > 2*previous_max_amplitude)
        threshold = 0.39*previous_max_amplitude;
    else if(rms < 0.18*max_amplitude)
        threshold = 1.6*rms;

    return threshold;
}

int32_t Rpeaks::RemoveFalseRpeaks(std::vector<std::pair<double, uint64_t>> & rpeaks, double time_period)
{
	int32_t num_of_peaks = rpeaks.size();
	int32_t n = 0;
	int32_t time_interval;
	auto rpeak1 = rpeaks.begin();
	auto rpeak2 = rpeak1 + 1;
	while(n != (num_of_peaks - 1)) {
		time_interval = (rpeak2->second - rpeak1->second)*time_period;
		if(time_interval < 200) {
			if(std::abs(rpeak1->first) > std::abs(rpeak2->first))
				rpeaks.erase(rpeak2);
			else
				rpeaks.erase(rpeak1);
			n--;
			num_of_peaks--;
		} else if(time_interval < 360) {
			if(std::abs(rpeak2->first) < std::abs(rpeak1->first)/2) {
				rpeaks.erase(rpeak2);
				n--;
				num_of_peaks--;
			} else if(std::abs(rpeak1->first) < std::abs(rpeak2->first)/2) {
				rpeaks.erase(rpeak1);
				n--;
				num_of_peaks--;
			} else {
				rpeak1++;
				rpeak2++;
			}
		} else {
			rpeak1++;
			rpeak2++;
		}
		n++;
	}

	/*for(auto rpeak = rpeaks.begin(); rpeak != rpeaks.end(); rpeak++) {
		std::cout << rpeak->second << ": " << rpeak->first << std::endl;
	}*/

	return 0;
}

double Rpeaks::RootMeanSquare(std::vector<double> & signal)
{
    double sum = 0;
    for(auto point = signal.begin(); point != signal.end(); point++) {
        sum += std::pow(*point, 2.0);
    }
    double rms = std::sqrt(sum/signal.size());

    return rms;
}

} // namespace ecg
