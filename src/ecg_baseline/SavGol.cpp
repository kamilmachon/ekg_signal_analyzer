#include <ecg_signal_analyzer/ecg_baseline/SavGol.hpp>

namespace SavGol
{
    SavGol::SavGol(uint32_t window_size, uint32_t filter_order)
    {
        if (window_size == 0) 
        {
            // std::cerr << "[EcgBaseline]  [SavGol Filter] Window size must be greater than 0. Setting default window size [21] ..." << std::endl;
            window_size_ = 21;
        }
        else
        {
            if (window_size % 2 == 1) 
            {
                window_size_ = window_size;
            }
            else 
            {
                // std::cerr << "[EcgBaseline]  [SavGol Filter] Window size must be odd. Incrementing window size ..." << std::endl;
                window_size_ = window_size + 1;
            }
        }

        if (filter_order == 0)
        {
            // std::cerr << "[EcgBaseline]  [SavGol Filter] Filter order must be greater than 0. Setting default filer order [3] ..." << std::endl;
            filter_order_ = 3;
        }
        else
        {
            filter_order_ = filter_order;
        }

        if (filter_order_ >= window_size_)
        {
            // std::cerr << "[EcgBaseline]  [SavGol Filter] Filter order must be less than window size. Setting default parameters [21, 3]..." << std::endl;
            window_size_ = 21;
            filter_order_ = 3;
        }
        
        CalculateSavGolCoefficients(coeffs_);
    }

    int32_t SavGol::SetWindowSize(uint32_t window_size)
    {
        uint32_t prev_window_size = window_size_;
        if (window_size == 0) 
        {
            // std::cerr << "[EcgBaseline]  [SavGol Filter] Window size must be greater than 0 ..." << std::endl;
            return -1;
        }
        else
        {
            if (window_size % 2 == 1) 
            {
                window_size_ = window_size;
            }
            else 
            {
                // std::cerr << "[EcgBaseline]  [SavGol Filter] Window size must be odd..." << std::endl;
                return -1;
            }
        }


        if (filter_order_ >= window_size_)
        {
            // std::cerr << "[EcgBaseline]  [SavGol Filter] Filter order must be less than window size..." << std::endl;
            window_size_ = prev_window_size;
            return -1;
        }

        CalculateSavGolCoefficients(coeffs_);
        return 0;
    }

    int32_t SavGol::SetFilterOrder(uint32_t filter_order)
    {
        if (filter_order == 0)
        {
            // std::cerr << "[EcgBaseline]  [SavGol Filter] Filter order must be greater than 0..." << std::endl;
        }
        else
        {
            if (filter_order >= window_size_)
            {
                // std::cerr << "[EcgBaseline]  [SavGol Filter] Filter order must be less than window size..." << std::endl;
                return -1;
            }
            else
            {
                filter_order_ = filter_order;
            }
        }

        CalculateSavGolCoefficients(coeffs_);
        return 0;
    }

    int32_t SavGol::FilterSignal(std::vector<double> & timestamp, std::vector<double> & raw_signal, std::vector<double> & filtered_signal, std::vector<double> & noise)
    {
        // std::cerr << "[EcgBaseline]  [SavGol Filter] Filtering signal with parameters: [" << window_size_ << ", " << filter_order_ << "]" << std::endl;
        if (raw_signal.size() == 0 || timestamp.size() == 0)
        {
            std::cerr << "[EcgBaseline]  [SavGol Filter] Input signal is empty!" << std::endl;
            return -1;
        }

        // std::cerr << "Input signal samples: " << raw_signal.size() << std::endl;
        // std::cerr << "Input signal frequency: " << 1.0/(timestamp[1] - timestamp[0]) << std::endl;

        const uint32_t signal_size = raw_signal.size();
        filtered_signal.resize(signal_size);
        noise.resize(signal_size);

        // for (int i=0; i<coeffs_.size(); i++) // std::cout << coeffs_[i] << ", ";
        // std::cout << std::endl;

        for(uint32_t i=0; i<signal_size; i++)
        {
            double accu = 0.0;
            if (window_size_/2 <= i && i < signal_size - window_size_/2)
            {
                for(uint32_t j = i-window_size_/2, k=0; j<i+window_size_/2+1; j++, k++)
                {
                    accu += coeffs_[k]*raw_signal[j];
                }
                filtered_signal[i] = accu;
            }
            else
            {
                accu = raw_signal[i];
            }

            filtered_signal[i] = accu;
            noise[i] = raw_signal[i] - filtered_signal[i];
        }

        return 0; 
    }

    // Function to get cofactor of A[p][q], n is current dimension of A matrix, out is resulted Cofactor matrix
    void SavGol::MatrixCofactor(std::vector<std::vector<double>> & A, std::vector<std::vector<double>> & out, uint32_t p, uint32_t q, uint32_t n) 
    { 
        // alocate memory for Cofactor
        out.resize(n-1);
        for(uint32_t idx=0; idx<n-1; idx++)
        {
            out[idx].resize(n-1);
        }

        uint32_t i = 0, j = 0; 
        // Looping for each element of the matrix 
        for (uint32_t row = 0; row < n; row++) 
        { 
            for (uint32_t col = 0; col < n; col++) 
            { 
                //  Copying into temporary matrix only those element 
                //  which are not in given row and column 
                if (row != p && col != q) 
                { 
                    out[i][j++] = A[row][col]; 
    
                    // Row is filled, so increase row index and 
                    // reset col index 
                    if (j == n - 1) 
                    { 
                        j = 0; 
                        i++; 
                    } 
                } 
            } 
        } 
    } 

    // Recursive function for finding determinant of matrix n is current dimension of A matrix
    double SavGol::MatrixDeterminant(std::vector<std::vector<double>> & A, uint32_t n) 
    { 
        double D = 0; // Initialize result 
    
        //  Base case : if matrix contains single element 
        if (n == 1) 
        {
            return A[0][0]; 
        }
    
        std::vector<std::vector<double>> temp; // To store cofactors 
    
        double sign = 1;  // To store sign multiplier 
    
        // Iterate for each element of first row 
        for (uint32_t f = 0; f < n; f++) 
        { 
            // Getting Cofactor of A[0][f] 
            MatrixCofactor(A, temp, 0, f, n); 
            D += sign * A[0][f] * MatrixDeterminant(temp, n - 1); 
    
            // terms are to be added with alternate sign 
            sign = -sign; 
        } 
    
        return D; 
    }

    // Function to get adjoint of A in adj 
    void SavGol::MatrixAdjoint(std::vector<std::vector<double>> & A, std::vector<std::vector<double>> & adj) 
    { 
        uint32_t N = A.size();
        if (N == 1) 
        { 
            adj.resize(1);
            adj[0].resize(1);
            adj[0][0] = A[0][0]; 
            return; 
        } 
    
        // temp is used to store cofactors of A[][] 
        double sign = 1;
        std::vector<std::vector<double>> temp;

        // Allocate memory for adjoint matrix
        adj.resize(N);
        for(uint32_t idx=0; idx<N; idx++)
        {
            adj[idx].resize(N);
        }
    
        for (uint32_t i=0; i<N; i++) 
        { 
            for (uint32_t j=0; j<N; j++) 
            { 
                // Get cofactor of A[i][j] 
                MatrixCofactor(A, temp, i, j, N); 
    
                // sign of adj[j][i] positive if sum of row 
                // and column indexes is even. 
                sign = ((i+j)%2==0)? 1.0: -1.0; 
    
                // Interchanging rows and columns to get the 
                // transpose of the cofactor matrix 
                adj[j][i] = (sign)*(MatrixDeterminant(temp, N-1)); 
            } 
        } 
    } 

    bool SavGol::MatrixInverse(std::vector<std::vector<double>> & A, std::vector<std::vector<double>> & inverse) 
    { 
        if (A.size() == 0) 
        {
            // std::cerr << "[MatrixInverse] Invalid matrix dimensions!" << std::endl;
            return false;
        }
        else if(A[0].size() == 0) 
        {
            // std::cerr << "[MatrixInverse] Invalid matrix dimensions!" << std::endl;
            return false;
        }
        else if (A.size() != A[0].size()) 
        {
            // std::cerr << "[MatrixInverse] Invalid matrix dimensions!" << std::endl;
            return false;
        }

        uint32_t N = A.size();

        // Find determinant of A
        double det = MatrixDeterminant(A, N); 
        if (det == 0) 
        { 
            // std::cerr << "Singular matrix, can't find its inverse" << std::endl; 
            return false; 
        } 
    
        // Find adjoint 
        std::vector<std::vector<double>> adj; 
        MatrixAdjoint(A, adj); 

        // Allocate memory for inverse matrix
        inverse.resize(N);
        for(uint32_t idx=0; idx<N; idx++)
        {
            inverse[idx].resize(N);
        }
    
        // Find Inverse using formula "A^-1 = Adj(A)/det(A)" 
        for (uint32_t i=0; i<N; i++) 
            for (uint32_t j=0; j<N; j++) 
                inverse[i][j] = adj[i][j]/det; 
    
        return true; 
    } 

    bool SavGol::MatrixTranspose(std::vector<std::vector<double>> & A, std::vector<std::vector<double>> & trans)
    {
        if (A.size() == 0) 
        {
            // std::cerr << "[MatrixTranspose] Invalid matrix dimensions!" << std::endl;
            return false;
        }
        else if(A[0].size() == 0) 
        {
            // std::cerr << "[MatrixTranspose] Invalid matrix dimensions!" << std::endl;
            return false;
        }

        uint32_t cols_A = A.size();
        uint32_t rows_A = A[0].size();

        // Allocate memory for transposed matrix
        trans.resize(rows_A);
        for(uint32_t idx=0; idx<rows_A; idx++)
        {
            trans[idx].resize(cols_A);
        }

        for (uint32_t i=0; i<cols_A; i++) 
        { 
            for (uint32_t j=0; j<rows_A; j++) 
            { 
                trans[j][i] = A[i][j];
            }
        }

        return true;
    }

    bool SavGol::MatrixMul(std::vector<std::vector<double>> & A, std::vector<std::vector<double>> & B, std::vector<std::vector<double>> & out)
    { 
        if (A.size() == 0 || B.size() == 0) 
        {
            // std::cerr << "[MatrixMul] 1 Invalid matrix dimensions!" << std::endl;
            return false;
        }
        else if(A[0].size() == 0 || B[0].size() == 0) 
        {
            // std::cerr << "[MatrixMul] 2 Invalid matrix dimensions!" << std::endl;
            return false;
        }
        
        uint32_t cols_A = A.size();
        uint32_t rows_A = A[0].size();
        uint32_t cols_B = B.size();
        uint32_t rows_B = B[0].size();

        if (rows_A != cols_B) 
        {
            // std::cerr << "[MatrixMul] 3 Invalid matrix dimensions!" << std::endl;
            return false;
        }

        // Allocate memory for transposed matrix
        out.resize(cols_A);
        for(uint32_t idx=0; idx<cols_A; idx++)
        {
            out[idx].resize(rows_B);
        }

        for (uint32_t i = 0; i < cols_A; i++)  
        { 
            for (uint32_t j = 0; j < rows_B; j++)  
            { 
                out[i][j] = 0; 
                for (uint32_t x = 0; x < rows_A; x++)  
                { 
                    out[i][j] += A[i][x] * B[x][j]; 
                } 
            } 
        } 

        return true;
    }

    void SavGol::CalculateSavGolCoefficients(std::vector<double> & coeffs)
    {
        std::vector<std::vector<double>> J;
        coeffs.resize(window_size_);

        // Allocate memory for J matrix
        J.resize(window_size_);
        for(uint32_t idx=0; idx<window_size_; idx++)
        {
            J[idx].resize(filter_order_+1);
        }

        std::vector<double> z(window_size_);
        for(uint32_t i=0; i<window_size_; i++)
        {
            z[i] = static_cast<double>(static_cast<int>(i)-static_cast<int>(window_size_/2));
        }

        for(uint32_t i=0; i<window_size_; i++)
        {
            for(uint32_t j=0; j<filter_order_+1; j++)
            {
                J[i][j] = std::pow(z[i], static_cast<double>(j));
            }
        }

        std::vector<std::vector<double>> J_T;
        std::vector<std::vector<double>> J_T_Mul_J;
        std::vector<std::vector<double>> J_T_Mul_J_Inv;
        std::vector<std::vector<double>> J_T_Mul_J_Inv_Mul_J_T;

        MatrixTranspose(J, J_T);
        MatrixMul(J_T, J, J_T_Mul_J);
        MatrixInverse(J_T_Mul_J, J_T_Mul_J_Inv);
        MatrixMul(J_T_Mul_J_Inv, J_T, J_T_Mul_J_Inv_Mul_J_T);

        for(uint32_t i=0; i<window_size_; i++)
        {
            coeffs[i] = J_T_Mul_J_Inv_Mul_J_T[0][i];
        }
    }

    void SavGol::GetSavGolCoefficients(std::vector<double> & coeffs)
    {
        CalculateSavGolCoefficients(coeffs);
    }

    template<class T> 
    void SavGol::MatrixDisplay(std::vector<std::vector<T>> & A) 
    {
        if (A.size() == 0) 
        {
            // std::cerr << "[MatrixDisplay] Invalid matrix dimensions!" << std::endl;
            return;
        }
        else if(A[0].size() == 0) 
        {
            // std::cerr << "[MatrixDisplay] Invalid matrix dimensions!" << std::endl;
            return;
        }

        // std::cerr << "[MatrixDisplay] Plotting matrix: " << std::endl;
        uint32_t N = A.size();
        uint32_t M = A[0].size();
        for (uint32_t i=0; i<N; i++) 
        { 
            for (uint32_t j=0; j<M; j++) 
            {
                // std::cerr << std::setw(12) << std::setprecision(7) << A[i][j] << " "; 
            }
            // std::cerr << std::endl; 
        } 
        // std::cerr << std::endl; 
    } 

} // namespace SavGol