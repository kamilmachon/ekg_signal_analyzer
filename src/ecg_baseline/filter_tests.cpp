// User include
#include <iostream>
#include "Butter.h"
#include "SavGol.h"
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>
#include <opencv2/opencv.hpp>
#include "opencv2/plot.hpp"

bool IsFileEmpty(std::istream &pFile);
int32_t GetSignalCSV(std::string filename, std::vector<double> & timestamp, std::vector<double> & raw_signal, std::vector<double> & filtered_signal, std::vector<double> & noise);
cv::Mat get_plot(std::vector<double> & timestamp, std::vector<double> & raw_signal, std::vector<double> & filtered_signal, std::vector<double> & noise);

int main(int argc, char **argv)
{
    std::cout << std::endl;
    std::cout << "ECG test started." << std::endl;
    std::string input_file = "/home/pawel/ESDMiT/MIT_DatabaseCSV/100.csv";

    std::vector<double> raw_signal;
    std::vector<double> timestamp;
    std::vector<double> filtered_signal_ref;
    std::vector<double> noise_ref;

    std::vector<double> filtered_signal_butter;
    std::vector<double> filtered_signal_savgol;
    std::vector<double> noise_butter;
    std::vector<double> noise_savgol;
    std::vector<double> noise_sum;

    if(GetSignalCSV(input_file, timestamp, raw_signal, filtered_signal_ref, noise_ref) != 0)
    {
        std::cout << "Error in file reading." << std::endl;
        return -1;
    }

    Butter::Butter ButterFilter;
    ButterFilter.SetFilterOrder(2);
    ButterFilter.SetCutoffFrequency(0.5);
    ButterFilter.FilterSignal(timestamp, raw_signal, filtered_signal_butter, noise_butter);

    SavGol::SavGol SavGolFilter(15, 3);
    SavGolFilter.FilterSignal(timestamp, filtered_signal_butter, filtered_signal_savgol, noise_savgol);

    noise_sum.resize(noise_savgol.size());
    for(uint32_t idx=0; idx<noise_sum.size(); idx++)
    {
        noise_sum[idx] = noise_savgol[idx] + noise_butter[idx];
    }

    cv::Mat plot = get_plot(timestamp, raw_signal, filtered_signal_savgol, noise_sum);
    cv::imshow("Results", plot);
    cv::waitKey();
    cv::waitKey();


    std::cout << std::endl;
    return 0;
}

cv::Mat get_plot(std::vector<double> & timestamp, std::vector<double> & raw_signal, std::vector<double> & filtered_signal, std::vector<double> & noise)
{
    int width = 1400;
    int height = 250;
    cv::Mat zero_split(cv::Size(width, 20), CV_8UC3, cv::Scalar(0, 0, 0)); 
    cv::Mat display_raw;
    cv::Mat display_filtered;
    cv::Mat display_noise;

    cv::Ptr<cv::plot::Plot2d> plot_raw;
    cv::Ptr<cv::plot::Plot2d> plot_filtered;
    cv::Ptr<cv::plot::Plot2d> plot_noise;

    plot_raw = cv::plot::createPlot2d(timestamp, raw_signal);
    plot_filtered = cv::plot::createPlot2d(timestamp, filtered_signal);
    plot_noise = cv::plot::createPlot2d(timestamp, noise);

    plot_raw->setPlotBackgroundColor(cv::Scalar(255, 255, 255)); 
    plot_filtered->setPlotBackgroundColor(cv::Scalar(255, 255, 255)); 
    plot_noise->setPlotBackgroundColor(cv::Scalar(255, 255, 255)); 

    plot_raw->setPlotLineColor(cv::Scalar(0, 0, 255));
    plot_filtered->setPlotLineColor(cv::Scalar(0, 150, 0));
    plot_noise->setPlotLineColor(cv::Scalar(255, 0, 0));

    plot_raw->setPlotGridColor(cv::Scalar(200, 200, 200));
    plot_filtered->setPlotGridColor(cv::Scalar(200, 200, 200));
    plot_noise->setPlotGridColor(cv::Scalar(200, 200, 200));

    plot_raw->setPlotSize(width, height);
    plot_filtered->setPlotSize(width, height);
    plot_noise->setPlotSize(width, height);


    // find the axis limits
    double max_raw = *max_element(raw_signal.begin(), raw_signal.end());
    double max_filtered = *max_element(filtered_signal.begin(), filtered_signal.end());
    double max = max_raw > max_filtered ? max_raw : max_filtered;

    double min_raw = *min_element(raw_signal.begin(), raw_signal.end());
    double min_filtered = *min_element(filtered_signal.begin(), filtered_signal.end());
    double min = min_raw < min_filtered ? min_raw : min_filtered;

    plot_raw->setMaxY(max);
    plot_raw->setMinY(min);
    plot_filtered->setMaxY(max);
    plot_filtered->setMinY(min);
    plot_noise->setMaxY(max);
    plot_noise->setMinY(min);

    plot_raw->render(display_raw);
    plot_filtered->render(display_filtered);
    plot_noise->render(display_noise);

    cv::flip(display_raw, display_raw, 0);
    cv::flip(display_filtered, display_filtered, 0);
    cv::flip(display_noise, display_noise, 0);

    // plot noise with raw signal on the same plot
    cv::Mat display_row_noise = cv::Mat::zeros(display_raw.rows, display_raw.cols, CV_8UC3);
    // return display_row_noise;
    for(int32_t i=0; i<display_raw.rows; i++)
    {
        for(int32_t j=0; j<display_raw.cols; j++)
        {
            if(display_noise.at<cv::Vec3b>(i, j) == cv::Vec3b(255, 0, 0))
            {
                display_row_noise.at<cv::Vec3b>(i, j) = cv::Vec3b(255, 0, 0);
            }
            else if(display_raw.at<cv::Vec3b>(i, j) == cv::Vec3b(0, 0, 255))
            {
                display_row_noise.at<cv::Vec3b>(i, j) = cv::Vec3b(0, 0, 255);
            }
            else if(display_noise.at<cv::Vec3b>(i, j) == cv::Vec3b(200, 200, 200))
            {
                display_row_noise.at<cv::Vec3b>(i, j) = cv::Vec3b(200, 200, 200);
            }
            else
            {
                display_row_noise.at<cv::Vec3b>(i, j) = cv::Vec3b(255, 255, 255);
            }
        }
    }

    cv::Mat display_out;

    cv::vconcat(display_raw, zero_split, display_out);
    cv::vconcat(display_out, display_row_noise, display_out);
    cv::vconcat(display_out, zero_split, display_out);
    cv::vconcat(display_out, display_filtered, display_out);

    return display_out;
}

bool IsFileEmpty(std::istream &pFile)
{
	return pFile.peek() == std::istream::traits_type::eof();
}

int32_t GetSignalCSV(std::string filename, std::vector<double> & timestamp, std::vector<double> & raw_signal, std::vector<double> & filtered_signal, std::vector<double> & noise)
{
    // Read signal from CSV file
    const std::ios_base::openmode mode_input = std::ios_base::in | std::ios::binary;
    std::fstream input_stream(filename, mode_input);

    // Check if CSV file is valid
    bool file_empty = IsFileEmpty(input_stream);
    if (file_empty)
    {
        std::cout << "\nInput file is empty!\n" << std::endl;
        return -1;
    }

    std::string line;
    std::vector<std::string> vec;
    std::getline(input_stream, line); // skip the 1st line

    while (std::getline(input_stream,line))
    {
        if (line.empty()) // skip empty lines:
        {
            //cout << "empty line!" << endl;
            continue;
        }

        std::istringstream iss(line);
        std::string lineStream;
        std::string::size_type sz;

        std::vector<double> row;

        while (getline(iss, lineStream, ','))
        {  
            row.push_back(std::stod(lineStream, &sz)); // convert to double
        }

        timestamp.push_back(row[0]);
        raw_signal.push_back(row[1]);
        filtered_signal.push_back(row[2]);
        noise.push_back(row[3]);
    }

    return 0;
}