#include <ecg_signal_analyzer/ecg_baseline/Butter.hpp>

namespace Butter
{
    Butter::Butter(double cutoff_frequency, uint32_t filter_order)
    {
        cutoff_frequency_ = cutoff_frequency;
        filter_order_ = filter_order;
    }

    int32_t Butter::SetCutoffFrequency(double cutoff_frequency)
    {
        if (cutoff_frequency < 0)
        {
            return -1;
        }
        cutoff_frequency_ = cutoff_frequency;
        return 0;
    }

    int32_t Butter::SetFilterOrder(uint32_t filter_order)
    {
        filter_order_ = filter_order;
        return 0;
    }

    int32_t Butter::FilterSignal(std::vector<double> & timestamp, std::vector<double> & raw_signal, std::vector<double> & filtered_signal, std::vector<double> & noise)
    {
        // std::cout << "[EcgBaseline]  [Butterworth Filter] Parameters: [" << cutoff_frequency_ << ", " << filter_order_ << "]" << std::endl;
        if (raw_signal.size() == 0)
        {
            std::cerr << "[EcgBaseline]  [Butterworth Filter] Input signal is empty!" << std::endl;
            return -1;
        }

        double signal_frequency = 1.0/(timestamp[1] - timestamp[0]);
        double FrequencyLimits[2] = {cutoff_frequency_/(signal_frequency/2), 0.99}; // High-Pass filter

        std::vector<double> a, b;
        a = ComputeDenCoeffs(filter_order_, FrequencyLimits[0], FrequencyLimits[1]);
        b = ComputeNumCoeffs(filter_order_, FrequencyLimits[0], FrequencyLimits[1], a);
        Filter(raw_signal, b, a, filtered_signal);
        noise.resize(filtered_signal.size());
        for (uint32_t i=0; i<filtered_signal.size(); i++)
        {
            noise[i] = raw_signal[i] - filtered_signal[i];
        }

        // std::cout << "Input signal frequency: " << signal_frequency << std::endl;
        // std::cout << "Frequency limits: [" << FrequencyLimits[0] << ", " << FrequencyLimits[1] << "]" << std::endl;
        // std::cout << "DenC: [";
        // for (uint32_t k = 0; k<a.size(); k++)
        // {
        //     if (k != a.size()-1)
        //     {
        //         // std::cout << a[k] << ",   ";
        //     }
        //     else
        //     {
        //         // std::cout << a[k] << "]" << std::endl;
        //     }
        // }

        // std::cout << "NumC: [";
        // for (uint32_t k = 0; k<b.size(); k++)
        // {
        //     if (k != b.size()-1)
        //     {
        //         // std::cout << b[k] << ",   ";
        //     }
        //     else
        //     {
        //         // std::cout << b[k] << "]" << std::endl;
        //     }
        // }

        return 0; 
    }

    std::vector<double> Butter::ComputeDenCoeffs(uint32_t filter_order, double low_cutoff, double high_cutoff)
    {
        uint32_t k;       // loop variables
        double theta;     // PI * (Ucutoff - Lcutoff) / 2.0
        double cp;        // cosine of phi
        double st;        // sine of theta
        double ct;        // cosine of theta
        double s2t;       // sine of 2*theta
        double c2t;       // cosine 0f 2*theta
        std::vector<double> RCoeffs(2 * filter_order);     // z^-2 coefficients 
        std::vector<double> TCoeffs(2 * filter_order);     // z^-1 coefficients
        std::vector<double> DenomCoeffs;     // dk coefficients
        double PoleAngle;       // pole angle
        double SinPoleAngle;    // sine of pole angle
        double CosPoleAngle;    // cosine of pole angle
        double a;               // workspace variables

        cp = cos(M_PI * (high_cutoff + low_cutoff) / 2.0);
        theta = M_PI * (high_cutoff - low_cutoff) / 2.0;
        st = sin(theta);
        ct = cos(theta);
        s2t = 2.0*st*ct;        // sine of 2*theta
        c2t = 2.0*ct*ct - 1.0;  // cosine of 2*theta

        for (k = 0; k < filter_order; ++k)
        {
            PoleAngle = M_PI * (double)(2 * k + 1) / (double)(2 * filter_order);
            SinPoleAngle = sin(PoleAngle);
            CosPoleAngle = cos(PoleAngle);
            a = 1.0 + s2t*SinPoleAngle;
            RCoeffs[2 * k] = c2t / a;
            RCoeffs[2 * k + 1] = s2t*CosPoleAngle / a;
            TCoeffs[2 * k] = -2.0*cp*(ct + st*SinPoleAngle) / a;
            TCoeffs[2 * k + 1] = -2.0*cp*st*CosPoleAngle / a;
        }

        DenomCoeffs = TrinomialMultiply(filter_order, TCoeffs, RCoeffs);

        DenomCoeffs[1] = DenomCoeffs[0];
        DenomCoeffs[0] = 1.0;
        for (k = 3; k <= 2 * filter_order; ++k)
            DenomCoeffs[k] = DenomCoeffs[2 * k - 2];

        for (uint32_t i = DenomCoeffs.size() - 1; i > filter_order * 2; i--)
            DenomCoeffs.pop_back();

        return DenomCoeffs;
    }

    std::vector<double> Butter::TrinomialMultiply(uint32_t filter_order, std::vector<double> & b, std::vector<double> & c)
    {
        std::vector<double> RetVal(4 * filter_order);

        RetVal[2] = c[0];
        RetVal[3] = c[1];
        RetVal[0] = b[0];
        RetVal[1] = b[1];

        for (uint32_t i = 1; i < filter_order; ++i)
        {
            RetVal[2 * (2 * i + 1)] += c[2 * i] * RetVal[2 * (2 * i - 1)] - c[2 * i + 1] * RetVal[2 * (2 * i - 1) + 1];
            RetVal[2 * (2 * i + 1) + 1] += c[2 * i] * RetVal[2 * (2 * i - 1) + 1] + c[2 * i + 1] * RetVal[2 * (2 * i - 1)];

            for (uint32_t j = 2 * i; j > 1; --j)
            {
                RetVal[2 * j] += b[2 * i] * RetVal[2 * (j - 1)] - b[2 * i + 1] * RetVal[2 * (j - 1) + 1] +
                    c[2 * i] * RetVal[2 * (j - 2)] - c[2 * i + 1] * RetVal[2 * (j - 2) + 1];
                RetVal[2 * j + 1] += b[2 * i] * RetVal[2 * (j - 1) + 1] + b[2 * i + 1] * RetVal[2 * (j - 1)] +
                    c[2 * i] * RetVal[2 * (j - 2) + 1] + c[2 * i + 1] * RetVal[2 * (j - 2)];
            }

            RetVal[2] += b[2 * i] * RetVal[0] - b[2 * i + 1] * RetVal[1] + c[2 * i];
            RetVal[3] += b[2 * i] * RetVal[1] + b[2 * i + 1] * RetVal[0] + c[2 * i + 1];
            RetVal[0] += b[2 * i];
            RetVal[1] += b[2 * i + 1];
        }

        return RetVal;
    }

    std::vector<double> Butter::ComputeNumCoeffs(uint32_t filter_order, double low_cutoff, double high_cutoff, std::vector<double> DenC)
    {
        std::vector<double> TCoeffs;
        std::vector<double> NumCoeffs(2 * filter_order + 1);
        std::vector<std::complex<double>> NormalizedKernel(2 * filter_order + 1);

        std::vector<double> Numbers;
        for (double n = 0; n < filter_order * 2 + 1; n++)
            Numbers.push_back(n);

        TCoeffs = ComputeHP(filter_order);

        for (uint32_t i = 0; i < filter_order; ++i)
        {
            NumCoeffs[2 * i] = TCoeffs[i];
            NumCoeffs[2 * i + 1] = 0.0;
        }
        NumCoeffs[2 * filter_order] = TCoeffs[filter_order];

        double cp[2];
        double Wn;
        cp[0] = 2 * 2.0*tan(M_PI * low_cutoff / 2.0);
        cp[1] = 2 * 2.0*tan(M_PI * high_cutoff / 2.0);

        // double Bw;
        // Bw = cp[1] - cp[0];

        //center frequency
        Wn = sqrt(cp[0] * cp[1]);
        Wn = 2 * atan2(Wn, 4);
        // double kern;
        const std::complex<double> result = std::complex<double>(-1, 0);

        for (uint32_t k = 0; k< filter_order * 2 + 1; k++)
        {
            NormalizedKernel[k] = std::exp(-sqrt(result)*Wn*Numbers[k]);
        }
        double b = 0;
        double den = 0;
        for (uint32_t d = 0; d < filter_order * 2 + 1; d++)
        {
            b += std::real(NormalizedKernel[d] * NumCoeffs[d]);
            den += std::real(NormalizedKernel[d] * DenC[d]);
        }
        for (uint32_t c = 0; c < filter_order * 2 + 1; c++)
        {
            NumCoeffs[c] = (NumCoeffs[c] * den) / b;
        }

        for (uint32_t i = NumCoeffs.size() - 1; i > filter_order * 2 + 1; i--)
            NumCoeffs.pop_back();

        return NumCoeffs;
    }

    std::vector<double> Butter::ComputeLP(uint32_t filter_order)
    {
        std::vector<double> NumCoeffs(filter_order + 1);
        int m;
        int i;

        NumCoeffs[0] = 1;
        NumCoeffs[1] = filter_order;
        m = filter_order / 2;
        for (i = 2; i <= m; ++i)
        {
            NumCoeffs[i] = (double)(filter_order - i + 1)*NumCoeffs[i - 1] / i;
            NumCoeffs[filter_order - i] = NumCoeffs[i];
        }
        NumCoeffs[filter_order - 1] = filter_order;
        NumCoeffs[filter_order] = 1;

        return NumCoeffs;
    }

    std::vector<double> Butter::ComputeHP(uint32_t filter_order)
    {
        std::vector<double> NumCoeffs;
        NumCoeffs = ComputeLP(filter_order);

        for (uint32_t i = 0; i <= filter_order; ++i)
        {
            if (i % 2) NumCoeffs[i] = -NumCoeffs[i];
        }

        return NumCoeffs;
    }

    void Butter::Filter(std::vector<double> & x, std::vector<double> & coeff_b, std::vector<double> & coeff_a, std::vector<double> & filtered_signal)
    {
        int32_t len_x = x.size();
        int32_t len_b = coeff_b.size();
        int32_t len_a = coeff_a.size();

        std::vector<double> zi(len_b);

        std::vector<double> filter_forward(len_x, 0.0);
        std::vector<double> filter_backward(len_x, 0.0);
        std::vector<double> filter_out(len_x, 0.0);


        // Filter in forward direction     ----->
        for(int32_t n=0; n<len_x; n++)
        {
            // feed-backward
            double out = 0.0;
            for(int32_t k=1; k<len_a; k++)
            {
                if(n >= k)
                {
                    out -= coeff_a[k]*filter_forward[n-k];
                }
            }

            // feed-forward
            for(int32_t k=0; k<len_b; k++)
            {
                if(n >= k)
                {
                    out += coeff_b[k]*x[n-k];
                }
            }
            filter_forward[n] = out;
        }

        // filter in backward direction     <-----
        std::reverse(filter_forward.begin(), filter_forward.end()); 

        for(int32_t n=0; n<len_x; n++)
        {
            // feed-backward
            double out = 0.0;
            for(int32_t k=1; k<len_a; k++)
            {
                if(n >= k)
                {
                    out -= coeff_a[k]*filter_backward[n-k];
                }
            }

            // feed-forward
            for(int32_t k=0; k<len_b; k++)
            {
                if(n >= k)
                {
                    out += coeff_b[k]*filter_forward[n-k];
                }
            }
            filter_backward[n] = out;
        }

        std::reverse(filter_backward.begin(), filter_backward.end()); 

        filtered_signal = filter_backward;
    }


} // namespace Butter