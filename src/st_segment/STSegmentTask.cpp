#include <ecg_signal_analyzer/st_segment/STSegmentTask.hpp>

#include <iostream>
namespace ecg
{

STSegmentTask::STSegmentTask()
{
    // The Constructor is needed only for parameter initialization (parameters could be changed in GUI).
    // task_parameters_ is a hash map where the key is string and the value is std::any.
    // Thanks to the std::any you can put any parameter type you want inside the map, just like in the python dictionary.
    // Parameters can be added by operator[] or AddParameter(std::string, std::any) method function, just like presented below.

    // no parameters in this module
    // task_parameters_["example0"] = 2137;
    // task_parameters_["czypapiesz"] = true;
    // AddParameter("example2", std::vector<float>(69, 1));
}

uint64_t STSegmentTask::GetTPointIdx(std::vector<double> & timestamp, std::vector<double> & filtered_signal, uint64_t s_point_idx, uint64_t t_end_idx)
{
    // find maximum value - top of the T segment
    uint64_t max_val_idx = s_point_idx+1;
    double t_max_value = filtered_signal[max_val_idx];
    for(uint64_t i=s_point_idx; i<t_end_idx; i++)
    {
        if(filtered_signal[i] > t_max_value)
        {
            t_max_value = filtered_signal[i];
            max_val_idx = i;
        }
    }
    // std::cout << "Max. in range: [" << s_point_idx << ", " << t_end_idx << "]   is: " << t_max_value << "  at idx: " << max_val_idx << std::endl;

    // find parameters of line which connect S point [x_a, y_a] with Max. Point [x_b, y_b]
    double start_timestamp = timestamp[s_point_idx];
    double x_a = timestamp[s_point_idx] - start_timestamp;
    double y_a = filtered_signal[s_point_idx];
    double x_b = timestamp[max_val_idx]- start_timestamp;
    double y_b = filtered_signal[max_val_idx];

    // line equation:  Ax + By + C = 0
    double A = (y_a-y_b)/(x_a-x_b);
    double B = -1;
    double C = (-1.0)*(y_a - A*x_a);

    // find point idx with maximum distance to line which connect S and Max. point
    double max_dist = 0;
    double max_dist_idx = s_point_idx;
    for(uint64_t i=s_point_idx; i<max_val_idx; i++)
    {
        double x_p = timestamp[i] - start_timestamp;
        double y_p = filtered_signal[i];

        double current_dist = std::abs(A*x_p+B*y_p+C) / std::sqrt(std::pow(A, 2)+std::pow(B, 2));
        if (current_dist > max_dist)
        {
            max_dist = current_dist;
            max_dist_idx = i;
        }
    }

    // std::cout << "Max. dist: " << max_dist << "  at idx: " << max_dist_idx << std::endl;

    return max_dist_idx;
}

double STSegmentTask::GetSTSegmentAngle(std::vector<double> & timestamp, std::vector<double> & filtered_signal, uint64_t s_point_idx, uint64_t t_point_idx)
{
    // find parameters of line which connect S point [x_a, y_a] with T point [x_b, y_b]
    double start_timestamp = timestamp[s_point_idx];
    double x_a = timestamp[s_point_idx] - start_timestamp;
    double y_a = filtered_signal[s_point_idx];
    double x_b = timestamp[t_point_idx]- start_timestamp;
    double y_b = filtered_signal[t_point_idx];

    double A = (y_a-y_b)/(x_a-x_b);
    double radian_angle = std::atan(A);

    // std::cout << "Indices: " << s_point_idx << ", " << t_point_idx << std::endl;
    // std::cout << "Points: [" << x_a << ", " << y_a << "]  ,  [" << x_b << ", " << y_b << "]" << std::endl;
    // std::cout << "ST_Segment angle: " << radian_angle * (180.0/M_PI) << std::endl << std::endl;
    return radian_angle * (180.0/M_PI);
}

int STSegmentTask::ProcessData(const InputsType &inputs, const OutputsType &outputs)
{
    std::cout << "[STSegment]  Processing data!" << std::endl;
    // First you can acces the parameters of your node. Due to the fact that the value is std::any accessing parameters requires casting.
    // You can either do it manually or call templated method GetParameter<T>(std::string key).
    // Casting value on wrong type will result in segmentation fault, so don't do that ;)
    {
        // no parameters in this module

        // int param1 = GetParameter<int>("example0");
        // bool param2 = GetParameter<bool>("czypapiesz");
        // std::vector<float> param3 = std::any_cast<std::vector<float>>(task_parameters_["example2"]);
    }

    std::vector<double> timestamp;
    std::vector<uint64_t> original_timestamp;
    std::vector<double> filtered_signal;
    std::vector<uint64_t> j_point_idx;
    std::vector<double> j_point_value;
    std::vector<uint64_t> j_point_idx_final;
    std::vector<double> j_point_value_final;
    std::vector<uint64_t> t_end_point_idx;
    std::vector<double> t_end_point_value;
    std::vector<uint64_t> t_end_point_idx_final;
    std::vector<double> t_end_point_value_final;

    std::vector<double> T_max_value;
    std::vector<uint64_t> T_max_idx;
    std::vector<uint64_t> S_point_idx;
    std::vector<uint64_t> T_point_idx;
    std::vector<double> ST_segment_angle;

    uint64_t last_j_idx = 0;
    uint64_t last_t_end_idx = 0;

    // Input data is given to you using pairs of iterators.
    // Because 'inputs' is a const reference, you cannot access the signals by name:
    {
        // auto some_signal = inputs["MySignalName"]; // Compilation error.
    }
    // Instead, you must iterate through them like any other container.
    for (const auto &input : inputs)
    {
        // input here is of type std::pair<std::string, std::pair<it, it>>. Pair-ception!
        // So, input.first will be a string - the name of the signal.
        
        if (input.first == "FilteredData")
        {
            std::cout << "This is the signal name: " << input.first << std::endl;
            // input.second will give you the pair of iterators. You can use them to access the values of your input data.
            auto data_range = input.second;
            int i = 0;
            for (auto it = data_range.first; it != data_range.second; ++it)
            {
                filtered_signal.push_back((it->value));
                timestamp.push_back(static_cast<double>(it->time) / 1000000000.0);
                original_timestamp.push_back(it->time);
            }
        }
    }

    for (const auto &input : inputs)
    {
        // input here is of type std::pair<std::string, std::pair<it, it>>. Pair-ception!
        // So, input.first will be a string - the name of the signal.

        if (input.first == "QRSe")
        {
            std::cout << "This is the signal name: " << input.first << std::endl;
            // input.second will give you the pair of iterators. You can use them to access the values of your input data.
            auto data_range = input.second;
            int i = 0;
            for (auto it = data_range.first; it != data_range.second; ++it)
            {
                double current_timestamp = static_cast<double>(it->time) / 1000000000.0;
                while(current_timestamp != timestamp[last_j_idx])
                {
                    last_j_idx ++;
                }
                // std::cout << "last_j_idx: " << last_j_idx << "    Value: " << it->value << std::endl;
                j_point_idx.push_back(last_j_idx);
                j_point_value.push_back(it->value);
            }
        }

        if (input.first == "Te")
        {
            std::cout << "This is the signal name: " << input.first << std::endl;
            // input.second will give you the pair of iterators. You can use them to access the values of your input data.
            auto data_range = input.second;
            int i = 0;
            for (auto it = data_range.first; it != data_range.second; ++it)
            {
                double current_timestamp = static_cast<double>(it->time) / 1000000000.0;
                while(current_timestamp != timestamp[last_t_end_idx])
                {
                    last_t_end_idx ++;
                }
                // std::cout << "last_t_end_idx: " << last_t_end_idx << "    Value: " << it->value << std::endl;
                t_end_point_idx.push_back(last_t_end_idx);
                t_end_point_value.push_back(it->value);
            }
        }
    }

    // std::cout << "j_point_idx.size() = " << j_point_idx.size() << std::endl;
    // std::cout << "t_end_point_idx.size() = " << t_end_point_idx.size() << std::endl;

    uint64_t number_of_st_segments = 0;
    // find pairs <J, T_end points>
    for (int i=0; i < j_point_idx.size(); i++)
    {
        bool match = false;
        for (int j=0; j<t_end_point_idx.size(); j++)
        {
            // find first T-end point with index higher than J points index
            if(t_end_point_idx[j] > j_point_idx[i])
            {
                if (!match)
                {
                    j_point_idx_final.push_back(j_point_idx[i]);
                    t_end_point_idx_final.push_back(t_end_point_idx[j]);
                    t_end_point_value_final.push_back(j_point_value[i]);
                    j_point_value_final.push_back(t_end_point_value[j]);
                    // std::cout << "Pair: [" << j_point_idx[i] << ", " << t_end_point_idx[j] << "]" << std::endl;
                    number_of_st_segments ++;
                    match = true;
                }
            }
        }
    }

    std::cout << "[STSegment]  Number of ST segments: " << number_of_st_segments << std::endl;
    for (int i=0; i<number_of_st_segments; i++)
    {
        uint64_t S_point_idx_current = j_point_idx_final[i];
        S_point_idx.push_back(S_point_idx_current);
        uint64_t T_point_idx_current = GetTPointIdx(timestamp, filtered_signal, S_point_idx_current, t_end_point_idx_final[i]);
        T_point_idx.push_back(T_point_idx_current);
        ST_segment_angle.push_back(GetSTSegmentAngle(timestamp, filtered_signal, S_point_idx_current, T_point_idx_current));
    }

    // When you have your results ready, store them in your output data.
    // Again, since this is a const reference, you can not ask for the signals by name:
    {
        // auto & some_signal = outputs["MySignalName"] // Compilation errors gallore!
    }
    // So you have to iterate through it.

    for (auto &output : outputs)
    {
        // Here, output is of type std::pair<std::string, std::reference_wrapper<TimeSeries<double>>>.
        // So, output.first is again, the name of the signal.
        std::cout << "[STSegment]  Output name: " << output.first << "\n";

        // output.second is the reference wrapper - use get() on it, and you have direct access to the TimeSeries object.
        auto &output_series = output.second.get();

        if (output.first == std::string("STStartPoint"))
        {
            output_series.points.resize(S_point_idx.size());
            for (uint64_t i=0; i<S_point_idx.size(); i++)
            {
                // save start point of ST Segment
                output_series.points[i].time = original_timestamp[S_point_idx[i]];
                output_series.points[i].value = filtered_signal[S_point_idx[i]];
            }
        }
        else if(output.first == std::string("STStopPoint"))
        {
            output_series.points.resize(T_point_idx.size());
            for (uint64_t i=0; i<T_point_idx.size(); i++)
            {
                // save stop point of ST Segment
                output_series.points[i].time = original_timestamp[T_point_idx[i]];
                output_series.points[i].value = filtered_signal[T_point_idx[i]];
            }
        }
        else if(output.first == std::string("STSegmentAngle"))
        {
            output_series.points.resize(ST_segment_angle.size());
            for (uint64_t i=0; i<ST_segment_angle.size(); i++)
            {
                // save ST segment angle
                output_series.points[i].time = static_cast<uint64_t>(0); // zero time - it is not important here
                output_series.points[i].value = ST_segment_angle[i];
            }
        }
    }

    // All good! Since nothing went wrong, return 0 as a success code.
    std::cout << "[STSegment]  Data processing succes!" << std::endl;
    return 0;
}

} // namespace ecg