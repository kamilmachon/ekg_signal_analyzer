#include <ecg_signal_analyzer/input_system/WfdbWrapper.hpp>

#include <ecg_signal_analyzer/input_system/WFDB.h>

using namespace input_system;

namespace ecg
{
WfdbWrapper::~WfdbWrapper() {}

std::shared_ptr<EcgData> WfdbWrapper::Load(const std::string &path)
{
  WFDB loader;

  if (!loader.open(path)) // Make sure the file opened correctly.
  {
    return std::make_shared<EcgData>(); // If not, return an empty dataset.
  }

  return loader.read_all();
}

} // namespace ecg