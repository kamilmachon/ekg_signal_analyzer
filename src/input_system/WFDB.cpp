#include <ecg_signal_analyzer/input_system/WFDB.h>

namespace input_system
{
WFDB::WFDB() : signals(nullptr) // : WFDB(std::make_shared<ecg::EcgData>())
{
}

// WFDB::WFDB(std::shared_ptr<ecg::EcgData> data) : signals(nullptr), len(0), ecg_data_(data)
// {
// }

WFDB::WFDB(std::string path) : WFDB() // : WFDB(data)
{
	open(path);
}

WFDB::~WFDB()
{
	if (signals != nullptr)
	{
		delete[] signals;
	}
}

bool WFDB::open(std::string path)
{
	if (signals != nullptr)
	{
		return false;
	}
	signals = new WFDB_Siginfo[2];

	len = isigopen(path.data(), signals, 0);

	if (len <= 0)
	{

		return false;
	}

	isigopen(path.data(), signals, len);

	WFDB_Sample v[len];
	freq = getifreq();
	//std::cout << "Input data Frequency: " << freq << std::endl;

	db_size = 0;
	//std::cout << mstimstr(0) << std::endl;
	while (getvec(v) > 0)
	{
		++db_size;
	}
	//std::cout << "Size of database: " << db_size << std::endl;
	isigsettime(0);
	return true;
}

std::shared_ptr<ecg::EcgData> WFDB::read_all() const
{
	InputData result;
	result.val1.reserve(db_size);
	result.val2.reserve(db_size);
	result.time.reserve(db_size);

	WFDB_Sample v[len];
	uint64_t amunt = 0;
	WFDB_Time t;
	std::regex rgxTimeType1("^\\s*(\\d+):(\\d{2}).(\\d{3})\\s*$");
	std::regex rgxTimeType2("^\\[(\\d{2}):(\\d{2}):(\\d{2}).(\\d{3})\\s{1}(0[1-9]|[1-2][0-9]|3[0-1])\\/(0[1-9]|1[0-2])\\/(\\d{4})\\]$");
	//std::regex rgxTimeType3("^\\s*(\\d{0,2}?):*(\\d{1,2}):(\\d{2}).(\\d{3})\\s*$");
	std::smatch matches;
	uint64_t ms;
	struct tm tm;
	tm.tm_hour = 0;
	tm.tm_mday = 0;
	tm.tm_min = 0;
	tm.tm_mon = 0;
	tm.tm_sec = 0;
	tm.tm_year = 70;

	ecg::TimeSeries<double> ts1;
	ts1.points.resize(db_size);
	ecg::TimeSeries<double> ts2;
	ts2.points.resize(db_size);

	while (getvec(v) > 0)
	{
		result.val1.push_back(v[0]);
		result.val2.push_back(v[1]);

		result.time.push_back(mstimstr(amunt));
		++amunt;
	}

	if (std::regex_search(result.time[0], matches, rgxTimeType1))
	{
		ts1.points[0].time = 1112470620000000000;
		ts1.points[0].value = result.val1[0];
		ts2.points[0].time = 1112470620000000000;
		ts2.points[0].value = result.val2[0];

		for (uint64_t p = 1; p <= db_size; p++)
		{
			ts1.points[p].time = ts1.points[0].time + ((p * 1000000000) / freq);
			ts2.points[p].time = ts1.points[0].time + ((p * 1000000000) / freq);
			ts1.points[p].value = result.val1[p];
			ts2.points[p].value = result.val2[p];
		}
	}
	else if (std::regex_search(result.time[0], matches, rgxTimeType2))
	{
		tm.tm_hour = atoi(matches[1].str().c_str());
		tm.tm_min = atoi(matches[2].str().c_str());
		tm.tm_sec = atoi(matches[3].str().c_str());
		ms = atoi(matches[4].str().c_str());
		tm.tm_mday = atoi(matches[5].str().c_str());
		tm.tm_mon = atoi(matches[6].str().c_str()) - 1;
		tm.tm_year = atoi(matches[7].str().c_str()) - 1900;

		ts1.points[0].time = (mktime(&tm) * 1000 + ms) * 1000000;
		ts1.points[0].value = result.val1[0];
		ts2.points[0].time = (mktime(&tm) * 1000 + ms) * 1000000;
		ts2.points[0].value = result.val2[0];
		std::cout << ts1.points[0].time << std::endl;
		for (uint64_t p = 1; p <= db_size; p++)
		{
			ts1.points[p].time = ts1.points[0].time + ((p * 1000000000) / freq);
			ts2.points[p].time = ts2.points[0].time + ((p * 1000000000) / freq);
			ts1.points[p].value = result.val1[p];
			ts2.points[p].value = result.val2[p];
		}
	}

	auto ecg_data = std::make_shared<ecg::EcgData>();
	ecg_data->signals.Insert("RawData", ts1);
	ecg_data->signals.Insert("RawData2", ts2);

	return ecg_data;
}

} //namespace input_system
