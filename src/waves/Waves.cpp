#include <ecg_signal_analyzer/waves/Waves.hpp>

#include <iostream>
#include <utility>
#include <gsl/gsl_wavelet.h>
#include <gsl/gsl_sort.h>
#include <math.h>
#include <vector>
#include <algorithm>
#include <iterator>
namespace ecg
{

Waves::Waves()
{
}

std::vector<std::pair<double, double>> Waves::Gradient(std::vector<std::pair<double, double>> signal){
    auto signalSize = signal.size();
    std::vector<std::pair<double, double>> tempSig;
    double min, max;
    int offset = 5;
    std::copy(signal.begin(), signal.begin()+offset, std::back_inserter(tempSig));

    for(auto it = (signal.begin()+offset); it!=(signal.end()-offset); it++){
        min = std::min_element(it-offset, it+offset, [](std::pair<double, double> el1, std::pair<double, double> el2){return (el1.first<el2.first); })->first;
        max = std::max_element(it-offset, it+offset, [](std::pair<double, double> el1, std::pair<double, double> el2){return (el1.first<el2.first); })->first;
        tempSig.push_back(std::make_pair(max-min, it->second));
    }
    std::copy(signal.end()-offset, signal.end(), std::back_inserter(tempSig));

    return tempSig;
}

std::vector<std::pair<double, double>> Waves::AverageFilter(std::vector<std::pair<double, double>> &signal){
    int offset = 5;
    double sumC = 0;
    std::vector<std::pair<double, double>> tempSig;

    for(int j=0; j<11; j++){
        sumC += signal[j].first;
    }
    for(auto it=signal.begin(); it!=signal.begin()+offset; it++){
        tempSig.push_back(std::make_pair(sumC/11, it->second));
    }
    for(auto it=signal.begin()+offset+1; it!=signal.end()-offset; it++){
        sumC = sumC - (it-6)->first + (it+5)->first;
        tempSig.push_back(std::make_pair(sumC/11, (it-1)->second));
    }
    for(auto it=signal.end()-offset-1; it!=signal.end(); it++){
        tempSig.push_back(std::make_pair(sumC/11, it->second));
    }
    return tempSig;
}






int Waves::ProcessData(const InputsType &inputs, const OutputsType &outputs)
{
    std::vector<std::pair<double, double>> Rpeaks;
    std::vector<std::pair<double, double>> FilteredData;
    //dynamic array for wavelet tranformation
    size_t n = 0;

    for (const auto &input : inputs)
    {
        std::cout << "This is the signal name: " << input.first << std::endl;
        auto data_range = input.second;
        if(input.first == "RpeaksData"){
            for (auto it = data_range.first; it != data_range.second; ++it){
                //std::cout << it->value << " " << it->time << std::endl;
                Rpeaks.push_back(std::make_pair(it->value, it->time));          
            }
        }
        if(input.first == "FilteredData"){
            n = data_range.second - data_range.first;
            for (auto it = data_range.first; it != data_range.second; ++it){
                FilteredData.push_back(std::make_pair(it->value, it->time));  
            }
        }
    }

    //power 
    std::vector<std::pair<double, double>> Power = FilteredData;
    std::for_each(Power.begin(), Power.end(), [](std::pair<double, double> &el){el.first = el.first*el.first; });
    //std::cout << FilteredData.size() << std::endl;

    //gradient
    std::vector<std::pair<double, double>> GradSig = Gradient(Power);
    //std::cout << GradSig.size() << " " << FilteredData.size() << std::endl;

    //average
    std::vector<std::pair<double, double>> fg1Sig = AverageFilter(GradSig);
    //std::cout << fg1Sig.size() << " " << FilteredData.size() << std::endl;

    //exp - from gradient
    std::vector<std::pair<double, double>> ExpSig = GradSig;
    std::for_each(ExpSig.begin(), ExpSig.end(),[](std::pair<double, double> &el){ 1-(2/std::exp(2*el.first)+1);} );
    //std::cout << "Module Waves";

    //gradient-exp
    GradSig = Gradient(ExpSig);

    //average fg2sig
    std::vector<std::pair<double, double>> fg2sig = GradSig;
    fg2sig = AverageFilter(GradSig);
    //std::vector<std::pair<double, double>> fg2sig = AverageFilter(GradSig);

    //ts3sig
    std::vector<std::pair<double, double>> ts3Sig;
    for(auto i=0; i<FilteredData.size(); i++){
        ts3Sig.push_back(std::make_pair(FilteredData[i].first*fg2sig[i].first, FilteredData[i].second));
    }

    //gradient
    GradSig = Gradient(ExpSig);

    //fg3
    std::vector<std::pair<double, double>> fg3sig = GradSig;
    fg3sig = AverageFilter(GradSig);

    //ts4Sig
    std::vector<std::pair<double, double>> fs4sig;
    for(auto i=0; i<FilteredData.size(); i++){
        fs4sig.push_back(std::make_pair(fg3sig[i].first+fg1Sig[i].first, FilteredData[i].second));
        //std::cout << fs4sig[i].first << " ";
    }
    //std::cout << std::endl << ts3Sig.size() << std::endl;

    //normalizacja
    double min, max;
    min = std::min_element(fs4sig.begin(), fs4sig.end(), [](std::pair<double, double> el1, std::pair<double, double> el2){return (el1.first<el2.first); })->first;
    max = std::max_element(Rpeaks.begin(), Rpeaks.end(), [](std::pair<double, double> el1, std::pair<double, double> el2){return (el1.first<el2.first); })->first;
    std::vector<std::pair<double, double>> normSig = fs4sig;
    std::for_each(normSig.begin(), normSig.end(), [max, min](std::pair<double, double> &el){ (el.first-min)/(max-min); });

    //cut less than 5%, nie jestem pewna, czy to dziala
    std::for_each(normSig.begin(), normSig.end(), [](std::pair<double, double> &el) -> void {
        if(el.first<0.06){
            el.first = 0;}});

    std::vector<std::pair<double, double>> QRSi;
    std::vector<int> QRSiIdx;
    std::vector<std::pair<double, double>> QRSe;
    std::vector<int> QRSeIdx;
    auto pos = FilteredData.begin();
    for(auto Rpair : Rpeaks){
        pos = std::find(pos, FilteredData.end(), Rpair);
        int idx = pos-FilteredData.begin();

        //find QRSi points
        for(int i=idx; i>(idx-100); --i){
            if(normSig[i].first==0){
                QRSi.push_back(FilteredData[i]);
                QRSiIdx.push_back(i);
                break;
            }
        }

        //find QRSe points
        for(int i=idx; i<(idx+100); ++i){
            if(normSig[i].first==0){
                QRSe.push_back(FilteredData[i]);
                QRSeIdx.push_back(i);
                break;
            }
        }
    }

    ////P DETECTION
    std::vector<std::pair<double, double>> PTSignal = FilteredData;
    std::vector<std::pair<double, double>> Pi;
    std::vector<int> PiIdx;
    std::vector<std::pair<double, double>> Pe;
    std::vector<int> PeIdx;
    std::vector<std::pair<double, double>> Te;
    std::vector<int> TeIdx;
    
    for(int i=0; i<QRSeIdx.size()-1; ++i){
        int first = QRSeIdx[i];
        int last =  QRSiIdx[i+1];
        int window_size = last-first;

        //zero value to QRS
        for(auto j=QRSiIdx[i]; j!=QRSeIdx[i]; j++){
            PTSignal[j].first = 0;
        }
        
        auto off = 2;
        //nie wiem czy potrzebne
        if ((last-first)<off) continue;
        min = std::min_element(FilteredData.begin()+first+off, FilteredData.begin()+last, [](std::pair<double, double> el1, std::pair<double, double> el2){return (el1.first<el2.first); })->first;
        max = std::max_element(FilteredData.begin()+first+off, FilteredData.begin()+last, [](std::pair<double, double> el1, std::pair<double, double> el2){return (el1.first<el2.first); })->first;
        
        std::vector<double> Window;
        for(auto j=first; j!=last; ++j){
            auto value = (FilteredData[j].first-min)/(max-min);
            PTSignal[j].first = value;
            Window.push_back(value);
        }
        
        //median
        std::sort(Window.begin(), Window.end());
        int id = Window.size()/2;
        double median = Window[id];

        for(auto j=first; j!=last; ++j){
            PTSignal[j].first -= median;
            PTSignal[j].first = (PTSignal[j].first>0.02)?1:0;
        }
        
        
        //finding T points
        double TiInd = 0, TeInd = 0;
        for(auto j=first; j<(first + window_size*3/4); ++j){
            auto value = PTSignal[j];
            if(TiInd==0){
                if(value.first>0){ TiInd = j; }
            }
            else if(value.first<1){
                TeInd = j;
                if(TeInd-TiInd>(window_size/100*15)){
                    Te.push_back(FilteredData[TeInd]);
                    TeIdx.push_back(TeInd);
                    //std::cout << TeInd << " ";
                    break;
                }
                else{
                    TeInd=TiInd=0;
                }
            }
        }

        //finding P points
        double PiInd = 0, PeInd = 0;
        //for(auto j=(first + window_size*2/4); j<last; ++j){
        for(auto j=(TeInd + window_size*20/100); j<last; ++j){
            auto value = PTSignal[j];
            if(PiInd==0){
                if(value.first>0){ PiInd = j; }
            }
            else if(value.first<1){
                PeInd = j;
                if((PeInd-PiInd)>(window_size/100*10)){
                    Pi.push_back(FilteredData[PiInd]);
                    PiIdx.push_back(PiInd);
                    Pe.push_back(FilteredData[PeInd]);
                    PeIdx.push_back(PeInd);
                    //std::cout << TeInd << " ";
                    break;
                }
                else{
                    PeInd=PiInd=0;
                }
            }
        }
    }

    for (auto &output : outputs)
    {
        std::cout << "Output name: " << output.first << "\n";
        auto &output_series = output.second.get();
        if(output.first=="QRSi"){
            size_t i = 0;
            output_series.points.resize(QRSi.size());
            for (auto &point : QRSi){
        	    output_series.points[i].value = point.first;
        	    output_series.points[i].time = point.second;
                i++;
            }
        }

        if(output.first=="QRSe"){
            size_t i = 0;
            output_series.points.resize(QRSe.size());
            for (auto &point : QRSe){
        	    output_series.points[i].value = point.first;
        	    output_series.points[i].time = point.second;
                i++;
            }
            std::cout << "i = " << i << std::endl;
        }

        if(output.first=="Te"){
            size_t i = 0;
            output_series.points.resize(Te.size());
            for (auto &point : Te){
        	    output_series.points[i].value = point.first;
        	    output_series.points[i].time = point.second;
                i++;
            }
        }
        
        if(output.first=="Pi"){
            size_t i = 0;
            output_series.points.resize(Pi.size());
            for (auto &point : Pi){
        	    output_series.points[i].value = point.first;
        	    output_series.points[i].time = point.second;
                i++;
            }
        }

        if(output.first=="Pe"){
            size_t i = 0;
            output_series.points.resize(Pe.size());
            for (auto &point : Pe){
        	    output_series.points[i].value = point.first;
        	    output_series.points[i].time = point.second;
                i++;
            }
        }
    }

    /*
    std::cout << "Signal \n";
    for(auto i=0; i<1600; i++){
        std::cout << FilteredData[i].first << " ";
    } 

    std::cout <<std::endl <<Te.size();
    std::cout << "\n Te - value, number\n";
    for(auto i=0; i<10; i++){
        std::cout << FilteredData[TeIdx[i]].first << " ";
    } 
    std::cout<<std::endl;
    for(auto i=0; i<10; i++){
        std::cout << TeIdx[i] << " ";
    } 
    std::cout <<std::endl <<Pi.size();
    std::cout << "\n Pi - value, number\n";
    for(auto i=0; i<10; i++){
        std::cout << FilteredData[PiIdx[i]].first << " ";
    } 
    std::cout<<std::endl;
    for(auto i=0; i<10; i++){
        std::cout << PiIdx[i] << " ";
    } 
    std::cout <<std::endl <<Pe.size();
    std::cout << "\n Pe - value, number\n";
    for(auto i=0; i<10; i++){
        std::cout << FilteredData[PeIdx[i]].first << " ";
    } 
    std::cout<<std::endl;
    for(auto i=0; i<10; i++){
        std::cout << PeIdx[i] << " ";
    } 

    
    //Results
    std::cout <<std::endl <<QRSi.size();
    std::cout << "QRSi - value, number\n";
    for(auto i=0; i<10; i++){
        std::cout << QRSiIdx[i] << " ";
    } 
    
    std::cout << "\n QRSe - value, number\n"; 
    std::cout<<std::endl;
    for(auto i=0; i<10; i++){
        std::cout << QRSeIdx[i] << " ";
    } 
/*
    std::cout<<std::endl;
    std::cout<<std::endl;
    std::cout << "Filtered Data\n";

    for(auto i=0; i<1500; i++){
        std::cout << FilteredData[i].first << " ";
    } 
    std::cout<<std::endl;
    std::cout<<std::endl; */
    
    std::cout << "[Waves]  Data processing succes!" << std::endl;
    return 0;
}


} // namespace ecg