// Header file
#include <ecg_signal_analyzer/core/WorkflowSupervisor.hpp>

// Custom
#include <ecg_signal_analyzer/core/IEcgTaskSpawner.hpp>
#include <ecg_signal_analyzer/core/ConcurrentSet.hpp>
#include <ecg_signal_analyzer/core/ConcurrentList.hpp>

// STL
#include <queue>
#include <algorithm>
#include <condition_variable>
#include <list>

// BOOST
#include <boost/property_tree/ptree.hpp>      // Deserializing the execution graph
#include <boost/property_tree/xml_parser.hpp> // from an xml config file.

// Other
#include <iostream>
#include <experimental/filesystem>
// #include <filesystem>

namespace ecg
{

WorkflowSupervisor::WorkflowSupervisor()
{
    LoadExecutionGraph("resources/config.xml");
}

WorkflowSupervisor::WorkflowSupervisor(std::string config_path)
{
    LoadExecutionGraph(config_path);
}

void WorkflowSupervisor::SetLoader(const std::shared_ptr<IInputModule> &loader)
{
    loader_ = loader;
}

void WorkflowSupervisor::Load(const std::string &path)
{
    data_ = loader_->Load(path);
}

const std::shared_ptr<IInputModule> &WorkflowSupervisor::GetLoader() const
{
    return loader_;
}

void WorkflowSupervisor::Run(uint64_t first, uint64_t last)
{
    // detail::Queue<IEcgTaskPtr> tasks_left;
    detail::ConcurrentSet<IEcgTaskPtr> tasks_left;
    detail::ConcurrentSet<IEcgTaskPtr> tasks_done;
    std::condition_variable wake_up;

    ResetRange(first, last);

    // tasks_left.Push(root_task_);
    tasks_left.Insert(root_task_);

    using namespace std::chrono_literals;

    std::mutex cv_mutex;

    std::vector<std::future<int>> results;

    std::list<InputsType> inputs_buffer;   // Make the input / output hashmaps persist on their threaded jobs.
    std::list<OutputsType> outputs_buffer; // Thread safety is guaranteed by the execution order of tasks.

    while (!tasks_left.Empty())
    {
        std::unique_lock<std::mutex> ulock(cv_mutex);
        // while (!tasks_left.Empty())
        // {
        //     auto task = tasks_left.Front();
        //     auto parent = task->parent_.lock();
        //     auto result = std::find(tasks_done.begin(), tasks_done.end(), parent);

        //     if (result != tasks_done.end() || parent == nullptr)
        //     {
        //         inputs_buffer.push_back(GetTaskInputs(task, first, last));
        //         outputs_buffer.push_back(GetTaskOutputs(task));

        //         results.push_back(task->RunAsync(inputs_buffer.back(), outputs_buffer.back(),
        //                                          tasks_left, tasks_done, wake_up, cv_mutex));
        //         tasks_left.Pop();
        //     }
        // }

        for (auto task : tasks_left)
        {
            auto parent = task->parent_.lock();
            auto result = std::find(tasks_done.begin(), tasks_done.end(), parent);

            if (result != tasks_done.end() || parent == nullptr)
            {
                inputs_buffer.push_back(GetTaskInputs(task, first, last));
                outputs_buffer.push_back(GetTaskOutputs(task));

                results.push_back(task->RunAsync(inputs_buffer.back(), outputs_buffer.back(),
                                                 tasks_left, tasks_done, wake_up, cv_mutex));
                // tasks_left.Pop();
                tasks_left.Erase(task);
            }
        }

        if (tasks_left.Empty())
        {
            wake_up.wait(ulock);
        }
    }

    for (auto &result : results)
    {
        result.get();
    }
}

void WorkflowSupervisor::LoadExecutionGraph(std::string config_path)
{
    namespace fs = std::experimental::filesystem;

    namespace pt = boost::property_tree;

    if (!fs::exists(config_path))
    {
        throw std::runtime_error("Invalid XML: File not found.");
    }

    if (!fs::is_regular_file(config_path))
    {
        throw std::runtime_error("Invalid XML: File is not a regular file.");
    }

    pt::ptree config_tree;
    pt::read_xml(config_path, config_tree, pt::xml_parser::no_comments);

    if (config_tree.size() != 1)
    {
        throw std::runtime_error("Invalid XML: Wrong number of root nodes.");
    }

    // Braces to prevent scope cluttering.
    {
        auto root_name = config_tree.begin()->first;
        if (root_name != "execution_graph")
        {
            throw std::runtime_error("Invalid XML: The root node must be called \'execution_graph\', but is actually called \'" + root_name + "\'.");
        }
    }

    std::string root_task_name = "";

    std::map<std::string, std::string> parent_names;

    for (auto &task : config_tree.begin()->second)
    {
        // Verify that this is a 'task' node.
        if (task.first != "task")
        {
            throw std::runtime_error("Invalid XML: Expected a \'task\' node, but the node is called \'" + task.first + "\'");
        }
        std::cout << "---TASK---\n";

        // Get type
        auto task_type = task.second.get("type", "");
        if (task_type == "")
        {
            throw std::runtime_error("Invalid XML: Task has no type.");
        }
        std::cout << "  Type: " << task_type << "\n";

        // Get instance
        auto task_ptr = IEcgTaskSpawner::Spawn(task_type);
        if (!task_ptr)
        {
            throw std::runtime_error("Invalid XML: Unrecognized task type: " + task_type);
        }

        // Get name
        auto task_name = task.second.get("name", "");
        if (task_type == "")
        {
            throw std::runtime_error("Invalid XML: Task has no name.");
        }
        std::cout << "  Name: " << task_name << "\n";
        task_ptr->name_ = task_name;

        // Get parent
        auto task_parent = task.second.get("parent", "");
        if (task_parent == "" && root_task_name != "")
        {
            throw std::runtime_error("Invalid XML: Found more than one root task (task with no parent node): " + task_name);
        }
        std::cout << "  Parent: " << task_parent << "\n";

        // If this is the root task, save its name. Otherwise, associate this task with the parent to connect them later.
        if (task_parent == "")
        {
            root_task_name = task_name;
        }
        else
        {
            parent_names[task_name] = task_parent;
        }

        // Get Input Signals
        {
            // Verify that the inputs node is there.
            pt::ptree inputs;
            try
            {
                inputs = task.second.get_child("inputs");
            }
            catch (...)
            {
                throw std::runtime_error("Invalid XML: No 'inputs' node provided for task: " + task_name);
            }
            std::cout << "  Inputs:\n";
            // Iterate over all nodes within it.
            for (auto &input : inputs)
            {
                // Only allow 'signal' nodes with valid (non-empty) contents.
                if (input.first != "signal")
                {
                    throw std::runtime_error("Invalid XML: the 'inputs' node can only contain 'signal' nodes, but contains a \'" + input.first + "\' node for task: " + task_name);
                }
                std::string signal = input.second.get("", "");
                if (signal == "")
                {
                    throw std::runtime_error("Invalid XML: Input signal has no name in task: " + task_name);
                }
                task_ptr->inputs_.push_back(signal);
                std::cout << "    " << signal << "\n";
            }
        }

        // Get Output Signals
        {
            // Verify that the 'outputs' node is there.
            pt::ptree outputs;
            try
            {
                outputs = task.second.get_child("outputs");
            }
            catch (...)
            {
                throw std::runtime_error("Invalid XML: No 'outputs' node provided for task: " + task_name);
            }
            std::cout << "  Outputs:\n";
            // Iterate over all nodes within it.
            for (auto &output : outputs)
            {
                // Only allow 'signal' nodes with valid (non-empty) contents.
                if (output.first != "signal")
                {
                    throw std::runtime_error("Invalid XML: the 'outputs' node can only contain 'signal' nodes, but contains a \'" + output.first + "\' node for task: " + task_name);
                }
                std::string signal = output.second.get("", "");
                if (signal == "")
                {
                    throw std::runtime_error("Invalid XML: Output signal has no name in task: " + task_name);
                }
                task_ptr->outputs_.push_back(signal);
                std::cout << "    " << signal << "\n";
            }
        }
        tasks_[task_name] = task_ptr;
        std::cout << std::endl;
    }

    if (root_task_name == "")
    {
        throw std::runtime_error("Invalid XML: No root task provided. There must be exactly one task without a parent node.");
    }

    for (auto &link : parent_names)
    {
        auto &child = tasks_[link.first];
        auto &parent = tasks_[link.second];

        parent->AddChild(child);
    }

    root_task_ = tasks_[root_task_name];
    // std::function<void(pt::ptree &, std::string)> print;
    // print = [&print](pt::ptree &config_tree, std::string prefix) {
    //     for (auto &p : config_tree)
    //     {
    //         std::cout << prefix << p.first << std::endl;
    //         print(p.second, prefix + "  ");
    //         std::cout << prefix << "/" << p.first << std::endl;
    //     }
    // };
    // print(config_tree, "");
}

auto WorkflowSupervisor::GetTaskInputs(std::string task_name, uint64_t first, uint64_t last)
    -> std::unordered_map<std::string, decltype(((TimeSeries<double> *)nullptr)->Range(0, 0))>
{
    auto task = tasks_.find(task_name);
    if (task == end(tasks_))
    {
        throw std::runtime_error("WorkflowSupervisor::GetTaskInputs(std::string, uint64_t, uint64_t): Task " + task_name + " not found.");
    }
    return GetTaskInputs(task->second, first, last);
}

auto WorkflowSupervisor::GetTaskInputs(IEcgTaskPtr task, uint64_t first, uint64_t last)
    -> std::unordered_map<std::string, decltype(((TimeSeries<double> *)nullptr)->Range(0, 0))>
{
    auto input_names = task->inputs_;

    std::unordered_map<std::string, decltype(((TimeSeries<double> *)nullptr)->Range(0, 0))> inputs;

    std::for_each(begin(input_names), end(input_names), [&inputs, first, last, this](const std::string &input_name) {
        inputs[input_name] = data_->signals[input_name].Range(first, last);
    });

    return inputs;
}

auto WorkflowSupervisor::GetTaskOutputs(std::string task_name)
    -> std::unordered_map<std::string, std::reference_wrapper<TimeSeries<double>>>
{
    auto task = tasks_.find(task_name);
    if (task == end(tasks_))
    {
        throw std::runtime_error("WorkflowSupervisor::GetTaskOutputs(std::string): Task " + task_name + " not found.");
    }

    return GetTaskOutputs(task->second);
}

auto WorkflowSupervisor::GetTaskOutputs(IEcgTaskPtr task)
    -> std::unordered_map<std::string, std::reference_wrapper<TimeSeries<double>>>
{
    auto output_names = task->outputs_;
    std::unordered_map<std::string, std::reference_wrapper<TimeSeries<double>>> outputs;

    std::for_each(begin(output_names), end(output_names), [&outputs, this](const std::string &output_name) {
        outputs.try_emplace(output_name, std::ref(data_->signals[output_name]));
    });

    return outputs;
}

const EcgDataPtr &WorkflowSupervisor::Data()
{
    return data_;
}

void WorkflowSupervisor::ResetRange(uint64_t first, uint64_t last)
{
    if (root_task_ == nullptr)
    {
        std::cerr << "WorkflowSupervisor::ResetRange called, but no root task was set." << std::endl;
    }

    for (auto &s : data_->signals)
    {
        auto s_found = std::find(begin(root_task_->inputs_), end(root_task_->inputs_), s.first);

        // Clear data that is not an input to the root task.
        // This clears all data that was produced by tasks,
        // and keeps the data that was read from a file.
        if (s_found == end(root_task_->inputs_))
        {
            s.second.Clear(first, last);
        }
    }
}

} // namespace ecg