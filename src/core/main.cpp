#include <ecg_signal_analyzer/core/IEcgTask.hpp>
#include <ecg_signal_analyzer/core/EcgData.hpp>
#include <ecg_signal_analyzer/core/WorkflowSupervisor.hpp>
#include <ecg_signal_analyzer/core/IInputModule.hpp>
#include <ecg_signal_analyzer/input_system/WfdbWrapper.hpp>
#include <iostream>

int main()
{
    ecg::WorkflowSupervisor ws("resources/config.xml");

    ws.SetLoader(std::make_shared<ecg::WfdbWrapper>());
    std::string path = "resources/database/209";
    ws.Load(path);   // Load a test file here
    ws.Run();

    // ws.Data()->signals["RawData"] = ecg::TimeSeries<double>{{{0.0, 0}, // Until loader is tested and integrated, mock some data here.
    //                                                          {1.0, 1},
    //                                                          {2.0, 2},
    //                                                          {3.0, 3},
    //                                                          {4.0, 4},
    //                                                          {5.0, 5}}};
    // ws.Run(1, 4);

    std::cout << "Workflow done." << std::endl;

    return 0;
}