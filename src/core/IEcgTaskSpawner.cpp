#include <ecg_signal_analyzer/core/IEcgTaskSpawner.hpp>
#include <ecg_signal_analyzer/core/IEcgTask.hpp>

#include <ecg_signal_analyzer/example_task/ExampleEcgTask.hpp>
#include <ecg_signal_analyzer/ecg_baseline/EcgBaselineTask.hpp>
#include <ecg_signal_analyzer/rpeaks/Rpeaks.hpp>
#include <ecg_signal_analyzer/hrv1/hrv1.hpp>
#include <ecg_signal_analyzer/hrv2/HRV2.hpp>
#include <ecg_signal_analyzer/hrv_dfa/HrvDfaTask.hpp>
#include <ecg_signal_analyzer/waves/Waves.hpp>
#include <ecg_signal_analyzer/st_segment/STSegmentTask.hpp>
#include <ecg_signal_analyzer/t_wave_alt/TWaveAltTask.hpp>


// Macro to register new task types with the spawner.
// Do NOT change this without good reason.
#define REGISTER(TASK) \
  if (task == #TASK)   \
    return std::make_shared<TASK>();

namespace ecg
{

IEcgTaskPtr IEcgTaskSpawner::Spawn(std::string task)
{

  REGISTER(ExampleEcgTask)
  REGISTER(EcgBaselineTask)
  REGISTER(Rpeaks)
  REGISTER(HRV1)
  REGISTER(HRV2)
  REGISTER(HrvDfaTask)
  REGISTER(Waves)
  REGISTER(STSegmentTask)
  REGISTER(TWaveAltTask)


  // Default (empty) return value. Do not remove this.
  return std::shared_ptr<IEcgTask>();
}

} // namespace ecg
