#include <ecg_signal_analyzer/core/IEcgTask.hpp>
#include <ecg_signal_analyzer/core/EcgData.hpp>
#include <thread> // explicitly inlcude hardware_concurrency()

#include <memory>
namespace ecg
{

std::unique_ptr<ThreadPool> IEcgTask::thread_pool_ = std::make_unique<ThreadPool>(std::thread::hardware_concurrency());

std::future<int> IEcgTask::RunAsync(const InputsType &inputs,
                                    const OutputsType &outputs,
                                    // detail::Queue<IEcgTaskPtr> &tasks_left,
                                    detail::ConcurrentSet<IEcgTaskPtr> &tasks_left,
                                    detail::ConcurrentSet<IEcgTaskPtr> &tasks_done,
                                    std::condition_variable &wake_up,
                                    std::mutex &wake_up_mutex)
{
    auto run = [](IEcgTaskPtr task,
                  const InputsType &inputs,
                  const OutputsType &outputs,
                  //   detail::Queue<IEcgTaskPtr> &tasks_left,
                  detail::ConcurrentSet<IEcgTaskPtr> &tasks_left,
                  detail::ConcurrentSet<IEcgTaskPtr> &tasks_done,
                  std::condition_variable &wake_up,
                  std::mutex &wake_up_mutex) {
        for (const auto &child : task->GetChildren())
        {
            // tasks_left.Push(child);
            tasks_left.Insert(child);
        }

        std::cout << "Starting task: " << task->Name() << std::endl;

        int result = task->ProcessData(inputs, outputs);

        std::unique_lock<std::mutex> lock(wake_up_mutex);
        std::cout << "Task done.\n"
                  << std::endl;
        tasks_done.Insert(task);

        wake_up.notify_one();

        return result;
    };
    return thread_pool_->AddJob(run, shared_from_this(), std::ref(inputs), std::ref(outputs), std::ref(tasks_left), std::ref(tasks_done), std::ref(wake_up), std::ref(wake_up_mutex));
}

int IEcgTask::Run(const InputsType &inputs, const OutputsType &outputs)
{
    return ProcessData(inputs, outputs);
}

void IEcgTask::AddChild(IEcgTaskPtr child)
{
    children_.insert(child);
    child->parent_ = shared_from_this();
}

const std::set<IEcgTaskPtr> &IEcgTask::GetChildren() const
{
    return children_;
}

void IEcgTask::AddParameter(const std::string &key, const std::any &value)
{
    task_parameters_[key] = value;
}

} // namespace ecg