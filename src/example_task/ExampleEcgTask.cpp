#include <ecg_signal_analyzer/example_task/ExampleEcgTask.hpp>

#include <iostream>
namespace ecg
{

ExampleEcgTask::ExampleEcgTask()
{
    // The Constructor is needed only for parameter initialization (parameters could be changed in GUI).
    // task_parameters_ is a hash map where the key is string and the value is std::any.
    // Thanks to the std::any you can put any parameter type you want inside the map, just like in the python dictionary.
    // Parameters can be added by operator[] or AddParameter(std::string, std::any) method function, just like presented below.
    task_parameters_["example0"] = 2137;
    task_parameters_["czypapiesz"] = true;
    AddParameter("example2", std::vector<float>(69, 1));
}

int ExampleEcgTask::ProcessData(const InputsType &inputs, const OutputsType &outputs)
{
    // First you can acces the parameters of your node. Due to the fact that the value is std::any accessing parameters requires casting.
    // You can either do it manually or call templated method GetParameter<T>(std::string key).
    // Casting value on wrong type will result in segmentation fault, so don't do that ;)
    {
        int param1 = GetParameter<int>("example0");
        bool param2 = GetParameter<bool>("czypapiesz");
        std::vector<float> param3 = std::any_cast<std::vector<float>>(task_parameters_["example2"]);
    }

    // Input data is given to you using pairs of iterators.
    // Because 'inputs' is a const reference, you cannot access the signals by name:
    {
        // auto some_signal = inputs["MySignalName"]; // Compilation error.
    }
    // Instead, you must iterate through them like any other container.
    for (const auto &input : inputs)
    {
        // input here is of type std::pair<std::string, std::pair<it, it>>. Pair-ception!
        // So, input.first will be a string - the name of the signal.
        std::cout << "This is the signal name: " << input.first << std::endl;

        // input.second will give you the pair of iterators. You can use them to access the values of your input data.
        auto data_range = input.second;
        int i = 0;
        for (auto it = data_range.first; it != data_range.second; ++it)
        {
            // if(i<100){
            // std::cout << "Value of " << it->value << " at time " << it->time << "\n";}
            // i++;
            // Do super smart signal processing stuff here.
            // You could also copy this data into a vector for convenience, although
            // this is not recommended because of performance.
        }
        // std::cout << std::endl;
    }

    // When you have your results ready, store them in your output data.
    // Again, since this is a const reference, you can not ask for the signals by name:
    {
        // auto & some_signal = outputs["MySignalName"] // Compilation errors gallore!
    }
    // So you have to iterate through it.
    for (auto &output : outputs)
    {
        // Here, output is of type std::pair<std::string, std::reference_wrapper<TimeSeries<double>>>.
        // So, output.first is again, the name of the signal.
        std::cout << "Output name: " << output.first << "\n";

        // output.second is the reference wrapper - use get() on it, and you have direct access to the TimeSeries object.
        auto &output_series = output.second.get();

        // By now you should have your output data. This example will mock it up right here (notice the {value, timestamp} pairs)
        TimeSeries<double> some_output = {{{1.0, 1},
                                           {1.1, 2},
                                           {0.9, 3},
                                           {0.7, 5}}};

        // Now, we have to put it in the right place of our output reference.
        // If you know you are the only one writing into it, you can assume
        // it is empty and insert your data at the beginning.
        // If not, it's up to you to guarantee the ascending order of timestamps,
        // so find the right spots to put your data into:

        size_t start_from = 0; // Use this to avoid searching from the beginning each time.
        for (auto &point : some_output.points)
        {
            // std::cout << "Value of " << point.value << " at time " << point.time << std::endl;
            auto it = output_series.Find(point.time, start_from);
            start_from = std::distance(begin(output_series.points), std::next(it)); // In the next iteration, search for the right spot will start from here.
            output_series.points.insert(it, point);
        }
    }

    // All good! Since nothing went wrong, return 0 as a success code.
    return 0;
}

} // namespace ecg