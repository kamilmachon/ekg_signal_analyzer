#include <ecg_signal_analyzer/hrv1/hrv1.hpp>

#include <iostream>
#include <vector>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <fftw3.h>
#include <gsl/gsl_spline.h>

namespace ecg
{

HRV1::HRV1()
{
    // The Constructor is needed only for parameter initialization (parameters could be changed in GUI).
    // task_parameters_ is a hash map where the key is string and the value is std::any.
    // Thanks to the std::any you can put any parameter type you want inside the map, just like in the python dictionary.
    // Parameters can be added by operator[] or AddParameter(std::string, std::any) method function, just like presented below.
    task_parameters_["sampl_freq"] = 7.0;
    task_parameters_["window_size"] = 3;
}

int HRV1::ProcessData(const InputsType &inputs, const OutputsType &outputs)
{
    // First you can acces the parameters of your node. Due to the fact that the value is std::any accessing parameters requires casting.
    // You can either do it manually or call templated method GetParameter<T>(std::string key).
    // Casting value on wrong type will result in segmentation fault, so don't do that ;)
    //{
        //int param1 = GetParameter<int>("example0");
        //bool param2 = GetParameter<bool>("czypapiesz");
        //std::vector<float> param3 = std::any_cast<std::vector<float>>(task_parameters_["example2"]);
    //}

	//Timestamps of RR intervals in two types: uint64 and double
	std::vector<uint64_t> timestamp_uint64;
	std::vector<double> timestamp_double;

	//Values of RR intervals and differences between two next RR intervals
    std::vector<double> RR;
	std::vector<double> RR_diff;

	//Number of RR intervals
	int size_RR = 0;

	//Sum of RR intervals (up to 300000ms) to find indexes
	//of 5min parts of signal
	double suma_5min = 0.0;

	//Indexes of 5min intervals
	std::vector<int> intervals_5min;
	intervals_5min.push_back(0);

	//Values of three time analysis parameters (computed for whole signal)
	float SDANN;
	float SDANNind;
	float SDSD;
	
	//Timestamps for output timeseries (with step 5min)
	std::vector<uint64_t> timestamp_5min;
	
	//Values of time analysis parameters 
	//(computed for each 5min part of signal)
	std::vector<double> mean_5min;
	std::vector<double> std_5min;
	std::vector<double> rmssd_5min;
	std::vector<double> nn50_5min;
	std::vector<double> pnn50_5min;

	//Values of frequency analysis parameters
	//(computed for each 5min part of signal)
	std::vector<double> hf_5min;
	std::vector<double> lf_5min;
	std::vector<double> vlf_5min;
	std::vector<double> ulf_5min;
	std::vector<double> tp_5min;
	std::vector<double> lfhf_5min;

	//Output vector consisting values of parameters computed from whole signal
	std::vector<double> other_data;

	//RR intervals and timestamps after resampling
	std::vector<double> resampled_RR;
	std::vector<double> resampled_time;

	//Output (magnitude and frequency) after FFT operation
	std::vector<double> fft_magnitude;
	std::vector<double> fft_frequency;

	//Window size of average filter for smoothing FFT output
	int avgFilter_window_size = GetParameter<int>("window_size");;
	std::vector<double> filtered_fft_magnitude;

	//Values of frequency analysis parameters 
	//(computed for whole signal)
	double HF = 0.0;
	double LF = 0.0;
	double VLF = 0.0;
	double ULF = 0.0;
	double TP, LFHF;

    for (const auto &input : inputs)
    {
        std::cout<<"[HRV1] This is the signal name: "<<input.first<< std::endl;

        auto data_range = input.second;
		double time_period = ((data_range.second-1)->time - data_range.first->time)/1000000.0;
		
		//std::ofstream output_file;
		//output_file.open("/home/esdemit/hrv1_end_FFT_filtr.csv");
		//output_file << "Timestamp, Value" << std::endl;
		
		//Time of first Rpeak
		uint64_t time_of_I_Rpeak = (data_range.first)->time;

		//Iterate through all data from Rpeaks module
        for (auto it = data_range.first; it != data_range.second-1; ++it)
        {
			//RR interval
			double RR_interval = (double)(((it+1)->time - it->time)/1000000.0);
			//Timestamp of RR interval (uint64)
			uint64_t RR_time = (it+1)->time;
			
			//Find RR intervals longer than 2200ms, generate artificial Rpeak
			if(RR_interval > 2200)
			{
				double half_RR = (double)(RR_interval)/2.0;
				double add_RR = (double)(RR_interval) - half_RR;
				uint64_t add_time = RR_time - (uint64_t)(half_RR*1000000);

				RR.push_back(add_RR);
				RR.push_back(half_RR);
				timestamp_double.push_back((double)(add_time-time_of_I_Rpeak));
				timestamp_double.push_back((double)(RR_time-time_of_I_Rpeak));
				timestamp_uint64.push_back(add_time);
				timestamp_uint64.push_back(RR_time);
				size_RR += 2;
			}
			else
			{
				RR.push_back(RR_interval);
				timestamp_double.push_back((double)(RR_time - time_of_I_Rpeak));
				timestamp_uint64.push_back(RR_time);
				size_RR++;
			}
			
			//Sum length of RR intervals
			suma_5min += RR_interval;

			//Find indexes of 5min parts of signal, to compute HRV1 analysis
			if(suma_5min >= 300000.0)
			{
				suma_5min = 0.0;
				intervals_5min.push_back(size_RR);
			}
        }

		if(size_RR - intervals_5min[intervals_5min.size()-1] > 3)
		{
			intervals_5min.push_back(size_RR);
		}

		//Save timestamps and RR values to csv file
		//for(int i=0; i<filtered_fft_magnitude.size();i++)
		//{
			//output_file<<fft_frequency[i]<<", "<<filtered_fft_magnitude[i]<< std::endl;
		//}
		//output_file.close();

		std::cout << "[HRV1] Time analysis proceeding." << std::endl;

		//Compute difference between two RR intervals
		for(int i=0; i<RR.size()-1; i++)
		{
			RR_diff.push_back(RR[i+1]-RR[i]);
		}
		//Compute value of SDSD parameter
		SDSD = (float)(compute_std(RR_diff));
		other_data.push_back(SDSD);

		//std::cout<<std::endl<<"Size RR: "<<size_RR<<std::endl;
		//std::cout<<"Num of intervals:"<<intervals_5min.size()-1<<std::endl;

		//Compute HRV1 time/frequency analysis parameters for 5min intervals
		for(int i=0; i<intervals_5min.size()-1; i++)
		{
			timestamp_5min.push_back(i*5+5);
			std::cout<<"Interval nr:"<<i<<std::endl;
			//Choose RR intervals and timestamps of proceeding 5min interval
			std::vector<double> RR_5min(&RR[intervals_5min[i]], &RR[intervals_5min[i+1]]);
			std::vector<double> time_5min(&timestamp_double[intervals_5min[i]], &timestamp_double[intervals_5min[i+1]]);

			//HRV1 time analysis
			HRV1_time_analysis(RR_5min, mean_5min, std_5min, rmssd_5min, nn50_5min, pnn50_5min);

			//HRV1 frequency analysis
			HRV1_freq_analysis(RR_5min, time_5min, hf_5min, lf_5min, vlf_5min, ulf_5min, tp_5min, lfhf_5min);
			
		}
		//Compute values of SDANN, SDANNind parameters
		SDANN = (float)(compute_std(mean_5min));
		other_data.push_back(SDANN);
		SDANNind = (float)(compute_mean(std_5min));
		other_data.push_back(SDANNind);

		std::cout << "[HRV1] Frequency analysis proceeding." << std::endl;

		//Resample RR intervals signal
		resample(RR, timestamp_double, resampled_RR, resampled_time);

		//Compute FFT of resampled signal
		doFFT(resampled_RR, fft_magnitude, fft_frequency);

		//Smooth signal with average filter
		avgFilter(fft_magnitude, filtered_fft_magnitude, avgFilter_window_size);

		//Compute values of frequency analysis parameters for whole signal
		for(int i=0; i<fft_frequency.size(); i++)
		{
			double f = fft_frequency[i];
			double m = filtered_fft_magnitude[i];

			if(f<=0.4 && f>0.15)
				HF += m;
			if(f<=0.15 && f>0.04)
				LF += m;
			if(f<=0.04 && f>0.003)
				VLF += m;
			if(f<=0.003 && f>0.0001)
				ULF += m;
		}
		TP = HF+LF+VLF+ULF;
		LFHF = (double)(LF/HF);

		other_data.push_back(HF);
		other_data.push_back(LF);
		other_data.push_back(VLF);
		other_data.push_back(ULF);
		other_data.push_back(TP);
		other_data.push_back(LFHF);

		//printf("\n HF: %8.8f, LF: %8.8f, VLF: %8.8f \n", HF, LF, VLF);	
		//printf("ULF: %8.8f, TP: %8.8f, LFHF: %8.8f \n", ULF, TP, LFHF);	

    }

	//Save computed data to outpet timeseries
    for (auto &output : outputs)
    {
        std::cout << "[HRV1] Output name: " << output.first << "\n";

        auto &output_series = output.second.get();

		if (output.first == std::string("HRV1_Tachogram"))
        {
			output_series.points.resize(RR.size());
            for (uint64_t i=0; i<RR.size(); i++)
            {
                output_series.points[i].time = timestamp_uint64[i];
                output_series.points[i].value = RR[i];
            }
        }
		else if(output.first == std::string("HRV1_mean_5min"))
        {
			output_series.points.resize(mean_5min.size());
            for (uint64_t i=0; i<mean_5min.size(); i++)
            {
                output_series.points[i].time = timestamp_5min[i];
                output_series.points[i].value = mean_5min[i];
            }
        }
		else if(output.first == std::string("HRV1_std_5min"))
        {
			output_series.points.resize(std_5min.size());
            for (uint64_t i=0; i<std_5min.size(); i++)
            {
                output_series.points[i].time = timestamp_5min[i];
                output_series.points[i].value = std_5min[i];
            }
        }
		else if(output.first == std::string("HRV1_rmssd_5min"))
        {
			output_series.points.resize(rmssd_5min.size());
            for (uint64_t i=0; i<rmssd_5min.size(); i++)
            {
                output_series.points[i].time = timestamp_5min[i];
                output_series.points[i].value = rmssd_5min[i];
            }
        }
		else if(output.first == std::string("HRV1_nn50_5min"))
        {
			output_series.points.resize(nn50_5min.size());
            for (uint64_t i=0; i<nn50_5min.size(); i++)
            {
                output_series.points[i].time = timestamp_5min[i];
                output_series.points[i].value = nn50_5min[i];
            }
        }
		else if(output.first == std::string("HRV1_pnn50_5min"))
        {
			output_series.points.resize(pnn50_5min.size());
            for (uint64_t i=0; i<pnn50_5min.size(); i++)
            {
                output_series.points[i].time = timestamp_5min[i];
                output_series.points[i].value = pnn50_5min[i];
            }
        }
		else if(output.first == std::string("HRV1_FFT_Magnitude"))
        {
			output_series.points.resize(filtered_fft_magnitude.size());
            for (uint64_t i=0; i<filtered_fft_magnitude.size(); i++)
            {
                output_series.points[i].time = 0;
                output_series.points[i].value = filtered_fft_magnitude[i];
            }
        }
		else if(output.first == std::string("HRV1_FFT_Frequency"))
        {
			output_series.points.resize(fft_frequency.size());
            for (uint64_t i=0; i<fft_frequency.size(); i++)
            {
                output_series.points[i].time = 0;
                output_series.points[i].value = fft_frequency[i];
            }
        }
		else if(output.first == std::string("HRV1_hf_5min"))
        {
			output_series.points.resize(hf_5min.size());
            for (uint64_t i=0; i<hf_5min.size(); i++)
            {
                output_series.points[i].time = 0;
                output_series.points[i].value = hf_5min[i];
            }
        }
		else if(output.first == std::string("HRV1_lf_5min"))
        {
			output_series.points.resize(lf_5min.size());
            for (uint64_t i=0; i<lf_5min.size(); i++)
            {
                output_series.points[i].time = 0;
                output_series.points[i].value = lf_5min[i];
            }
        }
		else if(output.first == std::string("HRV1_vlf_5min"))
        {
			output_series.points.resize(vlf_5min.size());
            for (uint64_t i=0; i<vlf_5min.size(); i++)
            {
                output_series.points[i].time = 0;
                output_series.points[i].value = vlf_5min[i];
            }
        }
		else if(output.first == std::string("HRV1_ulf_5min"))
        {
			output_series.points.resize(ulf_5min.size());
            for (uint64_t i=0; i<ulf_5min.size(); i++)
            {
                output_series.points[i].time = 0;
                output_series.points[i].value = ulf_5min[i];
            }
        }
		else if(output.first == std::string("HRV1_tp_5min"))
        {
			output_series.points.resize(tp_5min.size());
            for (uint64_t i=0; i<tp_5min.size(); i++)
            {
                output_series.points[i].time = 0;
                output_series.points[i].value = tp_5min[i];
            }
        }
		else if(output.first == std::string("HRV1_lfhf_5min"))
        {
			output_series.points.resize(lfhf_5min.size());
            for (uint64_t i=0; i<lfhf_5min.size(); i++)
            {
                output_series.points[i].time = 0;
                output_series.points[i].value = lfhf_5min[i];
            }
        }
		else if(output.first == std::string("HRV1_other_data"))
        {
			output_series.points.resize(other_data.size());
            for (uint64_t i=0; i<other_data.size(); i++)
            {
                output_series.points[i].time = 0;
                output_series.points[i].value = other_data[i];
            }
        }
    }

    // All good! Since nothing went wrong, return 0 as a success code.
	std::cout << "Data processing succes!" << std::endl;
    return 0;
}

double HRV1::compute_mean(std::vector<double> &data_in)
{
	double suma_data = 0.0;
	double size_data = data_in.size();
	double mean_data;

	for(int i=0; i<size_data; i++)
	{
		suma_data += data_in[i];
	}
	mean_data = (double)(suma_data/size_data);

	return mean_data;
}

double HRV1::compute_std(std::vector<double> &data_in)
{
	double size_data = data_in.size();
	double mean_data;
	double SDNN_data = 0.0;

	mean_data = compute_mean(data_in);

	for(int i=0; i<size_data; i++)
	{
		SDNN_data += pow((mean_data - data_in[i]), 2.0);
	}
	SDNN_data = sqrt(SDNN_data*(1.0/(size_data-1.0)));

	return SDNN_data;
}

void HRV1::HRV1_time_analysis(std::vector<double> &data_in, std::vector<double> &mean_out, std::vector<double> &std_out, std::vector<double> &rmssd_out, std::vector<double> &nn50_out, std::vector<double> &pnn50_out)
{
	double suma_RR = 0.0;
	double mean_RR;		
	double SDNN_RR = 0.0;
	double RMSSD_RR = 0.0;
	double NN50_RR = 0;
	double pNN50_RR;

	double size_RR = data_in.size();
	double diff_RR;

	for(int i=0; i<size_RR; i++)
	{
		if(i<=size_RR-2)
		{
			diff_RR = (double)(data_in[i+1] - data_in[i]);
			RMSSD_RR += pow(diff_RR, 2.0);
			if((double)(abs(diff_RR)) > 50.0)
			{
				NN50_RR += 1;
			}
		}
	}	
	
	mean_RR = compute_mean(data_in);
	RMSSD_RR = sqrt(RMSSD_RR*(1.0/(size_RR-1)));
	pNN50_RR = 100.0*((double)(NN50_RR))/(size_RR-1);

	SDNN_RR = compute_std(data_in);

	//Save parameters values in output vectors
	mean_out.push_back(mean_RR);
	std_out.push_back(SDNN_RR);
	rmssd_out.push_back(RMSSD_RR);
	nn50_out.push_back(NN50_RR);
	pnn50_out.push_back(pNN50_RR);
	
	return;
}

void HRV1::resample(std::vector<double> &data_in, std::vector<double> &time_in, std::vector<double> &data_out, std::vector<double> &time_out)
{
	double sampl_freq = GetParameter<double>("sampl_freq");
	double size = data_in.size();
	int size_int = (int)(data_in.size());
	double yi;

	double x[size_int], y[size_int];

	for(int i = 0; i < size_int; i++)
	{
		x[i] = (double)(time_in[i]/1000000000.0);
		y[i] = data_in[i];
		
	}

	gsl_interp_accel *acc = gsl_interp_accel_alloc();
	gsl_spline *spline = gsl_spline_alloc(gsl_interp_cspline, size);
	
	gsl_spline_init(spline, x, y, size);

	for(double xi = x[0]; xi <= x[size_int-1]; xi += (double)(1.0/sampl_freq))
	{
		yi = gsl_spline_eval(spline, xi, acc);

		time_out.push_back(xi);
		data_out.push_back(yi);
	}
	
	gsl_spline_free(spline);
	gsl_interp_accel_free(acc);
}

void HRV1::doFFT(std::vector<double> &data_in, std::vector<double> &magnitude, std::vector<double> &frequency)
{
	double sampl_freq = GetParameter<double>("sampl_freq");
	double size = data_in.size();
	int size_int = (int)(data_in.size());

	double mag = 0.0;
    double freq = 0.0;

	//Create tables of type fftw_complex for input/output FFT data
	fftw_complex in1[size_int], out1[size_int];

	//Copy input data to input table of typ fft complex
	for(int i = 0; i < size_int; i++) {
    	in1[i][0] = data_in[i];
    	in1[i][1] = 0;
    }

	//1D Fast Fourier transform
    fftw_plan p;
    p = fftw_plan_dft_1d(size_int, in1, out1, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(p);

	for(int i = 0; i < (size_int/2+1); i++)
	{
		if(i==0)
			mag = pow(sqrt(pow(out1[i][0]/size,2.0)+pow(out1[i][1]/size, 2.0)),2.0);
		else
			mag = 2.0*pow(sqrt(pow(out1[i][0]/size,2.0)+pow(out1[i][1]/size, 2.0)),2.0);

		freq = (double)(i)*sampl_freq/size;

		magnitude.push_back(mag);
		frequency.push_back(freq);
	}
	//FFTW cleanup
	fftw_destroy_plan(p);
	fftw_cleanup();

}

void HRV1::avgFilter(std::vector<double> &data_in, std::vector<double> &data_out, int window_size)
{
	int size = (int)(data_in.size());
	double suma = 0.0;
	double new_value;

	for(int i=0; i<size; i++)
	{
		suma = 0.0;
		for(int j=0; j<window_size; j++)
		{
			if(i-j>=0)
			{
				suma += data_in[i-j];
			}
		}
		new_value = suma/((double)(window_size));
		data_out.push_back(new_value);
	}
}

void HRV1::HRV1_freq_analysis(std::vector<double> &RR_in, std::vector<double> &time_in, std::vector<double> &hf_out, std::vector<double> &lf_out, std::vector<double> &vlf_out, std::vector<double> &ulf_out, std::vector<double> &tp_out, std::vector<double> &lfhf_out)
{
	std::vector<double> resampled_RR;
	std::vector<double> resampled_time;
	std::vector<double> fft_magnitude;
	std::vector<double> fft_frequency;
	std::vector<double> filtered_fft_magnitude;

	int avgFilter_window_size = GetParameter<int>("window_size");
	double HF = 0.0;
	double LF = 0.0;
	double VLF = 0.0;
	double ULF = 0.0;
	double TP = 0.0;
	double LFHF = 0.0;

	resample(RR_in, time_in, resampled_RR, resampled_time);

	doFFT(resampled_RR, fft_magnitude, fft_frequency);
	avgFilter(fft_magnitude, filtered_fft_magnitude, avgFilter_window_size);

	for(int i=0; i<fft_frequency.size(); i++)
	{
		double f = fft_frequency[i];
		double m = filtered_fft_magnitude[i];

		if(f<=0.4 && f>0.15)
			HF += m;
		if(f<=0.15 && f>0.04)
			LF += m;
		if(f<=0.04 && f>0.003)
			VLF += m;
		if(f<=0.003 && f>0.0001)
			ULF += m;
	}
	TP = HF+LF+VLF+ULF;
	LFHF = (double)(LF/HF);

	hf_out.push_back(HF);
	lf_out.push_back(LF);
	vlf_out.push_back(VLF);
	ulf_out.push_back(ULF);
	tp_out.push_back(TP);
	lfhf_out.push_back(LFHF);

}

} // namespace ecg
