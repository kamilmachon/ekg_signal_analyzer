#include <ecg_signal_analyzer/hrv_dfa/HrvDfaTask.hpp>

#include <iostream>
#include <fstream>
#include <numeric>
#include <gsl/gsl_interp.h>
#include <eigen3/Eigen/QR>

namespace ecg
{
HrvDfaTask::HrvDfaTask()
{
    task_parameters_["dfa_alpha"] = 1;
}

int HrvDfaTask::ProcessData(const InputsType &inputs, const OutputsType &outputs)
{
    std::cout << "[HrvDfa] Processing data!" << std::endl;

    int alpha = GetParameter<int>("dfa_alpha");
    //std::cout << alpha << std::endl;
    
    std::vector<double> rr;

    for (const auto &input : inputs)
    {
        std::cout << "[HrvDfa] This is the input signal name: " << input.first << std::endl;

        // std::ofstream inputFile;
        // inputFile.open ("input.txt");

        auto data_range = input.second;
        for (auto it = data_range.first; it != data_range.second; ++it)
        {
            rr.push_back(static_cast<double>(((it)->time) / 1000000000.0));
            //inputFile << rr.back() << std::endl;
        }
        //inputFile.close();
    }

    int rrLength = rr.size();

    //std::vector<double> out;

    std::vector<int> windows(61);
    std::iota(windows.begin(), windows.end(), 4);

    std::vector<double> F;

    for(auto window = windows.begin(); window != windows.end(); ++window)
    {
        int n = rrLength/ *window;
        int rrLengthFloor = n * *window;
        
        std::vector<double> data;
        data.resize(rrLengthFloor);
        std::copy_n ( rr.begin(), rrLengthFloor, data.begin() );

        double mean = std::accumulate(data.begin(), data.end(), 0.0) / data.size();

        transform(data.begin(), data.end(), data.begin(), bind2nd(std::minus<double>(), mean));
        
        std::vector<double> dataCumsum;
        dataCumsum.resize(rrLengthFloor);
        std::partial_sum(data.begin(), data.end(), dataCumsum.begin());

        std::vector<double> bins;
        for(size_t i = 0; i<rrLengthFloor; i = i + *window)
            bins.push_back(i);

        std::vector<double> x(*window);
        std::iota(x.begin(), x.end(), 1);

        std::vector<double> coeff(2);
        std::vector<double> y_polyval;
        
        for (size_t i = 0; i < n; i++)
        {
            std::vector<int> idx(*window);
            transform(idx.begin(), idx.end(), idx.begin(), bind2nd(std::plus<double>(), bins[i]));
            std::vector<double> y(dataCumsum.begin() + bins[i], dataCumsum.begin() + bins[i] + *window);
            polyfit(x, y, coeff);
            std::vector<double> y_tmp(*window);
            polyval(x, y_tmp, coeff);
            y_polyval.insert(y_polyval.end(), y_tmp.begin(), y_tmp.end());
        }
        
        std::vector<double> res(rrLengthFloor);
        transform(dataCumsum.begin(), dataCumsum.end(), y_polyval.begin(), res.begin(), std::minus<double>());
        transform(res.begin(), res.end(), res.begin(), [](double a){ return pow(a, 2.0); });
        
        F.push_back(std::sqrt(std::accumulate(res.begin(), res.end(), 0.0) / res.size()));
    }
    
    
    
    transform(F.begin(), F.end(), F.begin(), [](double a){ return log10(a); });

    std::vector<double> A;
    std::vector<double> P;
    std::vector<double> windowsLog;

    if(alpha == 1)
    {   
        windowsLog.resize(windows.size());
        transform(windows.begin(), windows.end(), windowsLog.begin(), [](double a){ return log10(a); });

        std::vector<double> A0(2);
        polyfit(windowsLog, F, A0);
        std::vector<double> P0(windows.size());
        polyval(windowsLog, P0, A0);

        A.insert(A.begin(), A0.begin(), A0.end());
        P.insert(P.begin(), P0.begin(), P0.end());
    }else
    {
        std::vector<std::vector<double>> A0(2, std::vector<double>(2));  
        windowsLog.resize(windows.size() + 1);

        transform(windows.begin(), windows.begin() + 13, windowsLog.begin(), [](double a){ return log10(a); });  
        polyfit(std::vector<double>(windowsLog.begin(), windowsLog.begin()+ 13), std::vector<double>(F.begin(), F.begin()+ 13), A0[0]);
        transform(windows.begin() + 12, windows.end(), windowsLog.begin() + 13, [](double a){ return log10(a); });
        polyfit(std::vector<double>(windowsLog.begin() + 13, windowsLog.end()), std::vector<double>(F.begin() + 12, F.end()), A0[1]);

        // std::cout << "__________________________________________________________________" << "\n";

        // std::cout << "[HrvDfa] Output a1: " << A0[0][0] << "\t" << A0[0][1] << "\n";
        // std::cout << "[HrvDfa] Output a2: " << A0[1][0] << "\t" << A0[1][1] << "\n";

        std::vector<double> P1(std::vector<double>(windowsLog.begin(), windowsLog.begin()+ 13).size());
        polyval(std::vector<double>(windowsLog.begin(), windowsLog.begin()+ 13), P1, A0[0]);
        std::vector<double> P2(std::vector<double>(windowsLog.begin() + 12, windowsLog.end()).size());
        polyval(std::vector<double>(windowsLog.begin() + 12, windowsLog.end()), P2, A0[1]);

        P.insert(P.begin(), P1.begin(), P1.end());
	    P.insert(P.end(), P2.begin(), P2.end());
        
        A.insert(A.begin(), A0[0].begin(), A0[0].end());
	    A.insert(A.end(), A0[1].begin(), A0[1].end());
    }

    for (auto &output : outputs)
    {
        // Here, output is of type std::pair<std::string, std::reference_wrapper<TimeSeries<double>>>.
        // So, output.first is again, the name of the signal.
        std::cout << "[HrvDfa] Output name: " << output.first << "\n";

        // output.second is the reference wrapper - use get() on it, and you have direct access to the TimeSeries object.
        auto &output_series = output.second.get();

        if (output.first == std::string("HrvDfaDataX")) {
            output_series.points.resize(windowsLog.size());
            
            uint32_t i = 0;
            
            for (auto &winL : windowsLog)
            {
                output_series.points[i].value = winL;
                output_series.points[i].time = 0;
                i++;
            }
                    
        } else if(output.first == std::string("HrvDfaDataY")){
            output_series.points.resize(P.size());

            uint32_t i = 0;
            
            for (auto &p : P)
            {
                output_series.points[i].value = p;
                output_series.points[i].time = 0;
                i++;
            }
        
        } else if(output.first == std::string("HrvDfaAlpha")){
            output_series.points.resize(A.size());
            uint32_t i = 0;
            
            for (auto &a : A)
            {
                output_series.points[i].value = a;
                output_series.points[i].time = 0;
                i++;
            }
        }

        // std::ofstream outputFile;
        // outputFile.open ("output.txt");
        
        // output_series.points.resize(out.size());
        // for (uint64_t i=0; i<out.size(); i++)
        // {
        //     output_series.points[i].time = 0;
        //     output_series.points[i].value = out[i];

        //     outputFile << out[i] <<std::endl;
        // }

        // outputFile.close();

    }
    std::cout << "[HrvDfa] Data processing succes!\n\n" << std::endl;
    return 0;
}

/*
 *  polyfif based on: http://svn.clifford.at/handicraft/2014/polyfit/polyfit.cc
 * 
 *  Example code for fitting a polynomial to sample data (using Eigen 3)
 *
 *  Copyright (C) 2014  RIEGL Research ForschungsGmbH
 *  Copyright (C) 2014  Clifford Wolf <clifford@clifford.at>
 */

void HrvDfaTask::polyfit(const std::vector<double> &xv, const std::vector<double> &yv, std::vector<double> &coeff)
{
	Eigen::MatrixXd A(xv.size(), 2);
	Eigen::VectorXd yv_mapped = Eigen::VectorXd::Map(&yv.front(), yv.size());
	Eigen::VectorXd result;

	assert(xv.size() == yv.size());
	assert(xv.size() >= 2);

	// create matrix
	for (size_t i = 0; i < xv.size(); i++)
	for (size_t j = 0; j < 2; j++)
		A(i, j) = pow(xv.at(i), j);

	// solve for linear least squares fit
	result = A.householderQr().solve(yv_mapped);

	coeff.resize(2);
	for (size_t i = 0; i < 2; i++)
		coeff[i] = result[i];
}

void HrvDfaTask::polyval(const std::vector<double> &xv, std::vector<double> &yv, std::vector<double> &coeff)
{
    for (auto i = 0; i < xv.size(); i++)
    {
        yv[i] = coeff[0] + xv[i] * coeff[1];
    }
}

}