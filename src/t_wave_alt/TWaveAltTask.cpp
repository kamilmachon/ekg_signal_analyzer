#include <ecg_signal_analyzer/t_wave_alt/TWaveAltTask.hpp>


#include <math.h>       /* sqrt */
#include <iostream>
#include <fstream> 
#include <unistd.h>
#define TWA_IDENTIFIER "[TWaveAlt] "

// #define _TWA_DBG

#ifdef _TWA_DBG
    FILE *mmaFile, *waveFile;
#endif

namespace ecg



{

TWaveAltTask::TWaveAltTask()
{
    // The Constructor is needed only for parameter initialization (parameters could be changed in GUI).
    // task_parameters_ is a hash map where the key is string and the value is std::any.
    // Thanks to the std::any you can put any parameter type you want inside the map, just like in the python dictionary.
    // Parameters can be added by operator[] or AddParameter(std::string, std::any) method function, just like presented below.
    AddParameter("TWaveTestParameter", true);
    // std::cout << TWA_IDENTIFIER << "TWaveAltTask Construcor\n";
    waveNum=0;
    mma[0].clear();
    mma[1].clear();
    maxMmaAmp[0] = -1; maxMmaAmp[1] = -1;
    beatsSkipped[0]=0; beatsSkipped[1]=0;
    TwaValue=0;
}

TWaveAltTask::~TWaveAltTask()
{
    mma[0].clear();
    mma[0].shrink_to_fit();
    mma[1].clear();
    mma[1].shrink_to_fit();
}

int TWaveAltTask::ProcessData(const InputsType &inputs, const OutputsType &outputs)
{
   
    {
        bool param1 = GetParameter<bool>("TWaveTestParameter");
    }
    
    #ifdef _TWA_DBG
        mmaFile = fopen("mmaFile.txt", "w");
        waveFile = fopen("waveFile.txt", "w");
    #endif

    
    std::vector<struct TwaHeartBeat>  Beats;
    LoadInputs(inputs, Beats);
    // mma[0].clear();
    // mma[1].clear();
    // printf("%lu, %lu\n", mma[0].size(), mma[1].size());
    std::vector<TimeSeries<double>::Point> TwaValuData; 
    std::vector<TimeSeries<double>::Point> Ton; 
    std::vector<TimeSeries<double>::Point> TwaBeats; 
    std::vector<TimeSeries<double>::Point> TwaveAmp; 

    int bets_processed=0;
    for(auto & Beat: Beats) {
        // std::cout << Beat.BeatEndIt - Beat.BeatStartIt << "-" << (Beat.TendIt - Beat.BeatStartIt) << "-" << Beat.isEaven << ", ";
        ProcessBeat(Beat);
        FindTon(&Beat);
        FindTwaveAmp(&Beat);
        TwaValuData.push_back({TwaValue, Beat.BeatStartIt->time});
        Ton.push_back({Beat.TonIt->value, Beat.TonIt->time});
        if(Beat.TwaveAmp>Beat.TonIt && Beat.TwaveAmp<Beat.TendIt)
            TwaveAmp.push_back({Beat.TwaveAmp->value, Beat.TwaveAmp->time});

        if((TwaValue>0.02) && (bets_processed > 3))
            TwaBeats.push_back({(double)(Beat.BeatEndIt->time - Beat.BeatStartIt->time + 
                                        ((Beat.BeatStartIt+1)->time - Beat.BeatStartIt->time)), 
                                Beat.BeatStartIt->time});
        bets_processed++;
    }

    
    // So you have to iterate through it.
    for (auto &output : outputs)
    {
        if(output.first == "TwaValue"){
            std::cout << TWA_IDENTIFIER << "Output name: " << output.first << "\n";
            auto &output_series = output.second.get();

            #ifdef _TWA_DBG
                std::ofstream DbgData;
                DbgData.open ("TwaValue.txt", std::ofstream::out);
            #endif     
            size_t start_from = 0; // Use this to avoid searching from the beginning each time.
            for (auto &point : TwaValuData)
            {
                #ifdef _TWA_DBG
                    DbgData << point.value << ", " << point.time << "\n";
                #endif
                auto it = output_series.Find(point.time, start_from);
                start_from = std::distance(begin(output_series.points), std::next(it)); // In the next iteration, search for the right spot will start from here.
                output_series.points.insert(it, point);
            }
            #ifdef _TWA_DBG
                DbgData.close();
            #endif
        }
        else if(output.first == "TwaBeats"){
            std::cout << TWA_IDENTIFIER << "Output name: " << output.first << "\n";
            auto &output_series = output.second.get();

            #ifdef _TWA_DBG
                std::ofstream DbgData1;
                DbgData1.open ("TwaBeats.txt", std::ofstream::out);
            #endif     
            size_t start_from = 0; // Use this to avoid searching from the beginning each time.
            for (auto &point : TwaBeats)
            {
                #ifdef _TWA_DBG
                    DbgData1 << point.value << ", " << point.time << "\n";
                #endif
                auto it = output_series.Find(point.time, start_from);
                start_from = std::distance(begin(output_series.points), std::next(it)); // In the next iteration, search for the right spot will start from here.
                output_series.points.insert(it, point);
            }
            #ifdef _TWA_DBG
                // DbgData1.close();
            #endif
        }
        else if(output.first == "Ton"){
            std::cout << TWA_IDENTIFIER << "Output name: " << output.first << "\n";
            auto &output_series = output.second.get();

            #ifdef _TWA_DBG
                std::ofstream DbgData1;
                DbgData1.open ("TonData.txt", std::ofstream::out);
            #endif     
            size_t start_from = 0; // Use this to avoid searching from the beginning each time.
            for (auto &point : Ton)
            {
                #ifdef _TWA_DBG
                    DbgData1 << point.value << ", " << point.time << "\n";
                #endif
                auto it = output_series.Find(point.time, start_from);
                start_from = std::distance(begin(output_series.points), std::next(it)); // In the next iteration, search for the right spot will start from here.
                output_series.points.insert(it, point);
            }
            #ifdef _TWA_DBG
                DbgData1.close();
            #endif
        }
        else if(output.first == "TwaveAmp"){
            std::cout << TWA_IDENTIFIER << "Output name: " << output.first << "\n";
            auto &output_series = output.second.get();

            #ifdef _TWA_DBG
                std::ofstream DbgData1;
                DbgData1.open ("TwaveAmp.txt", std::ofstream::out);
            #endif     
            size_t start_from = 0; // Use this to avoid searching from the beginning each time.
            for (auto &point : TwaveAmp)
            {
                #ifdef _TWA_DBG
                    DbgData1 << point.value << ", " << point.time << "\n";
                #endif
                auto it = output_series.Find(point.time, start_from);
                start_from = std::distance(begin(output_series.points), std::next(it)); // In the next iteration, search for the right spot will start from here.
                output_series.points.insert(it, point);
            }
            #ifdef _TWA_DBG
                DbgData1.close();
            #endif
        }
        else {
            std::cout << TWA_IDENTIFIER << "UNUSED: Output name: " << output.first << "\n";
        }

    }
    #ifdef _TWA_DBG
        fclose(mmaFile);
        fclose(waveFile);
    #endif
    std::cout << TWA_IDENTIFIER << "Data processing succes!\n";
    // All good! Since nothing went wrong, return 0 as a success code.
    // sleep(1);
    return 0;
}


int TWaveAltTask::LoadInputs(const InputsType &inputs, std::vector<struct TwaHeartBeat> & Beats){
    Beats.clear();

    std::vector<ecg::TimeSeries<double>::Point>::iterator 
        RpeaksStart, RpeaksEnd, TendStart, TendEnd, filteredStart, filteredEnd;


    // Iterate trought inputs 
    for (const auto &input : inputs)
    {
        
        if(input.first == "RpeaksData"){
            std::cout << TWA_IDENTIFIER << "Loading data: " << input.first << std::endl;
            auto data_range = input.second;
            RpeaksStart = data_range.first;
            RpeaksEnd = data_range.second;

            #ifdef _TWA_DBG
                std::ofstream DbgData;
                DbgData.open ("RpeaksData.txt", std::ofstream::out);
                for (auto it = data_range.first; it != data_range.second; ++it)
                {
                    DbgData << it->value << ", " << it->time << "\n";
                }
                DbgData.close();
            #endif            
        }
        else if(input.first == "FilteredData"){
            std::cout << TWA_IDENTIFIER << "Loading data: " << input.first << std::endl;
            auto data_range = input.second;
            filteredStart = data_range.first;
            filteredEnd = data_range.second;

            #ifdef _TWA_DBG
                std::ofstream DbgData;
                DbgData.open ("FilteredData.txt", std::ofstream::out);
                for (auto it = data_range.first; it != data_range.second; ++it)
                {
                    DbgData << it->value << ", " << it->time << "\n";
                }
                DbgData.close();
            #endif     
        }
        else if(input.first == "Te"){
            std::cout << TWA_IDENTIFIER << "Loading data: " << input.first << std::endl;
            auto data_range = input.second;
            TendStart = data_range.first;
            TendEnd = data_range.second;

            #ifdef _TWA_DBG
                std::ofstream DbgData;
                DbgData.open ("TeData.txt", std::ofstream::out);
                for (auto it = data_range.first; it != data_range.second; ++it)
                {
                    DbgData << it->value << ", " << it->time << "\n";
                }
                DbgData.close();
            #endif     
        }
        else if(input.first == "QRSi"){
            std::cout << TWA_IDENTIFIER << "Loading data: " << input.first << std::endl;
            auto data_range = input.second;
            #ifdef _TWA_DBG
                std::ofstream DbgData;
                DbgData.open ("QRSiData.txt", std::ofstream::out);
                for (auto it = data_range.first; it != data_range.second; ++it)
                {
                    DbgData << it->value << ", " << it->time << "\n";
                }
                DbgData.close();
            #endif     
        }
        else if(input.first == "QRSe"){
            std::cout << TWA_IDENTIFIER << "Loading data: " << input.first << std::endl;
            auto data_range = input.second;
            #ifdef _TWA_DBG
                std::ofstream DbgData;
                DbgData.open ("QRSeData.txt", std::ofstream::out);
                for (auto it = data_range.first; it != data_range.second; ++it)
                {
                    DbgData << it->value << ", " << it->time << "\n";
                }
                DbgData.close();
            #endif     
        }
        else{
            std::cout << TWA_IDENTIFIER << "Warning! Unrecongized Data" << std::endl;
            std::cout << TWA_IDENTIFIER << "This is the data name: " << input.first << std::endl;
        }
    } 

    RpeaksStart, RpeaksEnd, TendStart, TendEnd, filteredStart, filteredEnd;
    auto RpeaksIt = RpeaksStart;
    auto TendIt = TendStart;
    auto filteredIt = filteredStart;

    // align data to first Rpeak
    while(filteredIt->time < RpeaksIt->time && filteredIt != filteredEnd){
        filteredIt++;
    }
    while((TendIt->time < RpeaksIt->time) && (TendIt < TendEnd)){
        TendIt++;
    }

    // Extract Beats  
    struct TwaHeartBeat newBeat;
    newBeat.BeatStartIt = filteredIt;
    filteredIt++;
    for(filteredIt; filteredIt!=filteredEnd; filteredIt++){
        if((RpeaksIt<RpeaksEnd) && (RpeaksIt->time <= filteredIt->time)){
            newBeat.BeatEndIt = filteredIt-1;
            if(newBeat.BeatEndIt - newBeat.BeatStartIt > 10){
                newBeat.isEaven = !newBeat.isEaven;
                newBeat.RrInterval = (newBeat.BeatEndIt->time - newBeat.BeatStartIt->time) / 1e9;
                Beats.push_back(newBeat);
            }
            newBeat.BeatStartIt = filteredIt;
            RpeaksIt++;
        }
        if((TendIt<TendEnd)  && (TendIt->time <= filteredIt->time)){
            newBeat.TendIt = filteredIt;
            TendIt++;
        }
    }

    std::cout << TWA_IDENTIFIER << Beats.size() << " Beats loaded\n";
}


int TWaveAltTask::FindTon(struct TwaHeartBeat *Beat){
    double t1, t2;
    double w = 0.12;
    if(Beat->RrInterval<0.76){
        t1 = 0.05;
        t2 = 0.25;
    }
    else if(Beat->RrInterval<1.13){
        t1 = 0.05;
        t2 = 0.35;
    }
    else{
        t1 = 0.05;
        t2 = 0.45;
    }
    // std::cout<< Beat->RrInterval << " " << t1 << " " << t2 << " " << w << std::endl;
    t1 *= 1e9; t2 *= 1e9; w *= 1e9;
    auto windowStartIt = Beat->BeatStartIt;
    for(windowStartIt; windowStartIt->time - (uint64_t)t1  <  Beat->BeatStartIt->time; windowStartIt++);

    double maxMean = -1e9;
    for(windowStartIt; windowStartIt->time - (uint64_t)t2  <  Beat->BeatStartIt->time; windowStartIt++){
        double mean = 0;
        for(auto windowIt = windowStartIt; windowIt->time - (uint64_t)w < windowStartIt->time; windowIt++){
            mean+= windowIt->value - windowStartIt->value;
        }
        if(mean >= maxMean){
            maxMean = mean;
            Beat->TonIt = windowStartIt;
        }
    }
    // std::cout << Beat->TonIt - Beat->BeatStartIt << ", ";
    return 0;
}

int TWaveAltTask::FindTwaveAmp(struct TwaHeartBeat *Beat){

    Beat->TwaveAmp = Beat->BeatStartIt;
    double maxAmp = Beat->TonIt->value;
    if(((Beat->TendIt - Beat->TonIt) > 0 ) && ((Beat->TendIt - Beat->TonIt) < (Beat->BeatEndIt - Beat->BeatStartIt))){
        for(auto it = Beat->TonIt; it != Beat->TendIt; it++){
            if(it->value > maxAmp){
                maxAmp = it->value;
                Beat->TwaveAmp = it;
            }
        }
    }
    return 0;
}

int TWaveAltTask::ProcessBeat(struct TwaHeartBeat Beat)                       
{   
    int isOdd = !(Beat.isEaven);

    isOdd = !!isOdd;
    int TendOffset = Beat.TendIt - Beat.BeatStartIt;
    if(TendOffset < 30){
        return -1;
    }

    std::vector<double> waveBuf;
    waveBuf.clear();
    for(auto it = Beat.BeatStartIt; it!= Beat.BeatEndIt; it++){
        waveBuf.push_back(it->value);
    }
    double* wavePtr = waveBuf.data();
    double* mmaPtr = mma[isOdd].data();
    // std::cout << isOdd << ", ";
    // printf(TWA_IDENTIFIER"%i, %lu, %lu, %i\n", isOdd, waveBuf.size(), mma[isOdd].size(), TendOffset);
    // Count correlation between MMA and new signal
    double corr=1;
    if((waveBuf.size()>TendOffset) && (mma[isOdd].size()>TendOffset)){
        
        corr = gsl_stats_correlation(wavePtr+20, 1, mmaPtr+20, 1, TendOffset-21); 
    }

    if(corr > 0.9)
    {
        UpdateMma(isOdd, waveBuf, TendOffset);
        beatsSkipped[isOdd] = 0;
        ComputeTwa(TendOffset, isOdd);
    }else
    {
        if(beatsSkipped[isOdd] > 5)
        {
            UpdateMma(isOdd, waveBuf, TendOffset);
        }
        beatsSkipped[isOdd]++;
        // TwaValue = -0.1;
        // ComputeTwa();

    } 
    

    // Debug 
    #ifdef _TWA_DBG
        for(int i=0; i<300; i++)
        {
            fprintf(mmaFile, "% 2.6f, ", mma[isOdd].size()>i ? mma[isOdd].at(i) : 0.0);
            fprintf(waveFile, "% 2.6f, ", waveBuf.size()>i ? waveBuf.at(i) : 0.0);

        }
        fprintf(mmaFile, "0 \n");
        fprintf(waveFile, "0 \n");
    #endif

    
    waveBuf.clear();
    waveNum++;
}


int TWaveAltTask::UpdateMma(int isOdd, std::vector<double> & signal, int TendOffset)                        
{   

    // Update MMA with new signal
    // printf("%lu, %lu, ", mma[isOdd].size(), signal.size());
    size_t min_size = (signal.size() < mma[isOdd].size()) ? signal.size() : mma[isOdd].size();
    auto mmaIt = mma[isOdd].begin();
    for(size_t i = 0; i<min_size; i++){
        double delta = (*mmaIt - signal[i]) / 8;
        if(delta > 0.032) 
            delta = 0.032;
        else if(delta < -0.032) 
            delta = -0.032;
        *mmaIt -= delta;
        mmaIt++; 
    }

    // Enlarge and init MMA vector if nessesary
    if(signal.size() > mma[isOdd].size())
    {
        for(size_t i = mma[isOdd].size(); i<signal.size(); i++)
            mma[isOdd].push_back(signal[i]);
    }   
    return 0;
    // printf("%lu\n", mma[isOdd].size());
}

double TWaveAltTask::ComputeTwa(int TendOffset, int isEaven)
{
    if(mma[isEaven].size()<TendOffset)
        return -1;

    // maxMmaAmp[isEaven] = 
    double max = gsl_stats_max(mma[isEaven].data() + 20, 1, TendOffset-20);   
    double min = gsl_stats_min(mma[isEaven].data() + 20, 1, TendOffset-20);
    if(max<0) {max *= -1;}
    // if(min<0) {min *= -1;}
    // if(max<min) {max = min;}
    maxMmaAmp[isEaven] = max;

    if(mma[!isEaven].size()<10){
        return-1;
    }
    
    if(maxMmaAmp[0]>=0 && maxMmaAmp[1]>=0){
        TwaValue = maxMmaAmp[0] - maxMmaAmp[1];
        if(TwaValue < 0){
            TwaValue *= -1;
        }
    }
    if(TwaValue>1 || TwaValue<-1)
        TwaValue=0;

    return TwaValue;    
}

#undef _TWA_DBG
} // namespace ecg