#include <ecg_signal_analyzer/hrv2/HRV2.hpp>
#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_interp.h>

namespace ecg
{

int HRV2::ProcessData(const InputsType &inputs, const OutputsType &outputs)
{
    std::cout << "[HRV2] Processing data!" << std::endl;
    std::vector<double> timestamp_temp;

    for (const auto &input : inputs)
    {
        std::cout << "[HRV2] This is the input signal name: " << input.first << std::endl;
        auto data_range = input.second;

        if(input.first == "RpeaksData2")
        {
            for (auto it = data_range.first; it != data_range.second; ++it)
            {
                //std::cout << "Value of " << it->value << " at time " << it->time << "\n";
                timestamp_temp.push_back(it->time);
            }
        }
    }
    
    // removing last record (is equal to 0) and converting nano seconds to miliseconds
    std::vector<double> timestamp;
    for(auto it = timestamp_temp.begin(); it != (timestamp_temp.end() - 1); ++it)
    {
        timestamp.push_back((*it)/1000000);
    }

    // Poincare plot, SD1, SD2
    std::vector<std::pair<double, double>> poincare_plot = GetPoincare(timestamp);
    std::pair<double, double> sd_values = GetSDValues(timestamp);
    // std::cout << "SD1: " << sd_values.first << std::endl;
    // std::cout << "SD2: " << sd_values.second << std::endl;

    // Prepare histogram vector
    std::vector<std::pair<double, int32_t>> histogram = GetHistogram(timestamp);
    std::vector<int32_t> histogram_y;
    for(int32_t i = 0; i < histogram.size(); ++i)
    {
        histogram_y.push_back(histogram[i].second);
    }

    // Histogram Maximum Point, Triangular Index, TINN
    std::pair<double, double> max_histogram_point = MaxHistogramPoint(histogram_y);
    double triangular_index = GetTriangularIndex(histogram_y);
    std::pair<double, double> tinn_params = GetTINN(timestamp, histogram_y, max_histogram_point.first);
    double tinn = tinn_params.first - tinn_params.second;

    // Convert section number to value
    double bin_width = 1000.0 / 128;
    double min_rr_value = *std::min_element(timestamp.begin(), timestamp.end());
    max_histogram_point.first = min_rr_value + max_histogram_point.first * bin_width;

    for (auto &output : outputs)
    {
        std::cout << "[HRV2] This is the output signal name: " << output.first << "\n";
        auto & output_series = output.second.get();

        if(output.first == "SDVals")
        {
            output_series.points.resize(2);
            output_series.points[0].value = sd_values.first;
            output_series.points[1].value = sd_values.second;
        }

        if(output.first == "PoincarePlot")
        {
            size_t i = 0;
            output_series.points.resize(poincare_plot.size());
            for (auto &point : poincare_plot)
            {
                output_series.points[i].value = point.first;
                output_series.points[i].time = point.second;
                ++i;
            }
        }

        if(output.first == "Histogram")
        {
            size_t i = 0;
            output_series.points.resize(histogram.size());
            for (auto &point : histogram)
            {
                output_series.points[i].value = point.first;
                output_series.points[i].time = point.second;
                ++i;
            }
        }

        if(output.first == "TriangularIndex")
        {
            output_series.points.resize(1);
            output_series.points[0].value = triangular_index;
        }

        if(output.first == "TINN")
        {
            output_series.points.resize(3);
            output_series.points[0].value = tinn_params.first;
            output_series.points[1].value = tinn_params.second;
            output_series.points[2].value = tinn;
        }

        if(output.first == "MaxHistogramPoint")
        {
            output_series.points.resize(2);
            output_series.points[0].value = max_histogram_point.first;
            output_series.points[1].value = max_histogram_point.second;
        }
    }

    std::cout << "[HRV2] Data processing succes!\n\n" << std::endl;
    return 0;
}

std::vector<std::pair<double, int32_t>> HRV2::GetHistogram(std::vector<double> & timestamp)
{
    double bin_width = 1000.0 / 128;
    int32_t histogram_section;
    double min_rr_value = *std::min_element(timestamp.begin(), timestamp.end());
    double max_rr_value = *std::max_element(timestamp.begin(), timestamp.end());
    int histogram_length = int((max_rr_value - min_rr_value) / bin_width);
    if((max_rr_value - min_rr_value) / bin_width != int((max_rr_value - min_rr_value) / bin_width))
        histogram_length += 1;

    std::vector<int32_t> histogram_y(histogram_length, 0);
    std::vector<std::pair<double, int32_t>> histogram;

    for(auto it = timestamp.begin(); it != timestamp.end(); ++it)
    {
        histogram_section = (*it - min_rr_value)/bin_width;
        histogram_y[histogram_section] += 1;
    }

    for(int32_t i = 0; i < histogram_y.size(); ++i)
    {
        histogram.push_back(std::make_pair(min_rr_value + bin_width*i, histogram_y[i]));
    }
    return histogram;
}

double HRV2::GetTriangularIndex(std::vector<int32_t> & histogram)
{
    int32_t sum_of_elements = 0;
    for(auto it = histogram.begin(); it != histogram.end(); ++it)
    {
        sum_of_elements += *it;
    }
    return double(sum_of_elements) / double(*std::max_element(histogram.begin(), histogram.end()));
}

std::pair<double, double> HRV2::MaxHistogramPoint(std::vector<int32_t> & histogram)
{
    double Y = *std::max_element(histogram.begin(), histogram.end());
    double X;

    for(int32_t i = 0; i < histogram.size(); ++i)
    {
        if(histogram[i] == Y)
        {
            X = i;
        }
    }
    std::pair<double, double> max_histogram_point(X, Y);
    return max_histogram_point;
}

std::pair<double, double> HRV2::GetTINN(std::vector<double> & timestamp, std::vector<int32_t> & histogram, double X)
{
    double min_rr_value = *std::min_element(timestamp.begin(), timestamp.end());
    double minimum = 0.0;
    double global_minimum = 0.0;
    double N = 0.0;
    double M = 0.0;
    double linear_value;
    bool set_global_minimum = false;
    double x[3] = {N, X, M};
    double y[3] = {0.0, 0.0, 0.0};
    int32_t histogram_size = histogram.size();

    gsl_interp *interpolation = gsl_interp_alloc(gsl_interp_linear, 3);
	gsl_interp_accel *accelerator =  gsl_interp_accel_alloc();

    for(int index_N=0; index_N < X-1; index_N++)
	{
		for(int index_M=X+1; index_M < histogram_size; index_M++)
		{
			x[0] = index_N; x[2] = index_M;
			gsl_interp_init(interpolation, x, y, 3);

			for(int i=0; i < index_N; i++)
			{
				minimum += pow(timestamp[i], 2);
			}

			for(int i=index_N+1; i < index_M-1; i++)
			{
				linear_value = gsl_interp_eval(interpolation, x, y, i, accelerator);
				minimum += pow(linear_value - timestamp[i], 2);
			}

			for(int i=index_M; i < histogram_size; i++)
			{
				minimum += pow(timestamp[i], 2);
			}

			if(!set_global_minimum)
			{
				global_minimum = minimum;
				set_global_minimum = true;
			}

			if(minimum < global_minimum)
			{
				global_minimum = minimum;
				N = x[0];
				M = x[2];
			}
			minimum = 0;
		}
	}
    M = min_rr_value + M * 1000 / 128.0;
    N = min_rr_value + N * 1000 / 128.0;
    std::pair<double, double> tinn_params(M, N);
    return tinn_params;
}

std::pair<double, double> HRV2::GetSDValues(std::vector<double> & timestamp)
{
    double sum_of_elements = 0.0, mean, SDNN = 0.0, SDSD = 0.0;

    // SDNN calculation
    for(auto it = timestamp.begin(); it != timestamp.end(); ++it)
    {
        sum_of_elements += *it;
    }
    mean = sum_of_elements / timestamp.size();

    for(auto it = timestamp.begin(); it != timestamp.end(); ++it)
    {
        SDNN += pow(*it - mean, 2);
    }
    SDNN = sqrt(SDNN/timestamp.size());

    // SDSD calculation
    sum_of_elements = 0.0;
    std::vector<double> timestamp_diff(timestamp.size() -1);
    for(auto it = timestamp.begin(); it != timestamp.end(); ++it)
    {
        timestamp_diff.push_back(*(it+1) - *it);
    }

    for(auto it = timestamp_diff.begin(); it != timestamp_diff.end(); ++it)
    {
        sum_of_elements += *it;
    }
    mean = sum_of_elements / timestamp_diff.size();

    for(auto it = timestamp_diff.begin(); it != timestamp_diff.end(); ++it)
    {
        SDSD += pow(*it - mean, 2);
    }
    SDSD = sqrt(SDSD/timestamp_diff.size());
    double SD1 = SDSD/sqrt(2);
    double SD2 = sqrt(2 * pow(SDNN, 2) - pow(SDSD, 2)/2);

    std::pair<double, double> sd_values(SD1, SD2);
    return sd_values;
}

std::vector<std::pair<double, double>> HRV2::GetPoincare(std::vector<double> & timestamp)
{
    std::vector<std::pair<double, double>> poincare_axis;

    for(auto it = timestamp.begin(); it != (timestamp.end() - 1); ++it)
    {
        poincare_axis.push_back(std::make_pair(*it, *(it+1)));
    }

    return poincare_axis;
}

} // namespace ecg